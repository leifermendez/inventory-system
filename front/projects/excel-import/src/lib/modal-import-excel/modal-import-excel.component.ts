import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {RestService} from "../../../../../src/app/services/rest.service";
import {BsModalRef} from "ngx-bootstrap/modal";
import {faFileExcel} from '@fortawesome/free-solid-svg-icons';
import * as _ from 'lodash';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
import {ColumnMode, SortType} from '@swimlane/ngx-datatable';

type AOA = any[][];

@Component({
  selector: 'lib-modal-import-excel',
  templateUrl: './modal-import-excel.component.html',
  styleUrls: ['./modal-import-excel.component.css']
})
export class ModalImportExcelComponent implements OnInit {
  @Input() type: string = '';
  @ViewChild('imageInput') input_file: ElementRef;
  faFileExcel = faFileExcel
  public file = []
  public fileName: any
  public mode: string
  public loading: boolean
  public help = ''
  preview_file: boolean = false
  rows = [];
  columns = [{name: 'Company'}, {name: 'Name'}, {name: 'Gender'}];
  wopts: XLSX.WritingOptions = {bookType: 'xlsx', type: 'array'};
  public dataExcel = [];
  ColumnMode = ColumnMode;
  SortType = SortType;


  constructor(private rest: RestService, public bsModalRef: BsModalRef) {
  }

  ngOnInit(): void {
    console.log('El tipo de tabla a procesar es: ', this.type)
  }

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    Object.values(evt.target.files).map(async ($event: File) => {
      // console.log($event)
      // let mb = $event.size / 1024 / 1024;
      // if( mb < 2 ) this.preview_file = true
      // console.log('Los mb del archivo son:', mb)
      this.fileName = $event
      this.file.push($event);
    })

    if (!this.checkType(this.fileName.name)) return
    /* wire up file reader */
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      /* save data */
      this.dataExcel = this.clearArray(this.dataExcel)
      console.log('***', this.dataExcel)
      // if (this.data_excel.length > 500) this.preview_file = true
      this.preview_file = true
      this.dataExcel = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      this.columns = _.map(this.dataExcel.shift(), (v) => Object.assign({}, {name: v}));
      console.log('***',this.columns)

      this.dataExcel = [..._.map(this.dataExcel, (inValueArray) => {
        const keyFromHeader = _.map(this.columns, (v) => v.name.toLowerCase())
        return _.zipObject(keyFromHeader, inValueArray);
      })
      ]

      console.log(this.dataExcel)


    };
    reader.readAsBinaryString(target.files[0]);
  }

  checkType = (name: string) => {
    console.log(name)
    if (name.includes(this.type)) {
      this.mode = this.type

      return true
    } else {
      Swal.fire('File incorrecto', '', 'error');
      this.fileName = null
      this.input_file.nativeElement.value = "";
      this.file = []
      return false
    }
  }

  sendFile = () => {
    this.loading = true;
    const formData = new FormData();
    this.file.forEach((item) => formData.append('files[]', item));
    this.rest.post(`storage`, formData, true, {})
      .subscribe(res => {
        const {data} = res;
        this.importFile(data.find(() => true))
      }, e => this.loading = false)
  }

  importFile = (data: any) => {
    const {original} = data
    this.rest.postEvents(`excelImport/events/import_file`, {
      "value": {
        "url": original,
        "type": this.mode
      }
    }).subscribe(res => {
      this.bsModalRef.hide();
      window.location.reload()
    })
  }

  clearArray = (data) => {
    let array = []
    data.forEach(d => {
      if (d.length != 0) array.push(d.toString())
    });
    return array
  }
}
