import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalImportExcelComponent} from "../modal-import-excel/modal-import-excel.component";
import {
  faFileExcel,
  faFileImport
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'lib-excel-button',
  templateUrl: './excel-button.component.html',
  styleUrls: ['./excel-button.component.css']
})
export class ExcelButtonComponent implements OnInit {
  bsModalRef: BsModalRef;
  faFileImport = faFileImport
  @Input() type: string;

  constructor(private modalService: BsModalService) {
  }

  ngOnInit(): void {
  }

  open(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalImportExcelComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan modal-import-excel',
        ignoreBackdropClick: true
      })
    );
    this.bsModalRef.content.type = this.type
  }
}
