import {NgModule} from '@angular/core';
import {ExcelImportComponent} from './excel-import.component';
import {ExcelButtonComponent} from './excel-button/excel-button.component';
import {ModalModule} from "ngx-bootstrap/modal";
import {ModalImportExcelComponent} from './modal-import-excel/modal-import-excel.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {TranslateModule} from "@ngx-translate/core";
import {CommonModule} from "@angular/common";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";


@NgModule({
  declarations: [ExcelImportComponent, ExcelButtonComponent, ModalImportExcelComponent],
  imports: [
    ModalModule.forRoot(),
    FontAwesomeModule,
    TranslateModule,
    CommonModule,
    NgxDatatableModule.forRoot({
      messages: {
        emptyMessage: 'No data to display', // Message to show when array is presented, but contains no values
        totalMessage: 'total', // Footer total message
        selectedMessage: 'selected' // Footer selected message
      }
    })
  ],
  exports: [ExcelImportComponent, ExcelButtonComponent]
})
export class ExcelImportModule {
}
