import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PDFReportComponent } from './pdfreport.component';

describe('PDFReportComponent', () => {
  let component: PDFReportComponent;
  let fixture: ComponentFixture<PDFReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PDFReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PDFReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
