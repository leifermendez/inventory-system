import {NgModule} from '@angular/core';
import {PDFReportComponent} from './pdfreport.component';
import {SettingsComponent} from './settings/settings.component';
import {QuillModule} from "ngx-quill";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CodemirrorModule} from "@ctrl/ngx-codemirror";
import {ButtonPdfComponent} from './button-pdf/button-pdf.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {TranslateModule} from "@ngx-translate/core";
import {CommonModule} from "@angular/common";
import {ModalCustomTemplateComponent} from './modal-custom-template/modal-custom-template.component';
import {TemplateHelperPipe} from './template-helper.pipe';



@NgModule({
  declarations: [PDFReportComponent, SettingsComponent, ButtonPdfComponent, ModalCustomTemplateComponent, TemplateHelperPipe],
  imports: [
    QuillModule,
    CodemirrorModule,
    ReactiveFormsModule,
    FormsModule,
    FontAwesomeModule,
    TranslateModule,
    CommonModule
  ],
  exports: [PDFReportComponent, SettingsComponent, ButtonPdfComponent]
})
export class PDFReportModule {
}
