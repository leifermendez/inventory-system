import {Injectable} from '@angular/core';
import {RestService} from "../../../../src/app/services/rest.service";

@Injectable({
  providedIn: 'root'
})
export class PDFReportService {
  constructor(private rest: RestService) {
  }

  public generatePurchase = (id = null) => {
    let data = {
      "items":[],
      "taxes":[],
      "sumTaxesTotal":0,
      "subTotal":0,
      "total":0
    }
    this.rest.patch(`purchase/convert/${id}`, {...data,convert: 'order_generate_purchase'})
      .subscribe(res => {
        window.open(res.download)
    })
  }

  public generatePurchaseCopy = (id = null) => {
    this.rest.post(`purchase/convert`, {value: id})
      .subscribe(res => {
        window.open(res.download)
      })
  }

  public generateList = (data = {}) => {
    this.rest.post(`plugins/pdfReport/events/list`, {value: data})
      .subscribe(res => {
        window.open(res.download)
    })
  }

  public updateSettings = (data = {}) => {
    this.rest.post(`plugins/pdfReport/events/update`, {value: data})
      .subscribe(res => {
        console.log('------------', res)
      })
  }
}
