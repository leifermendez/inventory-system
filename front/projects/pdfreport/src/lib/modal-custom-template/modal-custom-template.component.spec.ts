import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCustomTemplateComponent } from './modal-custom-template.component';

describe('ModalCustomTemplateComponent', () => {
  let component: ModalCustomTemplateComponent;
  let fixture: ComponentFixture<ModalCustomTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCustomTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCustomTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
