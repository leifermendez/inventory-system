import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KitAgilStripeComponent } from './kit-agil-stripe.component';

describe('KitAgilStripeComponent', () => {
  let component: KitAgilStripeComponent;
  let fixture: ComponentFixture<KitAgilStripeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KitAgilStripeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitAgilStripeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
