import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RestService} from "../../../../../src/app/services/rest.service";
import {ShareService} from "../../../../../src/app/services/share.service";

@Component({
  selector: 'lib-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  public form: FormGroup;
  public data: any;

  constructor(private formBuilder: FormBuilder, private rest: RestService, public shared: ShareService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      pk: ['', Validators.required],
      sk: ['', Validators.required]
    });
    this.load()
  }

  onSubmit() {
    if (window.confirm("Seguro")) {
      this.rest.postEvents(`stripe/events/update_setting`, {
        value: this.form.value
      }).subscribe(res => {
        this.data = res;
        this.form.reset()
      })
    }
  }

  onRegisterHook() {
    if (window.confirm("Seguro")) {
      this.rest.postEvents(`stripe/events/register_hook`, {
        value: this.form.value
      }).subscribe(res => {
        this.data = res;
        this.form.reset()
      })
    }
  }

  load = () => {
    this.rest.getEvents(`stripe/events/check_keys`).subscribe(res => {
      this.data = res;
      if (this.data && !this.data.validCurrency) {
        this.form.disable()
      }
    })
  }
}
