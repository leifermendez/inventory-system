import { TestBed } from '@angular/core/testing';

import { ScriptKitAgilService } from './script-kit-agil.service';

describe('ScriptKitAgilService', () => {
  let service: ScriptKitAgilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScriptKitAgilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
