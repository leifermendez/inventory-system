import {Injectable} from '@angular/core';
import {KitAgilStripeService} from "./kit-agil-stripe.service";

interface Scripts {
  name: string;
  src: string;
}

export const ScriptStore: Scripts[] = [
  {name: 'stripe', src: 'https://js.stripe.com/v3/'}
];

declare var document: any;

@Injectable()
export class ScriptKitAgilService {

  private scripts: any = {};

  constructor() {
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });
  }

  WrapperWindow(): any {
    return window;
  }

  load(...scripts: string[]) {
    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string) {
    return new Promise((resolve, reject) => {
      if (!this.scripts[name].loaded) {
        // load script
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;
        if (script.readyState) {  // IE
          script.onreadystatechange = () => {
            if (script.readyState === 'loaded' || script.readyState === 'complete') {
              script.onreadystatechange = null;
              this.scripts[name].loaded = true;
              resolve({script: name, loaded: true, status: 'Loaded'});
            }
          };
        } else {  // Others
          script.onload = () => {
            this.scripts[name].loaded = true;
            resolve({script: name, loaded: true, status: 'Loaded'});
          };
        }
        script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
        document.getElementsByTagName('head')[0].appendChild(script);
      } else {
        resolve({script: name, loaded: true, status: 'Already Loaded'});
      }
    });
  }

  public getLink = (data) => {
    const {customData = {}} = data;
    let message = `Esta orden tiene vinculado un link de pago online, puedes compartir el siguiente enlace a tu cliente.`;
    message += `<br> <a href="${window.location.origin}/add-events/stripe-checkout/${customData.tracker}" target="_blank">Ver link de pago</a>`
    return message;
  }

  public moreOnceOrder = (data) => {
    let message = `Ya tienes una orden de pago generada vinculada con este pedido. Si deseas puedes generar una nueva o ver todas las ordenes`;
    message += `<br> <a href="${window.location.origin}/purchase/${data}?tab=1">Ver ordenes</a>`
    return message;
  }

}
