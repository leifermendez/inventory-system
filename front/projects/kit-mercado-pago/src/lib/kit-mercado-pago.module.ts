import { NgModule } from '@angular/core';
import { KitMercadoPagoComponent } from './kit-mercado-pago.component';
import { SettingsComponent } from './settings/settings.component';
import {ReactiveFormsModule} from "@angular/forms";
import { MpInitComponent } from './mp-init/mp-init.component';



@NgModule({
  declarations: [KitMercadoPagoComponent, SettingsComponent, MpInitComponent],
    imports: [
        ReactiveFormsModule
    ],
  exports: [KitMercadoPagoComponent]
})
export class KitMercadoPagoModule { }
