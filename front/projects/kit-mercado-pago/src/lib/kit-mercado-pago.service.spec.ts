import { TestBed } from '@angular/core/testing';

import { KitMercadoPagoService } from './kit-mercado-pago.service';

describe('KitMercadoPagoService', () => {
  let service: KitMercadoPagoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KitMercadoPagoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
