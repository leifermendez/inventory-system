import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KitMercadoPagoComponent } from './kit-mercado-pago.component';

describe('KitMercadoPagoComponent', () => {
  let component: KitMercadoPagoComponent;
  let fixture: ComponentFixture<KitMercadoPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KitMercadoPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitMercadoPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
