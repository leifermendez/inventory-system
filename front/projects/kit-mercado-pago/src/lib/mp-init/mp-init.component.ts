import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'lib-mp-init',
  templateUrl: './mp-init.component.html',
  styleUrls: ['./mp-init.component.css']
})
export class MpInitComponent implements OnInit {
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      clientID: ['', Validators.required],
      secretID: ['', Validators.required],
      mode: ['test', Validators.required]
    });
  }
}
