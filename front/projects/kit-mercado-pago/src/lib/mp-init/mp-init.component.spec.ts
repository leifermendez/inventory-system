import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpInitComponent } from './mp-init.component';

describe('MpInitComponent', () => {
  let component: MpInitComponent;
  let fixture: ComponentFixture<MpInitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpInitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
