import { TestBed } from '@angular/core/testing';

import { KitAgilPaypalService } from './kit-agil-paypal.service';

describe('KitAgilPaypalService', () => {
  let service: KitAgilPaypalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KitAgilPaypalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
