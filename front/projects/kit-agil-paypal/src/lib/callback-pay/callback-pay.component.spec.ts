import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallbackPayComponent } from './callback-pay.component';

describe('CallbackPayComponent', () => {
  let component: CallbackPayComponent;
  let fixture: ComponentFixture<CallbackPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallbackPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallbackPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
