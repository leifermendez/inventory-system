import {Component, OnInit} from '@angular/core';
import {RestService} from "../../../../../src/app/services/rest.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'lib-callback-pay',
  templateUrl: './callback-pay.component.html',
  styleUrls: ['./callback-pay.component.css']
})
export class CallbackPayComponent implements OnInit {
  public content: any;
  public data: any
  public loading: boolean;


  constructor(private rest: RestService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      (params: any) => {
        this.updateOrder(params.token)
      });

  }

  public updateOrder = (token) => {
    this.loading = true;
    this.rest.postEventsPublic(`paypal/events/check_order`,
      {
        "value": {
          token,
          id: this.content.customData.tracker
        }
      }).subscribe(res => {
      this.loading = false;
      this.data = res;
    }, () => this.loading = false)
  }
  error: any;
}
