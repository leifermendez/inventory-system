import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KitAgilPaypalComponent } from './kit-agil-paypal.component';

describe('KitAgilPaypalComponent', () => {
  let component: KitAgilPaypalComponent;
  let fixture: ComponentFixture<KitAgilPaypalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KitAgilPaypalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitAgilPaypalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
