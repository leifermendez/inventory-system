import { __decorate } from "tslib";
import { AfterViewInit, Directive, ElementRef, Input, OnInit, TemplateRef } from '@angular/core';
import { NgxCopilotService } from "./ngx-copilot.service";
let CopilotDirective = class CopilotDirective {
    constructor(service, elem) {
        this.service = service;
        this.elem = elem;
        this.mode = 'vertical';
        this.overviewcolor = 'false';
        this.animatedEffect = '';
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        if (this.template) {
            this.elem.nativeElement.classList.add('ngx-copilot-init');
            if (this.step) {
                this.elem.nativeElement.dataset.step = this.step;
                this.elem.nativeElement.dataset.comode = this.mode;
                this.elem.nativeElement.dataset.overviewcolor = this.overviewcolor;
                this.elem.nativeElement.dataset.animatedEffect = this.animatedEffect;
                this.service.template.emit({
                    step: this.step,
                    template: this.template,
                    mode: this.mode,
                    overviewcolor: this.overviewcolor,
                    animatedEffect: this.animatedEffect
                });
            }
        }
    }
};
CopilotDirective.ctorParameters = () => [
    { type: NgxCopilotService },
    { type: ElementRef }
];
__decorate([
    Input('copilot-step')
], CopilotDirective.prototype, "step", void 0);
__decorate([
    Input('copilot-template')
], CopilotDirective.prototype, "template", void 0);
__decorate([
    Input('copilot-mode')
], CopilotDirective.prototype, "mode", void 0);
__decorate([
    Input('copilot-color')
], CopilotDirective.prototype, "overviewcolor", void 0);
__decorate([
    Input('copilot-class')
], CopilotDirective.prototype, "animatedEffect", void 0);
CopilotDirective = __decorate([
    Directive({
        selector: '[copilot]'
    })
], CopilotDirective);
export { CopilotDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29waWxvdC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY29waWxvdC8iLCJzb3VyY2VzIjpbImxpYi9jb3BpbG90LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUNMLGFBQWEsRUFDYixTQUFTLEVBQ1QsVUFBVSxFQUNWLEtBQUssRUFDTCxNQUFNLEVBQ04sV0FBVyxFQUNaLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBSzFELElBQWEsZ0JBQWdCLEdBQTdCLE1BQWEsZ0JBQWdCO0lBTzNCLFlBQW9CLE9BQTBCLEVBQVUsSUFBZ0I7UUFBcEQsWUFBTyxHQUFQLE9BQU8sQ0FBbUI7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFZO1FBSmpELFNBQUksR0FBRyxVQUFVLENBQUM7UUFDakIsa0JBQWEsR0FBRyxPQUFPLENBQUM7UUFDeEIsbUJBQWMsR0FBRyxFQUFFLENBQUM7SUFJNUMsQ0FBQztJQUVELFFBQVE7SUFFUixDQUFDO0lBRUQsZUFBZTtRQUViLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDMUQsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNuRCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7Z0JBQ25FLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztnQkFDckUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUN6QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7b0JBQ2YsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO29CQUN2QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7b0JBQ2YsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhO29CQUNqQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWM7aUJBQ3BDLENBQUMsQ0FBQTthQUNIO1NBQ0Y7SUFDSCxDQUFDO0NBQ0YsQ0FBQTs7WUEzQjhCLGlCQUFpQjtZQUFnQixVQUFVOztBQU5qRDtJQUF0QixLQUFLLENBQUMsY0FBYyxDQUFDOzhDQUFXO0FBQ047SUFBMUIsS0FBSyxDQUFDLGtCQUFrQixDQUFDO2tEQUE0QjtBQUMvQjtJQUF0QixLQUFLLENBQUMsY0FBYyxDQUFDOzhDQUFtQjtBQUNqQjtJQUF2QixLQUFLLENBQUMsZUFBZSxDQUFDO3VEQUF5QjtBQUN4QjtJQUF2QixLQUFLLENBQUMsZUFBZSxDQUFDO3dEQUFxQjtBQUxqQyxnQkFBZ0I7SUFINUIsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLFdBQVc7S0FDdEIsQ0FBQztHQUNXLGdCQUFnQixDQWtDNUI7U0FsQ1ksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBBZnRlclZpZXdJbml0LFxyXG4gIERpcmVjdGl2ZSxcclxuICBFbGVtZW50UmVmLFxyXG4gIElucHV0LFxyXG4gIE9uSW5pdCxcclxuICBUZW1wbGF0ZVJlZlxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZ3hDb3BpbG90U2VydmljZSB9IGZyb20gXCIuL25neC1jb3BpbG90LnNlcnZpY2VcIjtcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gIHNlbGVjdG9yOiAnW2NvcGlsb3RdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29waWxvdERpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgQElucHV0KCdjb3BpbG90LXN0ZXAnKSBzdGVwOiBhbnk7XHJcbiAgQElucHV0KCdjb3BpbG90LXRlbXBsYXRlJykgdGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XHJcbiAgQElucHV0KCdjb3BpbG90LW1vZGUnKSBtb2RlID0gJ3ZlcnRpY2FsJztcclxuICBASW5wdXQoJ2NvcGlsb3QtY29sb3InKSBvdmVydmlld2NvbG9yID0gJ2ZhbHNlJztcclxuICBASW5wdXQoJ2NvcGlsb3QtY2xhc3MnKSBhbmltYXRlZEVmZmVjdCA9ICcnO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlcnZpY2U6IE5neENvcGlsb3RTZXJ2aWNlLCBwcml2YXRlIGVsZW06IEVsZW1lbnRSZWYpIHtcclxuXHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHJcbiAgfVxyXG5cclxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XHJcblxyXG4gICAgaWYgKHRoaXMudGVtcGxhdGUpIHtcclxuICAgICAgdGhpcy5lbGVtLm5hdGl2ZUVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnbmd4LWNvcGlsb3QtaW5pdCcpO1xyXG4gICAgICBpZiAodGhpcy5zdGVwKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtLm5hdGl2ZUVsZW1lbnQuZGF0YXNldC5zdGVwID0gdGhpcy5zdGVwO1xyXG4gICAgICAgIHRoaXMuZWxlbS5uYXRpdmVFbGVtZW50LmRhdGFzZXQuY29tb2RlID0gdGhpcy5tb2RlO1xyXG4gICAgICAgIHRoaXMuZWxlbS5uYXRpdmVFbGVtZW50LmRhdGFzZXQub3ZlcnZpZXdjb2xvciA9IHRoaXMub3ZlcnZpZXdjb2xvcjtcclxuICAgICAgICB0aGlzLmVsZW0ubmF0aXZlRWxlbWVudC5kYXRhc2V0LmFuaW1hdGVkRWZmZWN0ID0gdGhpcy5hbmltYXRlZEVmZmVjdDtcclxuICAgICAgICB0aGlzLnNlcnZpY2UudGVtcGxhdGUuZW1pdCh7XHJcbiAgICAgICAgICBzdGVwOiB0aGlzLnN0ZXAsXHJcbiAgICAgICAgICB0ZW1wbGF0ZTogdGhpcy50ZW1wbGF0ZSxcclxuICAgICAgICAgIG1vZGU6IHRoaXMubW9kZSxcclxuICAgICAgICAgIG92ZXJ2aWV3Y29sb3I6IHRoaXMub3ZlcnZpZXdjb2xvcixcclxuICAgICAgICAgIGFuaW1hdGVkRWZmZWN0OiB0aGlzLmFuaW1hdGVkRWZmZWN0XHJcbiAgICAgICAgfSlcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=