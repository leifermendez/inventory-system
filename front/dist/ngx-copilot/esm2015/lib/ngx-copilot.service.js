import { __decorate } from "tslib";
import { EventEmitter, Injectable, Output } from '@angular/core';
import * as i0 from "@angular/core";
let NgxCopilotService = class NgxCopilotService {
    constructor() {
        this.template = new EventEmitter();
        this.nextEmit = new EventEmitter();
        this.tmpColor = null;
        /**
         * Private functions
         */
        this.isInViewport = (el = null) => {
            const rect = el.getBoundingClientRect();
            return {
                view: (rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth)),
                axis: rect
            };
        };
        this.scrollLocated = (element) => new Promise((resolve, reject) => {
            let countFlag = 0;
            const init = 100;
            const options = { block: 'start', behavior: 'smooth' };
            element.scrollIntoView(options);
            const id = setInterval(() => {
                countFlag++;
                const { top } = this.isInViewport(element).axis;
                const fullHeight = (window.innerHeight || document.documentElement.clientHeight);
                const percentageView = parseFloat(String(top * 100)) / fullHeight;
                if ((top < 1) || (countFlag > init)) {
                    clearInterval(id);
                    if (countFlag > init) {
                        reject(false);
                    }
                    else {
                        resolve(true);
                    }
                }
                else {
                    if (countFlag === 20) {
                        element.scrollIntoView(options);
                    }
                    else if ((countFlag > 5) && (countFlag < 15)) {
                        if ((percentageView > 10) && (percentageView < 80)) {
                            clearInterval(id);
                            resolve(true);
                        }
                    }
                }
            }, 100);
        });
        this.getParent = () => {
            try {
                this.removeWrapper();
                const copilotsElement = document.querySelectorAll(`.ngx-copilot-init`); //Elementos donde se debe hacer foco
                Array.from(copilotsElement).map((e) => {
                    const order = e.getAttribute('data-step');
                    if (order) {
                        const el = e.getBoundingClientRect();
                        const single = document.querySelector(`.copilot-view-step-${order}`); // Ubicamos el template para ubicarlo donde foco
                        const { top, right, left, bottom, width } = el;
                        single.style.marginLeft = `${width}px`;
                        single.style.top = `${top}px`;
                        single.style.right = `${right}px`;
                        single.style.bottom = `${bottom}px`;
                        single.style.left = `${left}px`;
                    }
                });
            }
            catch (e) {
                return null;
            }
        };
        this.getTemplates = (o = '1') => {
            try {
                this.elementsDomActive = document.querySelectorAll(`.copilot-view.copilot-active`);
                this.elementsDom = document.querySelectorAll(`.copilot-view`);
                if (!(this.elementsDomActive.length)) {
                    this.find(o);
                }
            }
            catch (e) {
                return null;
            }
        };
        this.checkInit = (position = '1') => {
            // window.scrollTo({ top: 0});
            this.elementsDomActive = {};
            this.elementsDom = {};
            this.tmpColor = null;
            setTimeout(() => {
                this.getParent();
                this.getTemplates(position);
            }, 60);
        };
        this.removeWrapper = () => {
            try {
                const body = document.querySelector(`body`);
                const html = document.querySelector(`html`);
                const wrapper = document.querySelector(`.ngx-wrapper-overview`);
                const list = document.querySelector(`.copilot-view`);
                const listParent = document.querySelectorAll(`.ngx-copilot-init`);
                body.classList.remove('ngx-copilot-active');
                Array.from(document.querySelectorAll(`.copilot-view`)).map((e) => {
                    if (e && e.style) {
                        e.style.display = 'none';
                    }
                });
                Array.from(document.querySelectorAll(`.ngx-copilot-pulse`)).map((e) => {
                    if (e) {
                        e.classList.remove('ngx-copilot-pulse');
                    }
                });
                Array.from(listParent).map((e) => {
                    e.style.backgroundColor = 'initial';
                });
                Array.from(list).map((e) => {
                    e.style.display = `none`;
                });
                body.classList.remove('ngx-copilot-active');
                wrapper.style.display = 'none';
                html.style.overflow = 'auto';
                body.style.overflow = 'auto';
            }
            catch (e) {
                return null;
            }
        };
        this.removeClassByPrefix = (el, prefix) => {
            const regx = new RegExp('\\b' + prefix + '.*?\\b', 'g');
            el.className = el.className.replace(regx, '');
            return el;
        };
        this.find = (order = null) => {
            try {
                const wrapper = document.querySelector(`body`);
                if (!document.querySelector(`.ngx-copilot-init[data-step='${order}']`)) {
                    const wrapperSingle = document.querySelector(`.ngx-wrapper-overview`);
                    wrapperSingle.style.display = 'none';
                    this.removeClassByPrefix(wrapper, 'ngx-copilot-');
                }
                else {
                    Array.from(document.querySelectorAll(`.copilot-view`)).map((e) => {
                        const step = e.getAttribute('step'); // Obtenemos el paso al que va el template
                        const trace = `.ngx-copilot-init[data-step='${step}']`; // Buscamos el element hacer focus
                        const single = document.querySelector(trace);
                        if (single) {
                            if (`${order}` === step) { // Si el template con el paso y el focus son el mismo mostramos
                                const { comode, overviewcolor } = single.dataset;
                                single.style.backgroundColor = '#cfceff';
                                single.classList.add('ngx-copilot-pulse');
                                wrapper.classList.add('ngx-copilot-active');
                                wrapper.classList.add(`ngx-copilot-active-step-${step}`);
                                /**
                                 * Fix perfomance
                                 */
                                const checkViewPort = this.isInViewport(single);
                                if (checkViewPort.view) { // Yes in viewport
                                    this.setZone(trace, comode, overviewcolor);
                                    e.style.display = `block`;
                                }
                                else { //Must scrolled
                                    this.scrollLocated(single).then(() => {
                                        this.setZone(trace, comode, overviewcolor);
                                        e.style.display = `block`;
                                    });
                                }
                            }
                            else {
                                single.style.backgroundColor = 'initial';
                                wrapper.classList.remove(`ngx-copilot-active-step-${step}`);
                                single.classList.remove('ngx-copilot-pulse');
                                e.style.display = `none`;
                            }
                        }
                        else {
                            single.style.backgroundColor = 'initial';
                            wrapper.classList.remove(`ngx-copilot-active-step-${step}`);
                            single.classList.remove('ngx-copilot-pulse');
                            e.style.display = `none`;
                        }
                    });
                }
            }
            catch (e) {
                return null;
            }
        };
        this.setZone = (element = null, mode = 'vertical', overviewcolor = 'false') => {
            try {
                const html = document.querySelector(`html`);
                const body = document.querySelector(`body`);
                const wrapper = document.querySelector(`.ngx-wrapper-overview`);
                html.style.overflow = 'hidden';
                body.style.overflow = 'hidden';
                wrapper.style.display = 'block';
                element = document.querySelector(element);
                const root = document.documentElement;
                const bound = element.getBoundingClientRect();
                const { top, left, right, height, bottom, width } = bound;
                const centralPointHeight = parseFloat(String(bottom - top)) / 2;
                const centralPointWidth = parseFloat(String(right - left)) / 2;
                root.style.setProperty('--zoneY', parseFloat(left + centralPointWidth) + 'px');
                root.style.setProperty('--zoneX', parseFloat(top + centralPointHeight) + 'px');
                if (overviewcolor !== 'false') {
                    root.style.setProperty('--zoneColor', overviewcolor);
                }
                else {
                    this.tmpColor = (!this.tmpColor) ? getComputedStyle(root).getPropertyValue('--zoneColor') : this.tmpColor;
                    root.style.setProperty('--zoneColor', this.tmpColor);
                }
                if (mode === 'vertical') {
                    root.style.setProperty('--zoneSize', parseFloat(height) + parseFloat(String(height * 0.1)) + 'px');
                }
                else {
                    root.style.setProperty('--zoneSize', parseFloat(width) - parseFloat(String(width * 0.5)) + 'px');
                }
            }
            catch (e) {
                return null;
            }
        };
        this.next = (data = null) => {
            this.nextEmit.emit(data);
        };
        this.checkIfExistDom = (step) => {
            try {
                const dom = document.querySelector(`.copilot-view-step-${step}`);
                return (dom);
            }
            catch (e) {
                return null;
            }
        };
    }
};
NgxCopilotService.ɵprov = i0.ɵɵdefineInjectable({ factory: function NgxCopilotService_Factory() { return new NgxCopilotService(); }, token: NgxCopilotService, providedIn: "root" });
__decorate([
    Output()
], NgxCopilotService.prototype, "template", void 0);
__decorate([
    Output()
], NgxCopilotService.prototype, "nextEmit", void 0);
NgxCopilotService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], NgxCopilotService);
export { NgxCopilotService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNvcGlsb3Quc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1jb3BpbG90LyIsInNvdXJjZXMiOlsibGliL25neC1jb3BpbG90LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQzs7QUFLL0QsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFPNUI7UUFOVSxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNuQyxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUd0QyxhQUFRLEdBQUcsSUFBSSxDQUFDO1FBTXZCOztXQUVHO1FBRUssaUJBQVksR0FBRyxDQUFDLEVBQUUsR0FBRyxJQUFJLEVBQUUsRUFBRTtZQUNuQyxNQUFNLElBQUksR0FBRyxFQUFFLENBQUMscUJBQXFCLEVBQUUsQ0FBQztZQUN4QyxPQUFPO2dCQUNMLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQztvQkFDbEIsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDO29CQUNkLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDO29CQUM1RSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUM1RSxJQUFJLEVBQUUsSUFBSTthQUNYLENBQUM7UUFDSixDQUFDLENBQUM7UUFFTSxrQkFBYSxHQUFHLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuRSxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUM7WUFDbEIsTUFBTSxJQUFJLEdBQUcsR0FBRyxDQUFDO1lBQ2pCLE1BQU0sT0FBTyxHQUFHLEVBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFDLENBQUM7WUFDckQsT0FBTyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNoQyxNQUFNLEVBQUUsR0FBRyxXQUFXLENBQUMsR0FBRyxFQUFFO2dCQUMxQixTQUFTLEVBQUUsQ0FBQztnQkFDWixNQUFNLEVBQUMsR0FBRyxFQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQzlDLE1BQU0sVUFBVSxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNqRixNQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQztnQkFDbEUsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRTtvQkFDbkMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUNsQixJQUFJLFNBQVMsR0FBRyxJQUFJLEVBQUU7d0JBQ3BCLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDZjt5QkFBTTt3QkFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2Y7aUJBQ0Y7cUJBQU07b0JBQ0wsSUFBSSxTQUFTLEtBQUssRUFBRSxFQUFFO3dCQUNwQixPQUFPLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUNqQzt5QkFBTSxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxFQUFFO3dCQUM5QyxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQyxFQUFFOzRCQUNsRCxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7NEJBQ2xCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDZjtxQkFDRjtpQkFDRjtZQUNILENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNWLENBQUMsQ0FBQyxDQUFDO1FBRUssY0FBUyxHQUFHLEdBQUcsRUFBRTtZQUN2QixJQUFJO2dCQUNGLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDckIsTUFBTSxlQUFlLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFRLENBQUMsQ0FBQyxvQ0FBb0M7Z0JBQ25ILEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBTSxFQUFFLEVBQUU7b0JBQ3pDLE1BQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzFDLElBQUksS0FBSyxFQUFFO3dCQUNULE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO3dCQUNyQyxNQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLHNCQUFzQixLQUFLLEVBQUUsQ0FBUSxDQUFDLENBQUMsZ0RBQWdEO3dCQUM3SCxNQUFNLEVBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBQyxHQUFHLEVBQUUsQ0FBQzt3QkFDN0MsTUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsR0FBRyxLQUFLLElBQUksQ0FBQzt3QkFDdkMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQzt3QkFDOUIsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxLQUFLLElBQUksQ0FBQzt3QkFDbEMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxNQUFNLElBQUksQ0FBQzt3QkFDcEMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLElBQUksQ0FBQztxQkFDakM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNWLE9BQU8sSUFBSSxDQUFDO2FBQ2I7UUFDSCxDQUFDLENBQUM7UUFFSyxpQkFBWSxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsRUFBRSxFQUFFO1lBQ2hDLElBQUk7Z0JBQ0YsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO2dCQUNuRixJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDOUQsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUNwQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNkO2FBQ0Y7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixPQUFPLElBQUksQ0FBQzthQUNiO1FBQ0gsQ0FBQyxDQUFDO1FBRUssY0FBUyxHQUFHLENBQUMsUUFBUSxHQUFHLEdBQUcsRUFBRSxFQUFFO1lBQ3BDLDhCQUE4QjtZQUM5QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlCLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNULENBQUMsQ0FBQztRQUVLLGtCQUFhLEdBQUcsR0FBRyxFQUFFO1lBQzFCLElBQUk7Z0JBQ0YsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDNUMsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDNUMsTUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBUSxDQUFDO2dCQUN2RSxNQUFNLElBQUksR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBUSxDQUFDO2dCQUM1RCxNQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDbEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsQ0FBQztnQkFDNUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFNLEVBQUUsRUFBRTtvQkFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRTt3QkFDaEIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO3FCQUMxQjtnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ3BFLElBQUksQ0FBQyxFQUFFO3dCQUNMLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUM7cUJBQ3pDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNILEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBTSxFQUFFLEVBQUU7b0JBQ3BDLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQztnQkFDdEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFNLEVBQUUsRUFBRTtvQkFDOUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO2dCQUMzQixDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dCQUM1QyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQztnQkFDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDO2FBQzlCO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsT0FBTyxJQUFJLENBQUM7YUFDYjtRQUNILENBQUMsQ0FBQztRQUVLLHdCQUFtQixHQUFHLENBQUMsRUFBRSxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzFDLE1BQU0sSUFBSSxHQUFHLElBQUksTUFBTSxDQUFDLEtBQUssR0FBRyxNQUFNLEdBQUcsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3hELEVBQUUsQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzlDLE9BQU8sRUFBRSxDQUFDO1FBQ1osQ0FBQyxDQUFBO1FBRU0sU0FBSSxHQUFHLENBQUMsS0FBSyxHQUFHLElBQUksRUFBRSxFQUFFO1lBQzdCLElBQUk7Z0JBQ0YsTUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsZ0NBQWdDLEtBQUssSUFBSSxDQUFDLEVBQUU7b0JBQ3RFLE1BQU0sYUFBYSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsdUJBQXVCLENBQVEsQ0FBQztvQkFDN0UsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO29CQUNyQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLGNBQWMsQ0FBQyxDQUFDO2lCQUNuRDtxQkFBTTtvQkFDTCxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQU0sRUFBRSxFQUFFO3dCQUNwRSxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsMENBQTBDO3dCQUMvRSxNQUFNLEtBQUssR0FBRyxnQ0FBZ0MsSUFBSSxJQUFJLENBQUMsQ0FBQyxrQ0FBa0M7d0JBQzFGLE1BQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFRLENBQUM7d0JBQ3BELElBQUksTUFBTSxFQUFFOzRCQUNWLElBQUksR0FBRyxLQUFLLEVBQUUsS0FBSyxJQUFJLEVBQUUsRUFBRSwrREFBK0Q7Z0NBQ3hGLE1BQU0sRUFBQyxNQUFNLEVBQUUsYUFBYSxFQUFDLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztnQ0FDL0MsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDO2dDQUN6QyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dDQUMxQyxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dDQUM1QyxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsSUFBSSxFQUFFLENBQUMsQ0FBQztnQ0FDekQ7O21DQUVHO2dDQUNILE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7Z0NBQ2hELElBQUksYUFBYSxDQUFDLElBQUksRUFBRSxFQUFFLGtCQUFrQjtvQ0FDMUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFDO29DQUMzQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7aUNBQzNCO3FDQUFNLEVBQUUsZUFBZTtvQ0FDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO3dDQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsYUFBYSxDQUFDLENBQUM7d0NBQzNDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztvQ0FDNUIsQ0FBQyxDQUFDLENBQUM7aUNBQ0o7NkJBQ0Y7aUNBQU07Z0NBQ0wsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDO2dDQUN6QyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQywyQkFBMkIsSUFBSSxFQUFFLENBQUMsQ0FBQztnQ0FDNUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQ0FDN0MsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDOzZCQUMxQjt5QkFDRjs2QkFBTTs0QkFDTCxNQUFNLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUM7NEJBQ3pDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLDJCQUEyQixJQUFJLEVBQUUsQ0FBQyxDQUFDOzRCQUM1RCxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDOzRCQUM3QyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7eUJBQzFCO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2FBQ0Y7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixPQUFPLElBQUksQ0FBQzthQUNiO1FBQ0gsQ0FBQyxDQUFDO1FBRUssWUFBTyxHQUFHLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxJQUFJLEdBQUcsVUFBVSxFQUFFLGFBQWEsR0FBRyxPQUFPLEVBQUUsRUFBRTtZQUM5RSxJQUFJO2dCQUNGLE1BQU0sSUFBSSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFRLENBQUM7Z0JBQ25ELE1BQU0sSUFBSSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFRLENBQUM7Z0JBQ25ELE1BQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsdUJBQXVCLENBQVEsQ0FBQztnQkFDdkUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO2dCQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7Z0JBQy9CLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztnQkFDaEMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzFDLE1BQU0sSUFBSSxHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUM7Z0JBQ3RDLE1BQU0sS0FBSyxHQUFHLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2dCQUM5QyxNQUFNLEVBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUMsR0FBRyxLQUFLLENBQUM7Z0JBQ3hELE1BQU0sa0JBQWtCLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2hFLE1BQU0saUJBQWlCLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQy9ELElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7Z0JBQy9FLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsR0FBRyxHQUFHLGtCQUFrQixDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7Z0JBQy9FLElBQUksYUFBYSxLQUFLLE9BQU8sRUFBRTtvQkFDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2lCQUN0RDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUMxRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN0RDtnQkFDRCxJQUFJLElBQUksS0FBSyxVQUFVLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQztpQkFDcEc7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO2lCQUNsRzthQUNGO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsT0FBTyxJQUFJLENBQUM7YUFDYjtRQUNILENBQUMsQ0FBQztRQUVLLFNBQUksR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLEVBQUUsRUFBRTtZQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixDQUFDLENBQUM7UUFFSyxvQkFBZSxHQUFHLENBQUMsSUFBWSxFQUFFLEVBQUU7WUFDeEMsSUFBRztnQkFDRCxNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLHNCQUFzQixJQUFJLEVBQUUsQ0FBQyxDQUFBO2dCQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUE7YUFDYjtZQUFBLE9BQU8sQ0FBQyxFQUFFO2dCQUNULE9BQU8sSUFBSSxDQUFBO2FBQ1o7UUFDSCxDQUFDLENBQUE7SUFsT0QsQ0FBQztDQW9PRixDQUFBOztBQTVPVztJQUFULE1BQU0sRUFBRTttREFBb0M7QUFDbkM7SUFBVCxNQUFNLEVBQUU7bURBQW9DO0FBRmxDLGlCQUFpQjtJQUg3QixVQUFVLENBQUM7UUFDVixVQUFVLEVBQUUsTUFBTTtLQUNuQixDQUFDO0dBQ1csaUJBQWlCLENBNk83QjtTQTdPWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0V2ZW50RW1pdHRlciwgSW5qZWN0YWJsZSwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5neENvcGlsb3RTZXJ2aWNlIHtcclxuICBAT3V0cHV0KCkgdGVtcGxhdGUgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgbmV4dEVtaXQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBwcml2YXRlIGVsZW1lbnRzRG9tQWN0aXZlOiBhbnk7XHJcbiAgcHJpdmF0ZSBlbGVtZW50c0RvbTogYW55O1xyXG4gIHB1YmxpYyB0bXBDb2xvciA9IG51bGw7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG5cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFByaXZhdGUgZnVuY3Rpb25zXHJcbiAgICovXHJcblxyXG4gIHByaXZhdGUgaXNJblZpZXdwb3J0ID0gKGVsID0gbnVsbCkgPT4ge1xyXG4gICAgY29uc3QgcmVjdCA9IGVsLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgdmlldzogKHJlY3QudG9wID49IDAgJiZcclxuICAgICAgICByZWN0LmxlZnQgPj0gMCAmJlxyXG4gICAgICAgIHJlY3QuYm90dG9tIDw9ICh3aW5kb3cuaW5uZXJIZWlnaHQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCkgJiZcclxuICAgICAgICByZWN0LnJpZ2h0IDw9ICh3aW5kb3cuaW5uZXJXaWR0aCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50V2lkdGgpKSxcclxuICAgICAgYXhpczogcmVjdFxyXG4gICAgfTtcclxuICB9O1xyXG5cclxuICBwcml2YXRlIHNjcm9sbExvY2F0ZWQgPSAoZWxlbWVudCkgPT4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgbGV0IGNvdW50RmxhZyA9IDA7XHJcbiAgICBjb25zdCBpbml0ID0gMTAwO1xyXG4gICAgY29uc3Qgb3B0aW9ucyA9IHtibG9jazogJ3N0YXJ0JywgYmVoYXZpb3I6ICdzbW9vdGgnfTtcclxuICAgIGVsZW1lbnQuc2Nyb2xsSW50b1ZpZXcob3B0aW9ucyk7XHJcbiAgICBjb25zdCBpZCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgY291bnRGbGFnKys7XHJcbiAgICAgIGNvbnN0IHt0b3B9ID0gdGhpcy5pc0luVmlld3BvcnQoZWxlbWVudCkuYXhpcztcclxuICAgICAgY29uc3QgZnVsbEhlaWdodCA9ICh3aW5kb3cuaW5uZXJIZWlnaHQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCk7XHJcbiAgICAgIGNvbnN0IHBlcmNlbnRhZ2VWaWV3ID0gcGFyc2VGbG9hdChTdHJpbmcodG9wICogMTAwKSkgLyBmdWxsSGVpZ2h0O1xyXG4gICAgICBpZiAoKHRvcCA8IDEpIHx8IChjb3VudEZsYWcgPiBpbml0KSkge1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwoaWQpO1xyXG4gICAgICAgIGlmIChjb3VudEZsYWcgPiBpbml0KSB7XHJcbiAgICAgICAgICByZWplY3QoZmFsc2UpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICByZXNvbHZlKHRydWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoY291bnRGbGFnID09PSAyMCkge1xyXG4gICAgICAgICAgZWxlbWVudC5zY3JvbGxJbnRvVmlldyhvcHRpb25zKTtcclxuICAgICAgICB9IGVsc2UgaWYgKChjb3VudEZsYWcgPiA1KSAmJiAoY291bnRGbGFnIDwgMTUpKSB7XHJcbiAgICAgICAgICBpZiAoKHBlcmNlbnRhZ2VWaWV3ID4gMTApICYmIChwZXJjZW50YWdlVmlldyA8IDgwKSkge1xyXG4gICAgICAgICAgICBjbGVhckludGVydmFsKGlkKTtcclxuICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0sIDEwMCk7XHJcbiAgfSk7XHJcblxyXG4gIHByaXZhdGUgZ2V0UGFyZW50ID0gKCkgPT4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgdGhpcy5yZW1vdmVXcmFwcGVyKCk7XHJcbiAgICAgIGNvbnN0IGNvcGlsb3RzRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5uZ3gtY29waWxvdC1pbml0YCkgYXMgYW55OyAvL0VsZW1lbnRvcyBkb25kZSBzZSBkZWJlIGhhY2VyIGZvY29cclxuICAgICAgQXJyYXkuZnJvbShjb3BpbG90c0VsZW1lbnQpLm1hcCgoZTogYW55KSA9PiB7XHJcbiAgICAgICAgY29uc3Qgb3JkZXIgPSBlLmdldEF0dHJpYnV0ZSgnZGF0YS1zdGVwJyk7XHJcbiAgICAgICAgaWYgKG9yZGVyKSB7XHJcbiAgICAgICAgICBjb25zdCBlbCA9IGUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgICAgICBjb25zdCBzaW5nbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAuY29waWxvdC12aWV3LXN0ZXAtJHtvcmRlcn1gKSBhcyBhbnk7IC8vIFViaWNhbW9zIGVsIHRlbXBsYXRlIHBhcmEgdWJpY2FybG8gZG9uZGUgZm9jb1xyXG4gICAgICAgICAgY29uc3Qge3RvcCwgcmlnaHQsIGxlZnQsIGJvdHRvbSwgd2lkdGh9ID0gZWw7XHJcbiAgICAgICAgICBzaW5nbGUuc3R5bGUubWFyZ2luTGVmdCA9IGAke3dpZHRofXB4YDtcclxuICAgICAgICAgIHNpbmdsZS5zdHlsZS50b3AgPSBgJHt0b3B9cHhgO1xyXG4gICAgICAgICAgc2luZ2xlLnN0eWxlLnJpZ2h0ID0gYCR7cmlnaHR9cHhgO1xyXG4gICAgICAgICAgc2luZ2xlLnN0eWxlLmJvdHRvbSA9IGAke2JvdHRvbX1weGA7XHJcbiAgICAgICAgICBzaW5nbGUuc3R5bGUubGVmdCA9IGAke2xlZnR9cHhgO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRUZW1wbGF0ZXMgPSAobyA9ICcxJykgPT4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgdGhpcy5lbGVtZW50c0RvbUFjdGl2ZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5jb3BpbG90LXZpZXcuY29waWxvdC1hY3RpdmVgKTtcclxuICAgICAgdGhpcy5lbGVtZW50c0RvbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5jb3BpbG90LXZpZXdgKTtcclxuICAgICAgaWYgKCEodGhpcy5lbGVtZW50c0RvbUFjdGl2ZS5sZW5ndGgpKSB7XHJcbiAgICAgICAgdGhpcy5maW5kKG8pO1xyXG4gICAgICB9XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBjaGVja0luaXQgPSAocG9zaXRpb24gPSAnMScpID0+IHtcclxuICAgIC8vIHdpbmRvdy5zY3JvbGxUbyh7IHRvcDogMH0pO1xyXG4gICAgdGhpcy5lbGVtZW50c0RvbUFjdGl2ZSA9IHt9O1xyXG4gICAgdGhpcy5lbGVtZW50c0RvbSA9IHt9O1xyXG4gICAgdGhpcy50bXBDb2xvciA9IG51bGw7XHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgdGhpcy5nZXRQYXJlbnQoKTtcclxuICAgICAgdGhpcy5nZXRUZW1wbGF0ZXMocG9zaXRpb24pO1xyXG4gICAgfSwgNjApO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyByZW1vdmVXcmFwcGVyID0gKCkgPT4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgY29uc3QgYm9keSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYGJvZHlgKTtcclxuICAgICAgY29uc3QgaHRtbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYGh0bWxgKTtcclxuICAgICAgY29uc3Qgd3JhcHBlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYC5uZ3gtd3JhcHBlci1vdmVydmlld2ApIGFzIGFueTtcclxuICAgICAgY29uc3QgbGlzdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYC5jb3BpbG90LXZpZXdgKSBhcyBhbnk7XHJcbiAgICAgIGNvbnN0IGxpc3RQYXJlbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGAubmd4LWNvcGlsb3QtaW5pdGApO1xyXG4gICAgICBib2R5LmNsYXNzTGlzdC5yZW1vdmUoJ25neC1jb3BpbG90LWFjdGl2ZScpO1xyXG4gICAgICBBcnJheS5mcm9tKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5jb3BpbG90LXZpZXdgKSkubWFwKChlOiBhbnkpID0+IHtcclxuICAgICAgICBpZiAoZSAmJiBlLnN0eWxlKSB7XHJcbiAgICAgICAgICBlLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgQXJyYXkuZnJvbShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGAubmd4LWNvcGlsb3QtcHVsc2VgKSkubWFwKChlKSA9PiB7XHJcbiAgICAgICAgaWYgKGUpIHtcclxuICAgICAgICAgIGUuY2xhc3NMaXN0LnJlbW92ZSgnbmd4LWNvcGlsb3QtcHVsc2UnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBBcnJheS5mcm9tKGxpc3RQYXJlbnQpLm1hcCgoZTogYW55KSA9PiB7XHJcbiAgICAgICAgZS5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSAnaW5pdGlhbCc7XHJcbiAgICAgIH0pO1xyXG4gICAgICBBcnJheS5mcm9tKGxpc3QpLm1hcCgoZTogYW55KSA9PiB7XHJcbiAgICAgICAgZS5zdHlsZS5kaXNwbGF5ID0gYG5vbmVgO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGJvZHkuY2xhc3NMaXN0LnJlbW92ZSgnbmd4LWNvcGlsb3QtYWN0aXZlJyk7XHJcbiAgICAgIHdyYXBwZXIuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgICAgaHRtbC5zdHlsZS5vdmVyZmxvdyA9ICdhdXRvJztcclxuICAgICAgYm9keS5zdHlsZS5vdmVyZmxvdyA9ICdhdXRvJztcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHJlbW92ZUNsYXNzQnlQcmVmaXggPSAoZWwsIHByZWZpeCkgPT4ge1xyXG4gICAgY29uc3QgcmVneCA9IG5ldyBSZWdFeHAoJ1xcXFxiJyArIHByZWZpeCArICcuKj9cXFxcYicsICdnJyk7XHJcbiAgICBlbC5jbGFzc05hbWUgPSBlbC5jbGFzc05hbWUucmVwbGFjZShyZWd4LCAnJyk7XHJcbiAgICByZXR1cm4gZWw7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZmluZCA9IChvcmRlciA9IG51bGwpID0+IHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGNvbnN0IHdyYXBwZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGBib2R5YCk7XHJcbiAgICAgIGlmICghZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLm5neC1jb3BpbG90LWluaXRbZGF0YS1zdGVwPScke29yZGVyfSddYCkpIHtcclxuICAgICAgICBjb25zdCB3cmFwcGVyU2luZ2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLm5neC13cmFwcGVyLW92ZXJ2aWV3YCkgYXMgYW55O1xyXG4gICAgICAgIHdyYXBwZXJTaW5nbGUuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgICAgICB0aGlzLnJlbW92ZUNsYXNzQnlQcmVmaXgod3JhcHBlciwgJ25neC1jb3BpbG90LScpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIEFycmF5LmZyb20oZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgLmNvcGlsb3Qtdmlld2ApKS5tYXAoKGU6IGFueSkgPT4geyAvLyBUZW1wbGF0ZXNcclxuICAgICAgICAgIGNvbnN0IHN0ZXAgPSBlLmdldEF0dHJpYnV0ZSgnc3RlcCcpOyAvLyBPYnRlbmVtb3MgZWwgcGFzbyBhbCBxdWUgdmEgZWwgdGVtcGxhdGVcclxuICAgICAgICAgIGNvbnN0IHRyYWNlID0gYC5uZ3gtY29waWxvdC1pbml0W2RhdGEtc3RlcD0nJHtzdGVwfSddYDsgLy8gQnVzY2Ftb3MgZWwgZWxlbWVudCBoYWNlciBmb2N1c1xyXG4gICAgICAgICAgY29uc3Qgc2luZ2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0cmFjZSkgYXMgYW55O1xyXG4gICAgICAgICAgaWYgKHNpbmdsZSkge1xyXG4gICAgICAgICAgICBpZiAoYCR7b3JkZXJ9YCA9PT0gc3RlcCkgeyAvLyBTaSBlbCB0ZW1wbGF0ZSBjb24gZWwgcGFzbyB5IGVsIGZvY3VzIHNvbiBlbCBtaXNtbyBtb3N0cmFtb3NcclxuICAgICAgICAgICAgICBjb25zdCB7Y29tb2RlLCBvdmVydmlld2NvbG9yfSA9IHNpbmdsZS5kYXRhc2V0O1xyXG4gICAgICAgICAgICAgIHNpbmdsZS5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSAnI2NmY2VmZic7XHJcbiAgICAgICAgICAgICAgc2luZ2xlLmNsYXNzTGlzdC5hZGQoJ25neC1jb3BpbG90LXB1bHNlJyk7XHJcbiAgICAgICAgICAgICAgd3JhcHBlci5jbGFzc0xpc3QuYWRkKCduZ3gtY29waWxvdC1hY3RpdmUnKTtcclxuICAgICAgICAgICAgICB3cmFwcGVyLmNsYXNzTGlzdC5hZGQoYG5neC1jb3BpbG90LWFjdGl2ZS1zdGVwLSR7c3RlcH1gKTtcclxuICAgICAgICAgICAgICAvKipcclxuICAgICAgICAgICAgICAgKiBGaXggcGVyZm9tYW5jZVxyXG4gICAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAgIGNvbnN0IGNoZWNrVmlld1BvcnQgPSB0aGlzLmlzSW5WaWV3cG9ydChzaW5nbGUpO1xyXG4gICAgICAgICAgICAgIGlmIChjaGVja1ZpZXdQb3J0LnZpZXcpIHsgLy8gWWVzIGluIHZpZXdwb3J0XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFpvbmUodHJhY2UsIGNvbW9kZSwgb3ZlcnZpZXdjb2xvcik7XHJcbiAgICAgICAgICAgICAgICBlLnN0eWxlLmRpc3BsYXkgPSBgYmxvY2tgO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSB7IC8vTXVzdCBzY3JvbGxlZFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zY3JvbGxMb2NhdGVkKHNpbmdsZSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuc2V0Wm9uZSh0cmFjZSwgY29tb2RlLCBvdmVydmlld2NvbG9yKTtcclxuICAgICAgICAgICAgICAgICAgZS5zdHlsZS5kaXNwbGF5ID0gYGJsb2NrYDtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBzaW5nbGUuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gJ2luaXRpYWwnO1xyXG4gICAgICAgICAgICAgIHdyYXBwZXIuY2xhc3NMaXN0LnJlbW92ZShgbmd4LWNvcGlsb3QtYWN0aXZlLXN0ZXAtJHtzdGVwfWApO1xyXG4gICAgICAgICAgICAgIHNpbmdsZS5jbGFzc0xpc3QucmVtb3ZlKCduZ3gtY29waWxvdC1wdWxzZScpO1xyXG4gICAgICAgICAgICAgIGUuc3R5bGUuZGlzcGxheSA9IGBub25lYDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2luZ2xlLnN0eWxlLmJhY2tncm91bmRDb2xvciA9ICdpbml0aWFsJztcclxuICAgICAgICAgICAgd3JhcHBlci5jbGFzc0xpc3QucmVtb3ZlKGBuZ3gtY29waWxvdC1hY3RpdmUtc3RlcC0ke3N0ZXB9YCk7XHJcbiAgICAgICAgICAgIHNpbmdsZS5jbGFzc0xpc3QucmVtb3ZlKCduZ3gtY29waWxvdC1wdWxzZScpO1xyXG4gICAgICAgICAgICBlLnN0eWxlLmRpc3BsYXkgPSBgbm9uZWA7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldFpvbmUgPSAoZWxlbWVudCA9IG51bGwsIG1vZGUgPSAndmVydGljYWwnLCBvdmVydmlld2NvbG9yID0gJ2ZhbHNlJykgPT4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgY29uc3QgaHRtbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYGh0bWxgKSBhcyBhbnk7XHJcbiAgICAgIGNvbnN0IGJvZHkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGBib2R5YCkgYXMgYW55O1xyXG4gICAgICBjb25zdCB3cmFwcGVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLm5neC13cmFwcGVyLW92ZXJ2aWV3YCkgYXMgYW55O1xyXG4gICAgICBodG1sLnN0eWxlLm92ZXJmbG93ID0gJ2hpZGRlbic7XHJcbiAgICAgIGJvZHkuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJztcclxuICAgICAgd3JhcHBlci5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcclxuICAgICAgZWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWxlbWVudCk7XHJcbiAgICAgIGNvbnN0IHJvb3QgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XHJcbiAgICAgIGNvbnN0IGJvdW5kID0gZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgICAgY29uc3Qge3RvcCwgbGVmdCwgcmlnaHQsIGhlaWdodCwgYm90dG9tLCB3aWR0aH0gPSBib3VuZDtcclxuICAgICAgY29uc3QgY2VudHJhbFBvaW50SGVpZ2h0ID0gcGFyc2VGbG9hdChTdHJpbmcoYm90dG9tIC0gdG9wKSkgLyAyO1xyXG4gICAgICBjb25zdCBjZW50cmFsUG9pbnRXaWR0aCA9IHBhcnNlRmxvYXQoU3RyaW5nKHJpZ2h0IC0gbGVmdCkpIC8gMjtcclxuICAgICAgcm9vdC5zdHlsZS5zZXRQcm9wZXJ0eSgnLS16b25lWScsIHBhcnNlRmxvYXQobGVmdCArIGNlbnRyYWxQb2ludFdpZHRoKSArICdweCcpO1xyXG4gICAgICByb290LnN0eWxlLnNldFByb3BlcnR5KCctLXpvbmVYJywgcGFyc2VGbG9hdCh0b3AgKyBjZW50cmFsUG9pbnRIZWlnaHQpICsgJ3B4Jyk7XHJcbiAgICAgIGlmIChvdmVydmlld2NvbG9yICE9PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgcm9vdC5zdHlsZS5zZXRQcm9wZXJ0eSgnLS16b25lQ29sb3InLCBvdmVydmlld2NvbG9yKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnRtcENvbG9yID0gKCF0aGlzLnRtcENvbG9yKSA/IGdldENvbXB1dGVkU3R5bGUocm9vdCkuZ2V0UHJvcGVydHlWYWx1ZSgnLS16b25lQ29sb3InKSA6IHRoaXMudG1wQ29sb3I7XHJcbiAgICAgICAgcm9vdC5zdHlsZS5zZXRQcm9wZXJ0eSgnLS16b25lQ29sb3InLCB0aGlzLnRtcENvbG9yKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAobW9kZSA9PT0gJ3ZlcnRpY2FsJykge1xyXG4gICAgICAgIHJvb3Quc3R5bGUuc2V0UHJvcGVydHkoJy0tem9uZVNpemUnLCBwYXJzZUZsb2F0KGhlaWdodCkgKyBwYXJzZUZsb2F0KFN0cmluZyhoZWlnaHQgKiAwLjEpKSArICdweCcpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJvb3Quc3R5bGUuc2V0UHJvcGVydHkoJy0tem9uZVNpemUnLCBwYXJzZUZsb2F0KHdpZHRoKSAtIHBhcnNlRmxvYXQoU3RyaW5nKHdpZHRoICogMC41KSkgKyAncHgnKTtcclxuICAgICAgfVxyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBwdWJsaWMgbmV4dCA9IChkYXRhID0gbnVsbCkgPT4ge1xyXG4gICAgdGhpcy5uZXh0RW1pdC5lbWl0KGRhdGEpO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBjaGVja0lmRXhpc3REb20gPSAoc3RlcDogc3RyaW5nKSA9PiB7XHJcbiAgICB0cnl7XHJcbiAgICAgIGNvbnN0IGRvbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYC5jb3BpbG90LXZpZXctc3RlcC0ke3N0ZXB9YClcclxuICAgICAgcmV0dXJuIChkb20pXHJcbiAgICB9Y2F0Y2ggKGUpIHtcclxuICAgICAgcmV0dXJuIG51bGxcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==