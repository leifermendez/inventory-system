(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('ngx-copilot', ['exports', '@angular/core', '@angular/common'], factory) :
    (global = global || self, factory(global['ngx-copilot'] = {}, global.ng.core, global.ng.common));
}(this, (function (exports, core, common) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var NgxCopilotService = /** @class */ (function () {
        function NgxCopilotService() {
            var _this = this;
            this.template = new core.EventEmitter();
            this.nextEmit = new core.EventEmitter();
            this.tmpColor = null;
            /**
             * Private functions
             */
            this.isInViewport = function (el) {
                if (el === void 0) { el = null; }
                var rect = el.getBoundingClientRect();
                return {
                    view: (rect.top >= 0 &&
                        rect.left >= 0 &&
                        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                        rect.right <= (window.innerWidth || document.documentElement.clientWidth)),
                    axis: rect
                };
            };
            this.scrollLocated = function (element) { return new Promise(function (resolve, reject) {
                var countFlag = 0;
                var init = 100;
                var options = { block: 'start', behavior: 'smooth' };
                element.scrollIntoView(options);
                var id = setInterval(function () {
                    countFlag++;
                    var top = _this.isInViewport(element).axis.top;
                    var fullHeight = (window.innerHeight || document.documentElement.clientHeight);
                    var percentageView = parseFloat(String(top * 100)) / fullHeight;
                    if ((top < 1) || (countFlag > init)) {
                        clearInterval(id);
                        if (countFlag > init) {
                            reject(false);
                        }
                        else {
                            resolve(true);
                        }
                    }
                    else {
                        if (countFlag === 20) {
                            element.scrollIntoView(options);
                        }
                        else if ((countFlag > 5) && (countFlag < 15)) {
                            if ((percentageView > 10) && (percentageView < 80)) {
                                clearInterval(id);
                                resolve(true);
                            }
                        }
                    }
                }, 100);
            }); };
            this.getParent = function () {
                try {
                    _this.removeWrapper();
                    var copilotsElement = document.querySelectorAll(".ngx-copilot-init"); //Elementos donde se debe hacer foco
                    Array.from(copilotsElement).map(function (e) {
                        var order = e.getAttribute('data-step');
                        if (order) {
                            var el = e.getBoundingClientRect();
                            var single = document.querySelector(".copilot-view-step-" + order); // Ubicamos el template para ubicarlo donde foco
                            var top_1 = el.top, right = el.right, left = el.left, bottom = el.bottom, width = el.width;
                            single.style.marginLeft = width + "px";
                            single.style.top = top_1 + "px";
                            single.style.right = right + "px";
                            single.style.bottom = bottom + "px";
                            single.style.left = left + "px";
                        }
                    });
                }
                catch (e) {
                    return null;
                }
            };
            this.getTemplates = function (o) {
                if (o === void 0) { o = '1'; }
                try {
                    _this.elementsDomActive = document.querySelectorAll(".copilot-view.copilot-active");
                    _this.elementsDom = document.querySelectorAll(".copilot-view");
                    if (!(_this.elementsDomActive.length)) {
                        _this.find(o);
                    }
                }
                catch (e) {
                    return null;
                }
            };
            this.checkInit = function (position) {
                if (position === void 0) { position = '1'; }
                // window.scrollTo({ top: 0});
                _this.elementsDomActive = {};
                _this.elementsDom = {};
                _this.tmpColor = null;
                setTimeout(function () {
                    _this.getParent();
                    _this.getTemplates(position);
                }, 60);
            };
            this.removeWrapper = function () {
                try {
                    var body = document.querySelector("body");
                    var html = document.querySelector("html");
                    var wrapper = document.querySelector(".ngx-wrapper-overview");
                    var list = document.querySelector(".copilot-view");
                    var listParent = document.querySelectorAll(".ngx-copilot-init");
                    body.classList.remove('ngx-copilot-active');
                    Array.from(document.querySelectorAll(".copilot-view")).map(function (e) {
                        if (e && e.style) {
                            e.style.display = 'none';
                        }
                    });
                    Array.from(document.querySelectorAll(".ngx-copilot-pulse")).map(function (e) {
                        if (e) {
                            e.classList.remove('ngx-copilot-pulse');
                        }
                    });
                    Array.from(listParent).map(function (e) {
                        e.style.backgroundColor = 'initial';
                    });
                    Array.from(list).map(function (e) {
                        e.style.display = "none";
                    });
                    body.classList.remove('ngx-copilot-active');
                    wrapper.style.display = 'none';
                    html.style.overflow = 'auto';
                    body.style.overflow = 'auto';
                }
                catch (e) {
                    return null;
                }
            };
            this.removeClassByPrefix = function (el, prefix) {
                var regx = new RegExp('\\b' + prefix + '.*?\\b', 'g');
                el.className = el.className.replace(regx, '');
                return el;
            };
            this.find = function (order) {
                if (order === void 0) { order = null; }
                try {
                    var wrapper_1 = document.querySelector("body");
                    if (!document.querySelector(".ngx-copilot-init[data-step='" + order + "']")) {
                        var wrapperSingle = document.querySelector(".ngx-wrapper-overview");
                        wrapperSingle.style.display = 'none';
                        _this.removeClassByPrefix(wrapper_1, 'ngx-copilot-');
                    }
                    else {
                        Array.from(document.querySelectorAll(".copilot-view")).map(function (e) {
                            var step = e.getAttribute('step'); // Obtenemos el paso al que va el template
                            var trace = ".ngx-copilot-init[data-step='" + step + "']"; // Buscamos el element hacer focus
                            var single = document.querySelector(trace);
                            if (single) {
                                if ("" + order === step) { // Si el template con el paso y el focus son el mismo mostramos
                                    var _a = single.dataset, comode_1 = _a.comode, overviewcolor_1 = _a.overviewcolor;
                                    single.style.backgroundColor = '#cfceff';
                                    single.classList.add('ngx-copilot-pulse');
                                    wrapper_1.classList.add('ngx-copilot-active');
                                    wrapper_1.classList.add("ngx-copilot-active-step-" + step);
                                    /**
                                     * Fix perfomance
                                     */
                                    var checkViewPort = _this.isInViewport(single);
                                    if (checkViewPort.view) { // Yes in viewport
                                        _this.setZone(trace, comode_1, overviewcolor_1);
                                        e.style.display = "block";
                                    }
                                    else { //Must scrolled
                                        _this.scrollLocated(single).then(function () {
                                            _this.setZone(trace, comode_1, overviewcolor_1);
                                            e.style.display = "block";
                                        });
                                    }
                                }
                                else {
                                    single.style.backgroundColor = 'initial';
                                    wrapper_1.classList.remove("ngx-copilot-active-step-" + step);
                                    single.classList.remove('ngx-copilot-pulse');
                                    e.style.display = "none";
                                }
                            }
                            else {
                                single.style.backgroundColor = 'initial';
                                wrapper_1.classList.remove("ngx-copilot-active-step-" + step);
                                single.classList.remove('ngx-copilot-pulse');
                                e.style.display = "none";
                            }
                        });
                    }
                }
                catch (e) {
                    return null;
                }
            };
            this.setZone = function (element, mode, overviewcolor) {
                if (element === void 0) { element = null; }
                if (mode === void 0) { mode = 'vertical'; }
                if (overviewcolor === void 0) { overviewcolor = 'false'; }
                try {
                    var html = document.querySelector("html");
                    var body = document.querySelector("body");
                    var wrapper = document.querySelector(".ngx-wrapper-overview");
                    html.style.overflow = 'hidden';
                    body.style.overflow = 'hidden';
                    wrapper.style.display = 'block';
                    element = document.querySelector(element);
                    var root = document.documentElement;
                    var bound = element.getBoundingClientRect();
                    var top_2 = bound.top, left = bound.left, right = bound.right, height = bound.height, bottom = bound.bottom, width = bound.width;
                    var centralPointHeight = parseFloat(String(bottom - top_2)) / 2;
                    var centralPointWidth = parseFloat(String(right - left)) / 2;
                    root.style.setProperty('--zoneY', parseFloat(left + centralPointWidth) + 'px');
                    root.style.setProperty('--zoneX', parseFloat(top_2 + centralPointHeight) + 'px');
                    if (overviewcolor !== 'false') {
                        root.style.setProperty('--zoneColor', overviewcolor);
                    }
                    else {
                        _this.tmpColor = (!_this.tmpColor) ? getComputedStyle(root).getPropertyValue('--zoneColor') : _this.tmpColor;
                        root.style.setProperty('--zoneColor', _this.tmpColor);
                    }
                    if (mode === 'vertical') {
                        root.style.setProperty('--zoneSize', parseFloat(height) + parseFloat(String(height * 0.1)) + 'px');
                    }
                    else {
                        root.style.setProperty('--zoneSize', parseFloat(width) - parseFloat(String(width * 0.5)) + 'px');
                    }
                }
                catch (e) {
                    return null;
                }
            };
            this.next = function (data) {
                if (data === void 0) { data = null; }
                _this.nextEmit.emit(data);
            };
            this.checkIfExistDom = function (step) {
                try {
                    var dom = document.querySelector(".copilot-view-step-" + step);
                    return (dom);
                }
                catch (e) {
                    return null;
                }
            };
        }
        NgxCopilotService.ɵprov = core.ɵɵdefineInjectable({ factory: function NgxCopilotService_Factory() { return new NgxCopilotService(); }, token: NgxCopilotService, providedIn: "root" });
        __decorate([
            core.Output()
        ], NgxCopilotService.prototype, "template", void 0);
        __decorate([
            core.Output()
        ], NgxCopilotService.prototype, "nextEmit", void 0);
        NgxCopilotService = __decorate([
            core.Injectable({
                providedIn: 'root'
            })
        ], NgxCopilotService);
        return NgxCopilotService;
    }());

    var NgxCopilotComponent = /** @class */ (function () {
        function NgxCopilotComponent() {
        }
        NgxCopilotComponent.prototype.ngOnInit = function () {
        };
        NgxCopilotComponent = __decorate([
            core.Component({
                selector: 'lib-ngx-copilot',
                template: "\n    <p>\n      ngx-copilot works!\n    </p>\n  "
            })
        ], NgxCopilotComponent);
        return NgxCopilotComponent;
    }());

    var CopilotDirective = /** @class */ (function () {
        function CopilotDirective(service, elem) {
            this.service = service;
            this.elem = elem;
            this.mode = 'vertical';
            this.overviewcolor = 'false';
            this.animatedEffect = '';
        }
        CopilotDirective.prototype.ngOnInit = function () {
        };
        CopilotDirective.prototype.ngAfterViewInit = function () {
            if (this.template) {
                this.elem.nativeElement.classList.add('ngx-copilot-init');
                if (this.step) {
                    this.elem.nativeElement.dataset.step = this.step;
                    this.elem.nativeElement.dataset.comode = this.mode;
                    this.elem.nativeElement.dataset.overviewcolor = this.overviewcolor;
                    this.elem.nativeElement.dataset.animatedEffect = this.animatedEffect;
                    this.service.template.emit({
                        step: this.step,
                        template: this.template,
                        mode: this.mode,
                        overviewcolor: this.overviewcolor,
                        animatedEffect: this.animatedEffect
                    });
                }
            }
        };
        CopilotDirective.ctorParameters = function () { return [
            { type: NgxCopilotService },
            { type: core.ElementRef }
        ]; };
        __decorate([
            core.Input('copilot-step')
        ], CopilotDirective.prototype, "step", void 0);
        __decorate([
            core.Input('copilot-template')
        ], CopilotDirective.prototype, "template", void 0);
        __decorate([
            core.Input('copilot-mode')
        ], CopilotDirective.prototype, "mode", void 0);
        __decorate([
            core.Input('copilot-color')
        ], CopilotDirective.prototype, "overviewcolor", void 0);
        __decorate([
            core.Input('copilot-class')
        ], CopilotDirective.prototype, "animatedEffect", void 0);
        CopilotDirective = __decorate([
            core.Directive({
                selector: '[copilot]'
            })
        ], CopilotDirective);
        return CopilotDirective;
    }());

    var NgxWrapperCopilotComponent = /** @class */ (function () {
        function NgxWrapperCopilotComponent(service) {
            this.service = service;
            this.viewTemplate = [];
        }
        NgxWrapperCopilotComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.subscriber = this.service.template.subscribe(function (a) {
                if (!_this.service.checkIfExistDom(a.step)) {
                    _this.viewTemplate.push(a);
                }
            });
            this.service.nextEmit.subscribe(function (next) { return _this.service.find(next); });
        };
        NgxWrapperCopilotComponent.prototype.ngOnDestroy = function () {
            try {
                this.subscriber.unsubscribe();
                this.viewTemplate = [];
            }
            catch (e) {
                this.viewTemplate = [];
            }
        };
        NgxWrapperCopilotComponent.ctorParameters = function () { return [
            { type: NgxCopilotService }
        ]; };
        NgxWrapperCopilotComponent = __decorate([
            core.Component({
                selector: 'ngx-wrapper-copilot',
                template: "<div class=\"ngx-wrapper-overview\">\r\n  <ng-container *ngFor=\"let te  of viewTemplate\" >\r\n    <div [attr.step]=\"te?.step\"\r\n         class=\"{{(!te?.animatedEffect?.length) ? 'animate__animated animate__bounceIn': te?.animatedEffect}}\r\n     copilot-view copilot-view-step-{{te?.step}}\">\r\n\r\n      <ng-container *ngTemplateOutlet=\"te?.template\"></ng-container>\r\n    </div>\r\n  </ng-container>\r\n</div>\r\n",
                styles: [".ngx-wrapper-overview{display:none;position:fixed;z-index:9999;width:100%;height:100%;top:0;left:0}.ngx-wrapper-overview .copilot-view{position:fixed;max-width:300px;max-height:-webkit-min-content;max-height:-moz-min-content;max-height:min-content;padding:10px;border-radius:10px;background-color:#6b66ff;color:#fff;display:none}.ngx-copilot-pulse{display:block;border-radius:50%;background:#cca92c;cursor:pointer;box-shadow:0 0 0 rgba(204,169,44,.8);animation:2s infinite ngx-copilot-pulse}.ngx-copilot-pulse:hover{animation:none}@keyframes ngx-copilot-pulse{0%{box-shadow:0 0 0 0 rgba(204,169,44,.4)}70%{box-shadow:0 0 0 10px rgba(204,169,44,0)}100%{box-shadow:0 0 0 0 rgba(204,169,44,0)}}"]
            })
        ], NgxWrapperCopilotComponent);
        return NgxWrapperCopilotComponent;
    }());

    var NgxCopilotModule = /** @class */ (function () {
        function NgxCopilotModule() {
        }
        NgxCopilotModule = __decorate([
            core.NgModule({
                declarations: [NgxCopilotComponent, CopilotDirective, NgxWrapperCopilotComponent],
                imports: [
                    common.CommonModule
                ],
                exports: [NgxCopilotComponent, CopilotDirective, NgxWrapperCopilotComponent]
            })
        ], NgxCopilotModule);
        return NgxCopilotModule;
    }());

    exports.CopilotDirective = CopilotDirective;
    exports.NgxCopilotComponent = NgxCopilotComponent;
    exports.NgxCopilotModule = NgxCopilotModule;
    exports.NgxCopilotService = NgxCopilotService;
    exports.NgxWrapperCopilotComponent = NgxWrapperCopilotComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ngx-copilot.umd.js.map
