import { __decorate } from 'tslib';
import { EventEmitter, ɵɵdefineInjectable, Output, Injectable, Component, ElementRef, Input, Directive, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

let NgxCopilotService = class NgxCopilotService {
    constructor() {
        this.template = new EventEmitter();
        this.nextEmit = new EventEmitter();
        this.tmpColor = null;
        /**
         * Private functions
         */
        this.isInViewport = (el = null) => {
            const rect = el.getBoundingClientRect();
            return {
                view: (rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth)),
                axis: rect
            };
        };
        this.scrollLocated = (element) => new Promise((resolve, reject) => {
            let countFlag = 0;
            const init = 100;
            const options = { block: 'start', behavior: 'smooth' };
            element.scrollIntoView(options);
            const id = setInterval(() => {
                countFlag++;
                const { top } = this.isInViewport(element).axis;
                const fullHeight = (window.innerHeight || document.documentElement.clientHeight);
                const percentageView = parseFloat(String(top * 100)) / fullHeight;
                if ((top < 1) || (countFlag > init)) {
                    clearInterval(id);
                    if (countFlag > init) {
                        reject(false);
                    }
                    else {
                        resolve(true);
                    }
                }
                else {
                    if (countFlag === 20) {
                        element.scrollIntoView(options);
                    }
                    else if ((countFlag > 5) && (countFlag < 15)) {
                        if ((percentageView > 10) && (percentageView < 80)) {
                            clearInterval(id);
                            resolve(true);
                        }
                    }
                }
            }, 100);
        });
        this.getParent = () => {
            try {
                this.removeWrapper();
                const copilotsElement = document.querySelectorAll(`.ngx-copilot-init`); //Elementos donde se debe hacer foco
                Array.from(copilotsElement).map((e) => {
                    const order = e.getAttribute('data-step');
                    if (order) {
                        const el = e.getBoundingClientRect();
                        const single = document.querySelector(`.copilot-view-step-${order}`); // Ubicamos el template para ubicarlo donde foco
                        const { top, right, left, bottom, width } = el;
                        single.style.marginLeft = `${width}px`;
                        single.style.top = `${top}px`;
                        single.style.right = `${right}px`;
                        single.style.bottom = `${bottom}px`;
                        single.style.left = `${left}px`;
                    }
                });
            }
            catch (e) {
                return null;
            }
        };
        this.getTemplates = (o = '1') => {
            try {
                this.elementsDomActive = document.querySelectorAll(`.copilot-view.copilot-active`);
                this.elementsDom = document.querySelectorAll(`.copilot-view`);
                if (!(this.elementsDomActive.length)) {
                    this.find(o);
                }
            }
            catch (e) {
                return null;
            }
        };
        this.checkInit = (position = '1') => {
            // window.scrollTo({ top: 0});
            this.elementsDomActive = {};
            this.elementsDom = {};
            this.tmpColor = null;
            setTimeout(() => {
                this.getParent();
                this.getTemplates(position);
            }, 60);
        };
        this.removeWrapper = () => {
            try {
                const body = document.querySelector(`body`);
                const html = document.querySelector(`html`);
                const wrapper = document.querySelector(`.ngx-wrapper-overview`);
                const list = document.querySelector(`.copilot-view`);
                const listParent = document.querySelectorAll(`.ngx-copilot-init`);
                body.classList.remove('ngx-copilot-active');
                Array.from(document.querySelectorAll(`.copilot-view`)).map((e) => {
                    if (e && e.style) {
                        e.style.display = 'none';
                    }
                });
                Array.from(document.querySelectorAll(`.ngx-copilot-pulse`)).map((e) => {
                    if (e) {
                        e.classList.remove('ngx-copilot-pulse');
                    }
                });
                Array.from(listParent).map((e) => {
                    e.style.backgroundColor = 'initial';
                });
                Array.from(list).map((e) => {
                    e.style.display = `none`;
                });
                body.classList.remove('ngx-copilot-active');
                wrapper.style.display = 'none';
                html.style.overflow = 'auto';
                body.style.overflow = 'auto';
            }
            catch (e) {
                return null;
            }
        };
        this.removeClassByPrefix = (el, prefix) => {
            const regx = new RegExp('\\b' + prefix + '.*?\\b', 'g');
            el.className = el.className.replace(regx, '');
            return el;
        };
        this.find = (order = null) => {
            try {
                const wrapper = document.querySelector(`body`);
                if (!document.querySelector(`.ngx-copilot-init[data-step='${order}']`)) {
                    const wrapperSingle = document.querySelector(`.ngx-wrapper-overview`);
                    wrapperSingle.style.display = 'none';
                    this.removeClassByPrefix(wrapper, 'ngx-copilot-');
                }
                else {
                    Array.from(document.querySelectorAll(`.copilot-view`)).map((e) => {
                        const step = e.getAttribute('step'); // Obtenemos el paso al que va el template
                        const trace = `.ngx-copilot-init[data-step='${step}']`; // Buscamos el element hacer focus
                        const single = document.querySelector(trace);
                        if (single) {
                            if (`${order}` === step) { // Si el template con el paso y el focus son el mismo mostramos
                                const { comode, overviewcolor } = single.dataset;
                                single.style.backgroundColor = '#cfceff';
                                single.classList.add('ngx-copilot-pulse');
                                wrapper.classList.add('ngx-copilot-active');
                                wrapper.classList.add(`ngx-copilot-active-step-${step}`);
                                /**
                                 * Fix perfomance
                                 */
                                const checkViewPort = this.isInViewport(single);
                                if (checkViewPort.view) { // Yes in viewport
                                    this.setZone(trace, comode, overviewcolor);
                                    e.style.display = `block`;
                                }
                                else { //Must scrolled
                                    this.scrollLocated(single).then(() => {
                                        this.setZone(trace, comode, overviewcolor);
                                        e.style.display = `block`;
                                    });
                                }
                            }
                            else {
                                single.style.backgroundColor = 'initial';
                                wrapper.classList.remove(`ngx-copilot-active-step-${step}`);
                                single.classList.remove('ngx-copilot-pulse');
                                e.style.display = `none`;
                            }
                        }
                        else {
                            single.style.backgroundColor = 'initial';
                            wrapper.classList.remove(`ngx-copilot-active-step-${step}`);
                            single.classList.remove('ngx-copilot-pulse');
                            e.style.display = `none`;
                        }
                    });
                }
            }
            catch (e) {
                return null;
            }
        };
        this.setZone = (element = null, mode = 'vertical', overviewcolor = 'false') => {
            try {
                const html = document.querySelector(`html`);
                const body = document.querySelector(`body`);
                const wrapper = document.querySelector(`.ngx-wrapper-overview`);
                html.style.overflow = 'hidden';
                body.style.overflow = 'hidden';
                wrapper.style.display = 'block';
                element = document.querySelector(element);
                const root = document.documentElement;
                const bound = element.getBoundingClientRect();
                const { top, left, right, height, bottom, width } = bound;
                const centralPointHeight = parseFloat(String(bottom - top)) / 2;
                const centralPointWidth = parseFloat(String(right - left)) / 2;
                root.style.setProperty('--zoneY', parseFloat(left + centralPointWidth) + 'px');
                root.style.setProperty('--zoneX', parseFloat(top + centralPointHeight) + 'px');
                if (overviewcolor !== 'false') {
                    root.style.setProperty('--zoneColor', overviewcolor);
                }
                else {
                    this.tmpColor = (!this.tmpColor) ? getComputedStyle(root).getPropertyValue('--zoneColor') : this.tmpColor;
                    root.style.setProperty('--zoneColor', this.tmpColor);
                }
                if (mode === 'vertical') {
                    root.style.setProperty('--zoneSize', parseFloat(height) + parseFloat(String(height * 0.1)) + 'px');
                }
                else {
                    root.style.setProperty('--zoneSize', parseFloat(width) - parseFloat(String(width * 0.5)) + 'px');
                }
            }
            catch (e) {
                return null;
            }
        };
        this.next = (data = null) => {
            this.nextEmit.emit(data);
        };
        this.checkIfExistDom = (step) => {
            try {
                const dom = document.querySelector(`.copilot-view-step-${step}`);
                return (dom);
            }
            catch (e) {
                return null;
            }
        };
    }
};
NgxCopilotService.ɵprov = ɵɵdefineInjectable({ factory: function NgxCopilotService_Factory() { return new NgxCopilotService(); }, token: NgxCopilotService, providedIn: "root" });
__decorate([
    Output()
], NgxCopilotService.prototype, "template", void 0);
__decorate([
    Output()
], NgxCopilotService.prototype, "nextEmit", void 0);
NgxCopilotService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], NgxCopilotService);

let NgxCopilotComponent = class NgxCopilotComponent {
    constructor() { }
    ngOnInit() {
    }
};
NgxCopilotComponent = __decorate([
    Component({
        selector: 'lib-ngx-copilot',
        template: `
    <p>
      ngx-copilot works!
    </p>
  `
    })
], NgxCopilotComponent);

let CopilotDirective = class CopilotDirective {
    constructor(service, elem) {
        this.service = service;
        this.elem = elem;
        this.mode = 'vertical';
        this.overviewcolor = 'false';
        this.animatedEffect = '';
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        if (this.template) {
            this.elem.nativeElement.classList.add('ngx-copilot-init');
            if (this.step) {
                this.elem.nativeElement.dataset.step = this.step;
                this.elem.nativeElement.dataset.comode = this.mode;
                this.elem.nativeElement.dataset.overviewcolor = this.overviewcolor;
                this.elem.nativeElement.dataset.animatedEffect = this.animatedEffect;
                this.service.template.emit({
                    step: this.step,
                    template: this.template,
                    mode: this.mode,
                    overviewcolor: this.overviewcolor,
                    animatedEffect: this.animatedEffect
                });
            }
        }
    }
};
CopilotDirective.ctorParameters = () => [
    { type: NgxCopilotService },
    { type: ElementRef }
];
__decorate([
    Input('copilot-step')
], CopilotDirective.prototype, "step", void 0);
__decorate([
    Input('copilot-template')
], CopilotDirective.prototype, "template", void 0);
__decorate([
    Input('copilot-mode')
], CopilotDirective.prototype, "mode", void 0);
__decorate([
    Input('copilot-color')
], CopilotDirective.prototype, "overviewcolor", void 0);
__decorate([
    Input('copilot-class')
], CopilotDirective.prototype, "animatedEffect", void 0);
CopilotDirective = __decorate([
    Directive({
        selector: '[copilot]'
    })
], CopilotDirective);

let NgxWrapperCopilotComponent = class NgxWrapperCopilotComponent {
    constructor(service) {
        this.service = service;
        this.viewTemplate = [];
    }
    ngOnInit() {
        this.subscriber = this.service.template.subscribe(a => {
            if (!this.service.checkIfExistDom(a.step)) {
                this.viewTemplate.push(a);
            }
        });
        this.service.nextEmit.subscribe(next => this.service.find(next));
    }
    ngOnDestroy() {
        try {
            this.subscriber.unsubscribe();
            this.viewTemplate = [];
        }
        catch (e) {
            this.viewTemplate = [];
        }
    }
};
NgxWrapperCopilotComponent.ctorParameters = () => [
    { type: NgxCopilotService }
];
NgxWrapperCopilotComponent = __decorate([
    Component({
        selector: 'ngx-wrapper-copilot',
        template: "<div class=\"ngx-wrapper-overview\">\r\n  <ng-container *ngFor=\"let te  of viewTemplate\" >\r\n    <div [attr.step]=\"te?.step\"\r\n         class=\"{{(!te?.animatedEffect?.length) ? 'animate__animated animate__bounceIn': te?.animatedEffect}}\r\n     copilot-view copilot-view-step-{{te?.step}}\">\r\n\r\n      <ng-container *ngTemplateOutlet=\"te?.template\"></ng-container>\r\n    </div>\r\n  </ng-container>\r\n</div>\r\n",
        styles: [".ngx-wrapper-overview{display:none;position:fixed;z-index:9999;width:100%;height:100%;top:0;left:0}.ngx-wrapper-overview .copilot-view{position:fixed;max-width:300px;max-height:-webkit-min-content;max-height:-moz-min-content;max-height:min-content;padding:10px;border-radius:10px;background-color:#6b66ff;color:#fff;display:none}.ngx-copilot-pulse{display:block;border-radius:50%;background:#cca92c;cursor:pointer;box-shadow:0 0 0 rgba(204,169,44,.8);animation:2s infinite ngx-copilot-pulse}.ngx-copilot-pulse:hover{animation:none}@keyframes ngx-copilot-pulse{0%{box-shadow:0 0 0 0 rgba(204,169,44,.4)}70%{box-shadow:0 0 0 10px rgba(204,169,44,0)}100%{box-shadow:0 0 0 0 rgba(204,169,44,0)}}"]
    })
], NgxWrapperCopilotComponent);

let NgxCopilotModule = class NgxCopilotModule {
};
NgxCopilotModule = __decorate([
    NgModule({
        declarations: [NgxCopilotComponent, CopilotDirective, NgxWrapperCopilotComponent],
        imports: [
            CommonModule
        ],
        exports: [NgxCopilotComponent, CopilotDirective, NgxWrapperCopilotComponent]
    })
], NgxCopilotModule);

/*
 * Public API Surface of ngx-copilot
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CopilotDirective, NgxCopilotComponent, NgxCopilotModule, NgxCopilotService, NgxWrapperCopilotComponent };
//# sourceMappingURL=ngx-copilot.js.map
