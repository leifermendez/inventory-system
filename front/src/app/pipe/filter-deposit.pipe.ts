import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filterDeposit'
})
export class FilterDepositPipe implements PipeTransform {

  transform(value: any, args: any): unknown {
    try {
      const raw = value;
      args = args.find(() => true)
      return raw.find(a => args.includes(a.name));
    } catch (e) {
      return {
        qty: 0
      }
    }
  }

}
