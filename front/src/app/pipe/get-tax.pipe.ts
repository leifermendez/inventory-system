import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'getTax'
})
export class GetTaxPipe implements PipeTransform {

  transform(value: any, args: any): unknown {
    try {
      return parseFloat(String(value * parseFloat(String(args / 100)))).toFixed(2)
    } catch (e) {
      return null
    }
  }

}
