import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'invoicePlaceHolder'
})
export class InvoicePlaceHolderPipe implements PipeTransform {

  transform(value: any | string, args: string = ''): unknown {
    try {
      args = args.toString();
      value = value.replace(/%/gi, '0');
      const numberAmount = value.split('').join('');
      const firstPart = numberAmount.slice(0, -args.length)
      const numberZero = value.split('').filter(a => a === '0').length;
      if (numberZero < args.length) {
        return 'Formato no válido';
      }
      return `${firstPart}${args}`;
    } catch (e) {
      return null
    }
  }

}
