import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'firstValue'
})
export class FirstValuePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): any {
    try {
      // @ts-ignore
      const firstElement = value.find(() => true)
      if (args.length === 2) {
        // @ts-ignore
        return parseFloat(String(firstElement[args[0]] * args[1].qty)) || 0
      } else {
        return parseFloat(firstElement[args[0]]) || 0
      }
    } catch (e) {
      return 0;
    }
  }

}
