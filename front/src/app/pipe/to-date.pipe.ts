import { Pipe, PipeTransform } from '@angular/core';
import * as  moment from 'moment';
@Pipe({
  name: 'toDate'
})
export class ToDatePipe implements PipeTransform {

  transform(value: unknown, args: string): unknown {
    try {
      const momentParse = moment(value, args)
      console.log(momentParse.isValid())
      return momentParse.isValid() ? momentParse : null
    } catch (e) {
      return null;
    }
  }

}
