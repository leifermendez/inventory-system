import {Component, OnInit} from '@angular/core';
import {AnimationOptions} from "ngx-lottie";
import {RestService} from "../../services/rest.service";
import {OwlOptions} from "ngx-owl-carousel-o";
import {
  faAngleRight,
  faEllipsisV,
  faPlug
} from '@fortawesome/free-solid-svg-icons';
import {BsModalRef} from "ngx-bootstrap/modal";
import {Router} from "@angular/router";

@Component({
  selector: 'app-modal-view-add',
  templateUrl: './modal-view-add.component.html',
  styleUrls: ['./modal-view-add.component.css']
})
export class ModalViewAddComponent implements OnInit {
  public data: any;
  public dataLoad: any;
  options: AnimationOptions = {
    path: '/assets/images/404.json',
  };

  faPlug = faPlug;
  public gallery: any[];

  constructor(private rest: RestService, public bsModalRef: BsModalRef, private router: Router) {
  }

  ngOnInit(): void {
    this.load()
    this.gallery = [
      {
        source: ''
      }
    ]
  }

  close = () => this.bsModalRef.hide()

  load = () => {
    this.rest.get(`plugins/${this.data._id}`).subscribe(res => {
      this.dataLoad = res;
      this.view = !this.view;
    })
  }
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 2
      },
      940: {
        items: 2
      }
    },
    nav: false
  }
  view: any = false;

  active = () => {
    this.rest.post(`plugins/${this.dataLoad.item._id}/active`).subscribe(res => {
      this.router.navigate(['/', 'add-ons', this.dataLoad.item._id])
      this.close()
    })
  }

  disabled = () => {
    this.rest.delete(`plugins/${this.dataLoad.item._id}/disabled`).subscribe(res => {
      this.close()
    })
  }

}
