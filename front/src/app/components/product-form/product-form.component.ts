import {Component, ElementRef, Input, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  faQuestionCircle,
  faSave,
  faCheckCircle,
  faTrashAlt,
  faHandPointer
} from '@fortawesome/free-regular-svg-icons';
import {faAngleRight, faWarehouse, faPlus} from '@fortawesome/free-solid-svg-icons';
import {AnimationOptions} from 'ngx-lottie';
import {CurrencyMaskInputMode} from 'ngx-currency';
import {RestService} from '../../services/rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ShareService} from '../../services/share.service';
import {AnimationItem} from 'lottie-web';
import {animate, query, stagger, style, transition, trigger} from '@angular/animations';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ModalImageComponent} from '../modal-image/modal-image.component';
import {MediaService} from '../drop-gallery/media.service';
import {AuthService} from "../../services/auth.service";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css'],
  animations: [
    trigger('swipe', [
      transition(':enter', [
        style({transform: 'translateY(-20%)', opacity: '.6'}),
        animate('0.15s ease-in')
      ])
    ])
  ]
})
export class ProductFormComponent implements OnInit {

  @ViewChild('valueInput', {static: true}) valueInput: ElementRef;
  @Input() content: string;
  public form: FormGroup;
  public prices: any = [];
  files: File[] = [];
  bsModalRef: BsModalRef;
  public optionsMenu: any = [
    {
      name: 'Save',
      icon: faSave
    }
  ];
  faSave = faSave;
  faPlus = faPlus;
  faQuestionCircle = faQuestionCircle;
  faAngleRight = faAngleRight;
  faTrashAlt = faTrashAlt;
  faHandPointer = faHandPointer;
  faWarehouse = faWarehouse;
  faCheckCircle = faCheckCircle;
  options: AnimationOptions = {
    path: '/assets/images/drop.json',
  };
  private animationItem: AnimationItem;
  priceTmp = 0;

  editorContent: object = {
    type: 'doc',
    content: []
  };
  public id: any = null;
  itemsAsObjects = [];
  itemsAsCategories = [];
  public deposits = [];
  public providers = [];
  public data = [];
  public currency: any = null;
  public currencySymbol: any = null;
  public granted: any
  loading = false;

  constructor(
    private rest: RestService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public router: Router,
    public auth: AuthService,
    public share: ShareService,
    private modalService: BsModalService,
    private ngZone: NgZone,
    private media: MediaService
  ) {

  }

  public ngxCurrencyOptions = {
    prefix: ``,
    thousands: ',',
    decimal: '.',
    allowNegative: false,
    nullable: true,
    max: 250_000_000,
    inputMode: CurrencyMaskInputMode.FINANCIAL,
  };

  ngOnInit(): void {
    const {currency, currencySymbol} = this.share.getSettings();
    this.currency = currency;
    this.currencySymbol = currencySymbol;
    this.ngxCurrencyOptions = {
      ...this.ngxCurrencyOptions, ...{
        prefix: `${currency} `
      }
    };

    this.granted = ['admin', 'manager'].includes(this.auth.currentUser()?.role)

    this.media.files = [];

    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      categories: [''],
      sku: ['', Validators.required],
      tag: [[]],
      gallery: [''],
      prices: ['', Validators.required],
      measures: ['']
    });


    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
      if (this.id.length && this.id !== 'add') {
        this.loadItem();
      }
    });

  }

  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
    // this.animationItem.stop();
  }

  tt = () => {
    this.form.get('prices').setValidators([Validators.required]);
    // this.form.controls['email'].setErrors({'incorrect': true});
  }

  changePrices = () => {

    if (!this.prices.length) {
      this.form.get('prices').setValidators([Validators.required]);
      this.form.controls.prices.reset();
    } else {
      this.form.get('prices').clearValidators();
      this.form.controls.prices.reset();
    }
  }

  loopComplete(e): void {
    // e.stop().then();
    this.pause();
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.stop());
  }

  pause(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.setSegment(43, 44));
  }

  onSelect(event) {

    event.addedFiles.map(async i => {
      i.base = await this.share.toBase64(i);
    });
    this.files.push(...event.addedFiles);
  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  addPrice = (e) => {

    this.prices.push({
      amount: this.priceTmp
    });
    this.priceTmp = 0;
    this.changePrices();
  }

  viewImage = (e, data: any = {}) => {
    e.stopPropagation();
    this.open(data);
  }

  editorContentChange(doc: object) {
    this.editorContent = doc;
  }

  deletePrice = (i) => {
    this.prices.splice(i, 1);
    this.changePrices();
  }

  onFocus = (e) => {
    this.priceTmp = null;
  }


  loadItem = () => {
    this.loading = true;
    this.rest.get(`products/${this.id}`)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        const {prices, gallery, provider, deposits} = res;
        this.prices = prices;
        this.providers = provider;
        this.deposits = deposits;
        this.media.files = [...gallery];
        this.form.patchValue(res);
      });
  }

  cbList = () => {
    this.router.navigate(['/', 'products']);
  }

  cbTrash = () => {
    this.rest.delete(`products/${this.id}`)
      .subscribe(res => this.cbList());
  }

  submitData = async (data: any = {}) => {
    this.form.patchValue({prices: this.prices});
    const gallery = await this.media.loadImages();
    data = {...this.form.value, ...data, ...{gallery}};
    const method = (this.id) ? 'patch' : 'post';
    this.rest[method](`products${(method === 'patch') ? `/${this.id}` : ''}`,
      data)
      .subscribe(res => {
        this.cbList();
      });
  }

  loadImages = () => new Promise((resolve, reject) => {

    const formData = new FormData();
    for (let i = 0; i < this.files.length; i++) {
      formData.append('files[]', this.files[i]);
    }
    this.rest.post(`storage`, formData, true, {})
      .subscribe(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
  })

  open(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalImageComponent,
      Object.assign({initialState}, {
        class: 'modal-light-zoom',
        ignoreBackdropClick: false
      })
    );
  }


  parseData = (data: any) => {
    // const tmp = [];
    // data.docs.map(a => tmp.push()
    return data.docs;
  }

  getImageUp = (images: any[]) => {
    return images.filter(a => (a._id));
  }

  parseImage = (data: any = {}) => {
    return (data.base) ? data.base : data.large;
  }

}
