import {Component, NgZone, OnInit} from '@angular/core';
import {AnimationItem} from "lottie-web";
import {AnimationOptions} from "ngx-lottie";
import {BsModalRef} from "ngx-bootstrap/modal";
import {AuthService} from "../../services/auth.service";
import {environment} from "../../../environments/environment";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-modal-profile',
  templateUrl: './modal-profile.component.html',
  styleUrls: ['./modal-profile.component.css']
})
export class ModalProfileComponent implements OnInit {
  options: AnimationOptions = {
    path: '/assets/images/testers.json',
  };
  private animationItem: AnimationItem;
  public user: any;
  public version: string;

  constructor(private ngZone: NgZone, public bsModalRef: BsModalRef, private auth: AuthService,
              private cookie: CookieService) {
  }

  ngOnInit(): void {
    this.version = environment.version;
    this.user = this.auth.currentUser();
  }

  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
    // this.animationItem.stop();
  }

  loopComplete(e): void {
    // e.stop().then();
    this.pause()
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.stop());
  }

  pause(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.setSegment(43, 44));
  }

  public resetSetting = () => {
    this.bsModalRef.hide()
    this.auth.openWizard()

  }
}
