import { InventoryService } from './inventory.service';
import { ModalBarCodesComponent } from './../modal-bar-codes/modal-bar-codes.component';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { ShareService } from '../../services/share.service';
import { RestService } from '../../services/rest.service';
import { ModalUserComponent } from '../modal-user/modal-user.component';
import { TranslateService } from '@ngx-translate/core';
import { CurrencyMaskInputMode } from 'ngx-currency';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';
import { debounceTime, map, finalize } from 'rxjs/operators';
import {faSave} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-inventory-form',
  templateUrl: './inventory-form.component.html',
  styleUrls: ['./inventory-form.component.css']
})
export class InventoryFormComponent implements OnInit, OnDestroy {
  @ViewChild('selectProductRef') selectProductRef;
  @ViewChild('selectProviderInput') selectProviderInput;
  @ViewChild('selectDepositInput') selectDepositInput;
  public form: FormGroup;
  public listSubscribers: any = [];
  public data: any = [];
  public providers: any
  public products: any
  public deposits: any
  public users: any = [];
  public id: any = null;
  public shortMode: boolean;
  itemsAsObjects = [];
  bsModalRef: BsModalRef;
  faQrcode = faQrcode;
  faSave = faSave;
  priceTmp = 0;
  public ngxCurrencyOptions = {
    prefix: ``,
    thousands: ',',
    decimal: '.',
    allowNegative: false,
    nullable: true,
    max: 250_000_000,
    inputMode: CurrencyMaskInputMode.FINANCIAL,
  };
  loading = false
  private currency: any;
  private currencySymbol: any;


  constructor(private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private shared: ShareService,
    private translate: TranslateService,
    public router: Router,
    private inventory: InventoryService,
    private rest: RestService) {
  }


  ngOnInit(): void {
    const { currency, currencySymbol } = this.shared.getSettings();
    this.currency = currency;
    this.currencySymbol = currencySymbol;
    this.ngxCurrencyOptions = {
      ...this.ngxCurrencyOptions, ...{
        prefix: `${currency} `
      }
    };

    this.form = this.formBuilder.group({
      product: ['', Validators.required],
      provider: ['', Validators.required],
      qty: ['', Validators.required],
      priceBase: [''],
      deposit: ['', Validators.required],
      tag: [[]],
      serials: [[]],
      description: [''],
    });
    this.listObserver()
    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
    });

    this.route.queryParams.subscribe(
      params => {
        const { product, deposit } = params;
        if (product && deposit) {
          this.shortMode = true;
          this.form.patchValue({
            product: {
              _id: product
            }, deposit: {
              _id: deposit
            }
          });
        }
      });

    this.shared.registerUser.subscribe(res => {
      this.users = [...[res],
      ...this.users];
    });
  }

  listObserver = () => {
    const observer1$ = this.inventory.serials.subscribe(res => {
      this.form.patchValue({ serials: res });
    });
    this.listSubscribers.push(observer1$);
  }

  onSubmit(): void {
    const method = (this.id) ? 'patch' : 'post';
    this.form.patchValue({ priceBase: this.priceTmp });
    this.rest[method](`inventory${(method === 'patch') ? `/${this.id}` : ''}`, this.form.value)
      .subscribe(res => {
        this.cbList();
      });
  }

  loadProviders = () => {
    let name = null;
    this.translate.get('PROVIDER.NEW_PROVIDER').subscribe((res: string) => {
      name = res;
    });
    this.rest.get(`providers?filter&limit=10000&sort=name&order=-1`)
      .subscribe(res => {
        this.providers = [...[{
          _id: 0,
          name,
          value: 'new'
        }],
        ...this.parseData(res)];
      });
  }

  loadDeposits = () => {
    let name = null;
    this.translate.get('DEPOSITS.NEW_DEPOSITS').subscribe((res: string) => {
      name = res;
    });
    this.rest.get(`deposits?filter&limit=10000&sort=name&order=-1`)
      .subscribe(res => {
        this.deposits = [...[{
          _id: 0,
          name,
          value: 'new'
        }],
        ...this.parseData(res)];
      });
  }

  selectProduct = (e) => {
    console.log(e);
    // if (e.value === 'new') {
    //   this.form.patchValue({manager: null})
    //   this.open()
    // }
  }

  selectProvider = (e) => {
    if (e.value === 'new') {
      this.form.patchValue({ provider: null });
      this.router.navigate(['/', 'providers', 'add']);
    }
  }


  parseData = (data: any) => {
    const tmp = [];
    data.docs.map(a => tmp.push({
      ...a, ...{
        router: ['/', 'inventory', a._id]
      }
    }));
    return tmp;
  }

  cbList = () => {
    this.router.navigate(['/', 'inventory']);
  }


  open(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalUserComponent,
      Object.assign({ initialState }, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  src = (e, source: string, model: string) => {
    const { term } = e;
    const q = [
      `${source}?`,
      `filter=${term}`,
      `&fields=name,email`,
      `&page=1&limit=5`,
      `&sort=name&order=-1`,
    ];


    this.loading = true;
    this.rest.get(q.join(''), true, { ignoreLoadingBar: '' })
      .pipe(
        debounceTime(0),
        map(a => a.docs),
        this.shared.prepare(() => this.loading = true),
        finalize(() => this.loading = false)
      ).subscribe(res => this[model] = res)
  }

  srcProduct = (input: any) => {
    const { term } = input;
    if (term.length > 2) {
      const q = [
        `products?`,
        `filter=${term}`,
        `&fields=name`,
        `&page=1&limit=5`,
        `&sort=name&order=-1`,
      ];
      this.loading = true;
      this.rest.get(q.join(''), true, { ignoreLoadingBar: '' })
        .pipe(
          debounceTime(0),
          map(a => a.docs),
          this.shared.prepare(() => this.loading = true),
          finalize(() => this.loading = false)
        ).subscribe(res => this.products = res)
    }
    if (term.length === 0) {
      this.products = 0
    }
  };

  selectDeposit(e: any) {
    if (e && e.value === 'new') {
      this.form.patchValue({ deposit: null });
      this.router.navigate(['/', 'deposits', 'add']);
    }
  }

  onFocus = (e) => {
    this.priceTmp = null;
  }

  public openBarCode = () => {
    const { qty } = this.form.value;
    const initialState: any = {
      numberItems: qty
    };
    this.bsModalRef = this.modalService.show(
      ModalBarCodesComponent,
      Object.assign({ initialState }, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  ngOnDestroy() {
    this.listSubscribers.forEach(a => a.unsubscribe())
  }
}
