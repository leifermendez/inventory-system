import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef, Input, OnInit,
} from '@angular/core';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  DAYS_OF_WEEK
} from 'angular-calendar';
import {faAngleLeft, faAngleRight} from '@fortawesome/free-solid-svg-icons';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import {Subject} from 'rxjs';
import * as _ from 'lodash'
import * as  moment from 'moment';
// weekStartsOn option is ignored when using moment, as it needs to be configured globally for the moment locale
moment.updateLocale('en', {
  week: {
    dow: DAYS_OF_WEEK.MONDAY,
    doy: 0,
  },
});

import {RestService} from "../../services/rest.service";
import {Router} from "@angular/router";
import {MyCalendarService} from "../../modules/calendar/my-calendar.service";

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};


@Component({
  selector: 'app-calendary',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendary.component.html',
  styleUrls: ['./calendary.component.css']
})
export class CalendaryComponent implements OnInit {
  @ViewChild('modalContent', {static: true}) modalContent: TemplateRef<any>;
  @Input() title: string
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({event}: { event: any }): void => {
        this.handleEvent('Edited', event);
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({event}: { event: any }): void => {
        this.events = this.events.filter((iEvent) => iEvent !== event);
        this.handleEvent('Deleted', event);
      },
    },
  ];
  refresh: Subject<any> = new Subject();
  events: any[] = [];
  activeDayIsOpen: boolean = false;

  constructor(private rest: RestService, private router: Router, private myCalendarService: MyCalendarService) {
  }

  ngOnInit(): void {
    this.loadData()
  }

  loadData = (start = null, end = null) => {
    this.events = []
    const dateStartOfMonth = start || moment().startOf('month').format('YYYY-MM-DD')
    const dateEndOfMonth = end || moment().endOf('month').format('YYYY-MM-DD')
    this.rest.get(`calendar/all?start=${dateStartOfMonth}&end=${dateEndOfMonth}`)
      .subscribe(res => {
        _.forEach(res, (e) => {
          const start = moment.utc(e.start).format('YYYY/MM/DD')
          const end = moment.utc(e.end).format('YYYY/MM/DD')
          this.events = [
            ...this.events,
            {
              start: moment(start, 'YYYY/MM/DD').startOf('day').toDate(),
              end: moment(end, 'YYYY/MM/DD').endOf('day').toDate(),
              title: e.title,
              color: colors.red,
              actions: this.actions,
              observation: e?.observation,
              data: e?.customData,
              user: e?.user,
              allDay: true,
            }
          ];
        })

        this.refresh.next()
      })

  }

  dayClicked({date, events}: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
                      event,
                      newStart,
                      newEnd,
                    }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  closeOpenMonthViewDay($event) {
    console.log($event)
    this.activeDayIsOpen = false;
    this.refresh.next()
    const dateStartOfMonth = moment($event).startOf('month').format('YYYY-MM-DD')
    const dateEndOfMonth = moment($event).endOf('month').format('YYYY-MM-DD')
    console.log(dateStartOfMonth)
    this.loadData(dateStartOfMonth, dateEndOfMonth)
  }

  handleEvent(action: string, event: any): void {
    this.modalData = {event, action};
    if (event && event.data && event?.data?.route) {
      console.log(event?.data?.route)
      this.router.navigate(event?.data?.route.split('/'))
    }
    if (event && event.data && !event?.data?.route) {
      this.myCalendarService.openModalCalendar(event)
    }
    // this.modal.open(this.modalContent, {size: 'lg'});
  }

  addEvent(): void {

  }
}
