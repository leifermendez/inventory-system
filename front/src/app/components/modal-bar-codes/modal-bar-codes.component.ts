import { InventoryService } from './../inventory-form/inventory.service';
import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-modal-bar-codes',
  templateUrl: './modal-bar-codes.component.html',
  styleUrls: ['./modal-bar-codes.component.css']
})
export class ModalBarCodesComponent implements OnInit {
  dynamicForm: FormGroup;
  public numberItems: any;
  public dataSerials: any = {};
  public serials = new FormArray([]);

  constructor(public bsModalRef: BsModalRef, private inventory: InventoryService) { }

  ngOnInit(): void {
    this.numberItems = Array.from(Array(this.numberItems).keys());
    this.numberItems.forEach(a => this.serials.push(new FormControl('')))

  }


  saveClose = () => {
    const response = this.serials.controls.map(a => {
      return a.value;
    })
    this.inventory.serials.emit(response)
    this.bsModalRef.hide();
  }

}
