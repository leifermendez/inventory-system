import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBarCodesComponent } from './modal-bar-codes.component';

describe('ModalBarCodesComponent', () => {
  let component: ModalBarCodesComponent;
  let fixture: ComponentFixture<ModalBarCodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBarCodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBarCodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
