import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddTaxComponent } from './modal-add-tax.component';

describe('ModalAddTaxComponent', () => {
  let component: ModalAddTaxComponent;
  let fixture: ComponentFixture<ModalAddTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
