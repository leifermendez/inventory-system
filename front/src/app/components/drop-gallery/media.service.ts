import {Injectable} from '@angular/core';
import {RestService} from "../../services/rest.service";
import {DomSanitizer} from "@angular/platform-browser";

@Injectable({
  providedIn: 'root'
})
export class MediaService {
  public files: any = [];
  public items: any = [];

  constructor(private rest: RestService, private sanitizer: DomSanitizer) {
  }

  public processFile = async (imageInput: any) => {
    await Promise.all(
      Object.values(imageInput.files).map(async ($event: File) => {
        const image = await this.blobFile($event);
        if (image) {
          this.files.push(image);
        }
      })
    )
      .then(() => console.log('then'))
      .catch((e) => console.log(e));
  }

  public toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);

  });

  blobFile = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          blob: $event,
          image,
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          blob: $event,
          image,
          base: null
        });
      };

    } catch (e) {
      return null;
    }
  })

  loadImages = (merge = false) => new Promise((resolve, reject) => {
    try {
      const alreadyUploaded = this.files.filter(a => a.original);
      if (this.files.filter(i => i.blob).length) {
        const formData = new FormData();
        this.files.forEach((item) => {
          if (item.blob) {
            formData.append('files[]', item.blob)
          }
        });
        this.rest.post(`storage`, formData, true, {})
          .subscribe(res => {
            if (merge) {
              this.files = [];
            }
            resolve([...alreadyUploaded, ...res['data']]);
          }, error => {
            reject([...alreadyUploaded]);
          });
      } else {
        resolve([...alreadyUploaded]);
      }
    } catch (e) {
      resolve([]);
    }
  });
}
