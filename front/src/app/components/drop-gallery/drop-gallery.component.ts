import {Component, EventEmitter, Input, NgZone, OnInit, Output} from '@angular/core';
import {NgxDropzoneChangeEvent} from "ngx-dropzone";
import {ShareService} from "../../services/share.service";
import {MediaService} from "./media.service";
import {AnimationOptions} from "ngx-lottie";
import {AnimationItem} from "lottie-web";
import {
  faHandPointer
}
  from '@fortawesome/free-solid-svg-icons';
import {ModalImageComponent} from "../modal-image/modal-image.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-drop-gallery',
  templateUrl: './drop-gallery.component.html',
  styleUrls: ['./drop-gallery.component.css']
})
export class DropGalleryComponent implements OnInit {
  @Output() cbAdded = new EventEmitter<any>();
  faHandPointer = faHandPointer
  bsModalRef: BsModalRef;
  options: AnimationOptions = {
    path: '/assets/images/click.json',
  };
  private animationItem: AnimationItem;

  constructor(public share: ShareService, public media: MediaService,
              private ngZone: NgZone, private modalService: BsModalService) {
  }

  ngOnInit(): void {
  }

  parseImage = (data: any = {}) => {
    return (data.medium) ? data.medium : data.base;
  }

  cbSwipe($event: any) {
    this.media.items = this.media.items.filter(a => {
      return (!Object.values(a).includes($event));
    })
  }

  viewImage = (e, data: any = {}) => {
    console.log(data)
    e.stopPropagation();
    this.open(data)
  }

  open(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalImageComponent,
      Object.assign({initialState}, {
        class: 'modal-light-zoom',
        ignoreBackdropClick: false
      })
    );
  }

  onRemove(event, i) {
    this.media.files.splice(i, 1);
    this.cbAdded.emit(event)
  }

  onSelect(event: NgxDropzoneChangeEvent) {
    this.media.processFile({files: event.addedFiles})
    this.cbAdded.emit(event)
  }

  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
    // this.animationItem.stop();
  }

  loopComplete(e): void {
    // e.stop().then();
    this.pause()
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.stop());
  }

  pause(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.setSegment(43, 44));
  }


}
