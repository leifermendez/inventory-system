import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddTaxOffsetComponent } from './modal-add-tax-offset.component';

describe('ModalAddTaxOffsetComponent', () => {
  let component: ModalAddTaxOffsetComponent;
  let fixture: ComponentFixture<ModalAddTaxOffsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddTaxOffsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddTaxOffsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
