import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCardPayComponent } from './modal-card-pay.component';

describe('ModalCardPayComponent', () => {
  let component: ModalCardPayComponent;
  let fixture: ComponentFixture<ModalCardPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCardPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCardPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
