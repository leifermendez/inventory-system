import {Component, NgZone, OnInit, ViewEncapsulation} from '@angular/core';
import {faLifeRing, faBell, faQuestionCircle, faUserCircle} from '@fortawesome/free-regular-svg-icons';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import {NotificationsService} from "../../services/notifications.service";
import {animate, style, transition, trigger} from "@angular/animations";
import {RestService} from "../../services/rest.service";
import {map} from "rxjs/operators";
import {AnimationOptions} from "ngx-lottie";
import {AnimationItem} from "lottie-web";

@Component({
  selector: 'app-notification-drop',
  templateUrl: './notification-drop.component.html',
  styleUrls: ['./notification-drop.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateY(100%)', opacity: 0}),
          animate('150ms', style({transform: 'translateY(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateY(0)', opacity: 1}),
          animate('150ms', style({transform: 'translateY(100%)', opacity: 0}))
        ])
      ]
    )
  ],
  encapsulation: ViewEncapsulation.None,
})
export class NotificationDropComponent implements OnInit {
  faBell = faBell
  faTimes = faTimes
  data$: any;
  options: AnimationOptions = {
    path: '/assets/images/781-no-notifications.json',
  };
  private animationItem: AnimationItem;

  constructor(public notificationsService: NotificationsService, private rest: RestService, private ngZone: NgZone) {
  }

  ngOnInit(): void {
    this.loadData()
  }

  loadData = () => {
    this.rest.get(`notifications`)
      .pipe(map(a => a.docs))
      .subscribe(res => this.data$ = res)
  }

  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
    // this.animationItem.stop();
  }

  loopComplete(e): void {
    // e.stop().then();
    this.pause()
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.stop());
  }

  pause(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.setSegment(50, 118));
  }
}
