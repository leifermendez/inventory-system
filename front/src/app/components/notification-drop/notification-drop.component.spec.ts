import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationDropComponent } from './notification-drop.component';

describe('NotificationDropComponent', () => {
  let component: NotificationDropComponent;
  let fixture: ComponentFixture<NotificationDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
