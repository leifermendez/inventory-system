import {Component, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {PurchaseService} from "../../modules/purchases/purchase.service";

@Component({
  selector: 'app-modal-pay',
  templateUrl: './modal-pay.component.html',
  styleUrls: ['./modal-pay.component.css']
})
export class ModalPayComponent implements OnInit {
  public section: any

  constructor(public bsModalRef: BsModalRef, private purchaseService: PurchaseService) {
  }

  ngOnInit(): void {
    this.section = {
      ...this.section, ...{
        purchase: this.section,
        amount: this.section?.totalSubtract,
        status: null
      }
    }
  }

  callbackData = () => {
    this.bsModalRef.hide()
    this.purchaseService.refreshData.emit(true)
  }

}
