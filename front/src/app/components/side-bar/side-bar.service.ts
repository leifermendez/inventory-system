import {Injectable} from '@angular/core';
import {RestService} from "../../services/rest.service";
import {NgxCopilotService} from "../../../../projects/ngx-copilot/src/lib/ngx-copilot.service";
import {AuthService} from "../../services/auth.service";
import {NotificationsService} from "../../services/notifications.service";


@Injectable({
  providedIn: 'root'
})
export class SideBarService {

  constructor(private  copilot: NgxCopilotService, private rest: RestService, private auth: AuthService,
              private notificationsService: NotificationsService) {
    copilot.done.subscribe(res => {
      if (res === 'not_found_nex_step') {
        this.done()
      }
    })
  }

  saveStepper = (step: string) => {
    this.rest.patch(`profile/stepper`, {
      stepper: step
    }).subscribe(res => {
      this.auth.updateUser('stepper', res.stepper);
    });
  };

  loadDummy = () => {
    this.rest.post(`profile/dummy`).subscribe(res => {
      this.notificationsService.open = true
    });
    this.done()
  }

  /*Re initialize in specify step*/
  initPosition = (stepNumber: any) => {
    this.copilot.checkInit(stepNumber);
  }

  /*Next Step*/
  nextStep = (stepNumber: any) => this.copilot.next(stepNumber);

  /*Finish*/
  done = (step: any = null) => {
    this.saveStepper((!step) ? 'intro_user' : step);
    this.copilot.removeWrapper();
  };
}
