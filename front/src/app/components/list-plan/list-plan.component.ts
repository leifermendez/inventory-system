import {Component, OnInit} from '@angular/core';
import {SettingsService} from "../../modules/settings/settings.service";

@Component({
  selector: 'app-list-plan',
  templateUrl: './list-plan.component.html',
  styleUrls: ['./list-plan.component.css']
})
export class ListPlanComponent implements OnInit {
  plans: Array<any> = []

  constructor(private settingsService: SettingsService) {
  }

  ngOnInit(): void {
    this.plans = [
      {
        title: 'Plan básico',
        description: 'Diseñado para emprendedores que se encuentren iniciando un proyecto',
        price: 0,
        disabled: true,
        features: [
          {
            title: 'Usuarios gratis',
            description: 'Gestiona distintos roles de usuarios',
            icon: false
          },
          {
            title: 'Productos limitados',
            description: 'Ingresa todos tus productos',
            icon: false
          },
          {
            title: 'Proveedores limitados',
            description: 'Administra todos los proveedores ',
            icon: false
          },
          {
            title: 'Depositos limitados',
            description: 'Controla todas tus bodegas y almacenes',
            icon: false
          }
        ]
      },
      {
        title: 'Plan Ilimitado',
        description: 'Diseñado para emprendedores que se encuentren iniciando un proyecto',
        price: 9.99,
        disabled: false,
        features: [
          {
            title: 'Usuarios gratis',
            description: 'Gestiona distintos roles de usuarios',
            icon: false
          },
          {
            title: 'Productos ilimitados',
            description: 'Ingresa todos tus productos',
            icon: false
          },
          {
            title: 'Movimientos ilimitados',
            description: 'Manten un historial de tu inventario',
            icon: false
          },
          {
            title: 'Proveedores ilimitados',
            description: 'Administra todos los proveedores ',
            icon: false
          },
          {
            title: 'Depositos ilimitados',
            description: 'Controla todas tus bodegas y almacenes',
            icon: false
          },
          {
            title: 'Almacenamiento limitado',
            description: 'Adjunta toda la galeria de fotos de tus productos',
            icon: false
          },
          {
            title: 'Estadísticas de tus ventas',
            description: 'Analiza y estudia el comportamiento de tus ventas',
            icon: false
          },
          {
            title: 'Diseños personalizados',
            description: 'Personaliza tus presupuestos',
            icon: false
          }
        ]
      },
    ]
  }

  open = (data) => this.settingsService.openModalCard(data)
}
