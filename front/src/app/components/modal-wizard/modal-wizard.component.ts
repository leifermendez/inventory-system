import {Component, Inject, NgZone, OnInit} from '@angular/core';
import {AnimationOptions} from "ngx-lottie";
import {AnimationItem} from "lottie-web";
import {BsModalRef} from "ngx-bootstrap/modal";
import {
  faTimes
}
  from '@fortawesome/free-solid-svg-icons';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DemoFilePickerAdapter} from "../../demo-file-picker.adapter";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CookieService} from "ngx-cookie-service";
import {DomSanitizer} from "@angular/platform-browser";
import {Observable, throwError} from "rxjs";
import {environment} from "../../../environments/environment";
import {catchError, finalize} from "rxjs/operators";
import {ShareService} from "../../services/share.service";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {CurrenciesDataService} from "../../services/currencies-data.service";
import {VideoHelpService} from "../../services/video-help.service";

@Component({
  selector: 'app-modal-wizard',
  templateUrl: './modal-wizard.component.html',
  styleUrls: ['./modal-wizard.component.css']
})
export class ModalWizardComponent implements OnInit {
  adapter = new DemoFilePickerAdapter(this.http, this.cookieService);
  public form: FormGroup;
  currencies = []
  faTimes = faTimes
  options: AnimationOptions = {
    path: '/assets/images/wizard.json',
  };
  public preview = {
    image: null,
    blob: null
  }
  loading: any;
  listVideos = {
    settingsVideo: 'https://player.vimeo.com/video/510207635'
  }

  constructor(private ngZone: NgZone, public bsModalRef: BsModalRef,
              private http: HttpClient,
              private share: ShareService,
              private sanitizer: DomSanitizer,
              private router: Router,
              public videoHelpService: VideoHelpService,
              public currencyService: CurrenciesDataService,
              private cookieService: CookieService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      currency: ['', Validators.required],
      currencySymbol: [''],
      logo: [''],
    });

    this.checkSession();
    this.loadCurrencies()
  }

  loadCurrencies = () => {
    this.currencies = Object.values(this.currencyService.listCurrencies)
  }

  checkSession = () => {
    try {
      const {_id} = this.share.getSettings();
      if (!_id) {
        throw new Error("invalid ID");
      }
    } catch (e) {
      this.share.alert('ERROR', 'Settings not found');
      setTimeout(() => {
        this.bsModalRef.hide();
        this.cookieService.delete('session', '/');
        this.cookieService.delete('settings', '/');
        this.cookieService.delete('user', '/');
        this.router.navigate(['/', 'oauth', 'login']);
      }, 1000)
    }
  }

  reset = () => {
    this.preview = {
      image: null,
      blob: null
    }
  }

  close = () => {
    this.bsModalRef.hide()
  }

  update = () => {
    const {name, currency, currencySymbol} = this.form.value;
    const formData = new FormData();
    formData.append('logo', this.preview.blob)
    formData.append('name', name)
    formData.append('currency', currency)
    formData.append('currencySymbol', currencySymbol)
    this.loading = true;
    this.saveRest(`settings/general`, formData)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
          this.cookieService.set(
            'settings',
            JSON.stringify(res),
            environment.daysTokenExpire,
            '/');
          this.share.changeSetting.emit(res);
          this.bsModalRef.hide()
        },
        error => console.log('err', error))
  }

  fileAdded($event: any) {
    const unsafeImg = URL.createObjectURL($event.file);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    this.preview = {
      blob: $event.file,
      image
    }
  }

  parseHeader = () => {
    const token = this.cookieService.get('session');
    let header = {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    };
    return new HttpHeaders(header);
  };

  saveRest(path = '', body = {}): Observable<any> {
    try {
      this.share.loading.emit(true)
      return this.http.patch(`${environment.api}/${path}`, body,
        {headers: this.parseHeader()})
        .pipe(
          catchError((e: any) => {
            this.share.loading.emit(false)
            return throwError({
              status: e.status,
              statusText: e.statusText,
              e
            });
          }),
        );
    } catch (e) {
      this.share.loading.emit(false)
    }
  }

  preSelectSymbol($event: any) {
    this.form.patchValue({currencySymbol: $event.symbol});

  }

  test($event: any) {
    console.log($event)

  }
}
