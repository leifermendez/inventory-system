import {Component, OnInit} from '@angular/core';
import {
  faPlus
} from '@fortawesome/free-solid-svg-icons';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalCardPayComponent} from "../modal-card-pay/modal-card-pay.component";
import {RestService} from "../../services/rest.service";
import {ShareService} from "../../services/share.service";

@Component({
  selector: 'app-method-payment',
  templateUrl: './method-payment.component.html',
  styleUrls: ['./method-payment.component.css']
})
export class MethodPaymentComponent implements OnInit {
  paymentMethod: Array<any> = []
  bsModalRef: BsModalRef;
  faPlus = faPlus

  constructor(private modalService: BsModalService, private sharedService: ShareService, private rest: RestService) {
  }

  ngOnInit(): void {
    this.loadData()
  }

  open(item: any) {

  }

  loadData = () => {
    this.rest.get('settings/payment')
      .subscribe(res => {
        if( res.paymnet )
        this.paymentMethod.push(res.payment)
      })
  }

  openCard(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalCardPayComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }
}
