import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanExpiredComponent } from './plan-expired.component';

describe('PlanExpiredComponent', () => {
  let component: PlanExpiredComponent;
  let fixture: ComponentFixture<PlanExpiredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanExpiredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanExpiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
