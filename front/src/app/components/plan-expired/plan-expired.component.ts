import { Component, OnDestroy, OnInit } from '@angular/core';
import { ShareService } from "../../services/share.service";
import * as moment from "moment";

@Component({
  selector: 'app-plan-expired',
  templateUrl: './plan-expired.component.html',
  styleUrls: ['./plan-expired.component.css']
})
export class PlanExpiredComponent implements OnInit, OnDestroy {
  public plan: any
  expired: any;
  public listSubscribers: any = [];

  constructor(private shareService: ShareService) {
  }


  ngOnInit(): void {
    this.plan = this.shareService.getSettings()
    const { planDate = null } = this.plan

    if (planDate) {
      const dateA = moment(planDate?.createdAt).add(1, 'month');
      const dateB = moment().subtract(1, 'day');
      this.expired = dateA.diff(dateB, 'days')

    } else {
      this.expired = 0;
    }

    console.log(this.expired)
    this.listObserver();
  }

  ngOnDestroy(): void {
    this.listSubscribers.forEach(a => a.unsubscribe());
  }

  listObserver = () => {
    const observer1$ = this.shareService.payPlan.subscribe(res => {
      if (res?.msg === 'CARD_UPDATE') {
        this.expired = 1;
        this.shareService.setSettings('planDate', { cratedAt: moment().toISOString() })
      }
    });

    this.listSubscribers.push(observer1$);
  };
}
