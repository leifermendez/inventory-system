import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RestService} from "../../services/rest.service";
import {ShareService} from "../../services/share.service";
import {BsModalService} from "ngx-bootstrap/modal";
import {ActivatedRoute, Router} from "@angular/router";
import {finalize} from "rxjs/operators";
import {faSave} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Input() mode: string = 'list'
  @Input() formInit: any = {};
  itemsAsObjects: any;
  loading = false;
  faSave = faSave;
  public form: FormGroup;
  public roles: any = [
    {
      name: 'Administrador',
      value: 'admin'
    },
    {
      name: 'Encargado',
      value: 'manager'
    },
    {
      name: 'Vendedor',
      value: 'seller'
    },
    {
      name: 'Proveedor',
      value: 'provider'
    },
    {
      name: 'Cliente',
      value: 'customer'
    }
  ]
  public id: any = null;
  public data: any = []

  constructor(private formBuilder: FormBuilder,
              private modalService: BsModalService,
              private route: ActivatedRoute,
              private shared: ShareService,
              public router: Router,
              private rest: RestService) {
  }

  ngOnInit(): void {

    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      nie: ['', Validators.required],
      role: ['', Validators.required],
      nameBusiness: [''],
      password: [''],
      phone: ['', Validators.required],
      tag: [[]],
      description: [''],
    });

    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
    });

    if (this.form && this.formInit) {
      this.form.patchValue(this.formInit)
    }
    this.loadProvider()
  }

  onSubmit(): void {
    this.loading = true;
    const password = this.shared.generate(10)
    this.form.patchValue({password})
    const method = (this.id) ? 'patch' : 'post';
    this.rest[method](`users${(method === 'patch') ? `/${this.id}` : ''}`, this.form.value)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        if (this.mode === 'list') {
          this.cbList();
        }
        this.shared.registerUser.emit(res)
      })
  }

  cbList = () => {
    this.router.navigate(['/', 'users'])
  }

  loadProvider = () => {
    if (this.id && (this.id.length)) {
      this.rest.get(`users/${this.id}`)
        .subscribe(res => {
          this.form.patchValue(res)
        })
    }
  }


  cbTrash() {
    this.rest.delete(`users/${this.id}`)
      .subscribe(res => this.router.navigate(['/', 'users']))
  }
}
