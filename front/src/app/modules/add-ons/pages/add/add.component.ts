import {Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SettingsComponent as pdfReportSetting} from "../../../../../../projects/pdfreport/src/lib/settings/settings.component";
import {SettingsComponent as stripeSetting} from "../../../../../../projects/kit-agil-stripe/src/lib/settings/settings.component";
import {SettingsComponent as paypalSetting} from "../../../../../../projects/kit-agil-paypal/src/lib/settings/settings.component";
import {SettingsComponent as mercadopagoSetting} from "../../../../../../projects/kit-mercado-pago/src/lib/settings/settings.component";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  @ViewChild('settingContainer', {read: ViewContainerRef}) entry: ViewContainerRef;
  bsModalRef: BsModalRef;
  componentRef: any;
  public history: any = [
    {
      name: 'Complementos'
    }
  ]
  private id: any;
  private index = [
    {
      name: 'pdfReportSetting',
      class: pdfReportSetting
    },
    {
      name: 'stripeSetting',
      class: stripeSetting
    },
    {
      name: 'paypalSetting',
      class: paypalSetting
    },
    {
      name: 'mercadopagoSetting',
      class: mercadopagoSetting
    }
  ];
  private dataLoad: any;

  constructor(private modalService: BsModalService,
              private componentFactoryResolver: ComponentFactoryResolver,
              private rest: RestService,
              private resolver: ComponentFactoryResolver,
              private route: ActivatedRoute,
              public router: Router) {
  }


  createComponent(name = '') {
    this.entry.clear();
    this.index.filter((a: any) => {
      if (a.name === name) {
        const factory = this.resolver.resolveComponentFactory(a.class);
        this.componentRef = this.entry.createComponent(factory);
        this.componentRef.instance.content = this.dataLoad;
        this.componentRef.instance.userData = this.dataLoad;
      }
    });
  }

  destroyComponent() {
    this.componentRef.destroy();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
      if (this.id.length && this.id !== 'add') {
        this.load();
      }
    });
  }


  private load() {
    this.rest.get(`plugins/${this.id}`).subscribe(res => {
      const {item} = res;
      this.dataLoad = res;
      /** The settings pages is import! top page **/
      this.createComponent(`${item.path}Setting`)
    })
  }


}
