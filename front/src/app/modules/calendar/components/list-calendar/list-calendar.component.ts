import {Component, Input, OnInit} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PaginationServiceService} from "../../../../services/pagination-service.service";
import {PDFReportService} from "../../../../../../projects/pdfreport/src/lib/pdfreport.service";
import {SearchService} from "../../../search/search.service";
import {HomeService} from "../../../home/home.service";
import {ShareService} from "../../../../services/share.service";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {faPhoneAlt, faIndustry, faUser, faListAlt, faListUl} from '@fortawesome/free-solid-svg-icons';
import {faAngleLeft, faAngleRight, faAngleDoubleLeft, faAngleDoubleRight} from '@fortawesome/free-solid-svg-icons';
import {faCalendarAlt} from '@fortawesome/free-regular-svg-icons';
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {debounceTime, finalize, map, tap} from "rxjs/operators";

@Component({
  selector: 'app-list-calendar',
  templateUrl: './list-calendar.component.html',
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({opacity: 0}),
          stagger(20, [
            animate(25, style({opacity: 1}))
          ])
        ], {optional: true})
      ])
    ])
  ],
  styleUrls: ['./list-calendar.component.css']
})
export class ListCalendarComponent implements OnInit {
  @Input() mode: string = 'page'
  @Input() title: any = false;
  @Input() limit: any = 15;
  @Input() viewMore: boolean = true;
  public cbMode: any = false;
  public dataTake: any
  loading: any;

  constructor(private rest: RestService,
              private router: Router,
              public pagination: PaginationServiceService,
              private route: ActivatedRoute,
              private pdf: PDFReportService,
              private search: SearchService,
              public homeService: HomeService,
              public share: ShareService) {
  }
  faAngleDoubleLeft = faAngleDoubleLeft
  faAngleDoubleRight = faAngleDoubleRight
  faCalendarAlt = faCalendarAlt
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight
  faIndustry = faIndustry
  faPhoneAlt = faPhoneAlt
  faUser = faUser
  public data: any;
  public source = 'calendar';
  public currency = null;
  public currencySymbol = null;
  public history: any = [
    {
      name: 'Eventos'
    }
  ]

  ngOnInit(): void {
    let fields = [
      `?fields=title,tag`
    ];
    this.search.setConfig({fields, key: this.source, limit: this.limit})
    const {currency, currencySymbol} = this.share.getSettings();
    this.currency = currency;
    this.currencySymbol = currencySymbol
    this.route.queryParams.subscribe(
      params => {
        const {q = ''} = params
        this.onSrc(q)
      });
  }

  cbPdf() {
    this.pdf.generateList({source: this.source, data: this.dataTake})
  }

  /**** GLOBAL FUNCTIONS ****/

  load = (src: any, resetPagination = true) => {
    this.loading = true;
    this.data = this.search.find({
      source: this.source,
      query: src
    }, resetPagination).pipe(
      debounceTime(500),
      tap(a => this.dataTake = a.docs),
      finalize(() =>  this.loading = false),
      map((a) => a.docs)
    )
  }

  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => this.load(e);

  pageChanged($event: PageChangedEvent) {
    const {page} = $event;
    this.pagination.page = page;
    this.load(undefined, false);
  }

  /**** END GLOBAL FUNCTIONS ****/
}
