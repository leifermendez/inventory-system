import {AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ActivatedRoute, Router} from "@angular/router";
import {ShareService} from "../../../../services/share.service";
import {RestService} from "../../../../services/rest.service";
import {ModalUserComponent} from "../../../../components/modal-user/modal-user.component";
import {catchError, debounceTime, distinctUntilChanged, finalize, map, switchMap, tap} from "rxjs/operators";
import {concat, Observable, of, Subject} from "rxjs";
import * as moment from 'moment'

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit, AfterContentChecked {
  public history: any = [
    {
      name: 'Calendario',
      router: ['/', 'calendar']
    },
    {
      name: 'Nuevo',
      router: null
    }
  ]
  public form: FormGroup;
  public data: any = []
  public users: any = []
  public id: any = null;
  itemsAsObjects = [];
  bsModalRef: BsModalRef;
  loading: boolean
  userLoading = false;
  userInput$ = new Subject<string>();
  results$: Observable<any>;
  @ViewChild('selectUserInput') selectUserInput;
  configFormat = 'MMM, DD YYYY hh:mm A';

  constructor(private formBuilder: FormBuilder,
              private modalService: BsModalService,
              private route: ActivatedRoute,
              private shared: ShareService,
              public router: Router,
              private cdr: ChangeDetectorRef,
              private rest: RestService) {
  }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges()
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      start: ['', Validators.required],
      end: ['', Validators.required],
      title: ['', Validators.required],
      user: ['', Validators.required],
      customData: [[]],
      tag: [[]],
      observation: [''],
    });
    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
    });

    this.shared.registerUser.subscribe(res => {
      this.form.patchValue({manager: res})
      this.users = [...[res],
        ...this.users];
    })
    this.listenObserver();
    this.loadUsers()
  }

  onSubmit(): void {
    const method = (this.id) ? 'patch' : 'post';
    this.rest[method](`calendar${(method === 'patch') ? `/${this.id}` : ''}`, this.form.value)
      .subscribe(res => {
        this.cbList()
      })
  }

  // loadProvider = () => {
  //   if (this.id && (this.id.length)) {
  //     this.rest.get(`deposits/${this.id}`)
  //       .subscribe(res => {
  //         console.log(res)
  //         this.form.patchValue(res)
  //         // this.data = this.parseData(res);
  //       })
  //   }
  // }


  parseData = (data: any) => {
    const tmp = [];
    data.docs.map(a => tmp.push({
      ...a, ...{
        router: ['/', 'calendar', a._id]
      }
    }));
    return tmp;
  }

  cbList = () => {
    this.router.navigate(['/', 'calendar'])
  }

  cbTrash = () => {
    this.rest.delete(`calendar/${this.id}`)
      .subscribe(res => this.cbList())
  }


  /**
   * ---------------------
   * @param e
   */

  listenObserver = () => {
    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
      this.rest.get(`calendar/${this.id}`).subscribe(res => {
        const {start, end} = res;
        this.data = {
          ...res, ...{
            start: (start) ? moment(start).format(this.configFormat) : null,
            end: (end) ? moment(end).format(this.configFormat) : null
          }
        }
        this.form.patchValue(res)
      })
    });

  }


  open(data: any = null) {
    const initialState: any = {
      section: data
    };
    this.bsModalRef = this.modalService.show(
      ModalUserComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }


  private loadUsers() {
    this.results$ = concat(
      of([]), // default items
      this.userInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.userLoading = true),
        switchMap(term => this.singleSearch$(term).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.userLoading = false)
        ))
      )
    );
  }


  singleSearch$ = (term) => {
    const q = [
      `users?`,
      `filter=${term}`,
      `&fields=name,email,nameBusiness`,
      `&page=1&limit=5`,
      `&sort=name&order=-1`,
    ];
    return this.rest.get(q.join(''), true,
      {ignoreLoadingBar: ''}).pipe(
      map((a) => a.docs)
    )
  }

  selectUser = (e) => {
    if (e) {
      if (!e._id) {
        const initialData = {...e, ...{manager: null, role: 'customer'}}
        this.open(initialData)
      } else {
        this.form.patchValue({deliveryAddress: e.address})
      }
    }
  }

  trackByFn(item: any) {
    return item._id;
  }

  listDate = ($event = {startDate: null, endDate: null}) => {
    const body = {
      start: ($event.startDate) ? moment($event.startDate).format('YYYY-MM-DD HH:mm') : null,
      end: ($event.endDate) ? moment($event.endDate).format('YYYY-MM-DD HH:mm') :
        moment($event.startDate).format('YYYY-MM-DD HH:mm'),
    };
    this.form.patchValue(body);
  };
}
