import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import { AddComponent } from './pages/add/add.component';
import { ListComponent } from './pages/list/list.component';
import {SharedModule} from "../shared/shared.module";
import {PurchasesModule} from "../purchases/purchases.module";
import { ListCalendarComponent } from './components/list-calendar/list-calendar.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {PaginationModule} from "ngx-bootstrap/pagination";
import {TooltipModule} from "ngx-bootstrap/tooltip";
import {PDFReportModule} from "../../../../projects/pdfreport/src/lib/pdfreport.module";
import {TranslateModule} from "@ngx-translate/core";
import {ReactiveFormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {AvatarModule} from "ngx-avatar";
import {TagInputModule} from "ngx-chips";
import {NgxFunnyDatepickerModule} from "ngx-funny-datepicker";
import { ModalDetailCalendarComponent } from './components/modal-detail-calendar/modal-detail-calendar.component';


@NgModule({
  declarations: [AddComponent, ListComponent, ListCalendarComponent, ModalDetailCalendarComponent],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    SharedModule,
    PurchasesModule,
    FontAwesomeModule,
    PaginationModule,
    TooltipModule,
    PDFReportModule,
    TranslateModule,
    ReactiveFormsModule,
    NgSelectModule,
    AvatarModule,
    TagInputModule,
    NgxFunnyDatepickerModule
  ]
})
export class CalendarModule { }
