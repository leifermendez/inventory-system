import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  public modeView: boolean = true

  constructor() {
  }
}
