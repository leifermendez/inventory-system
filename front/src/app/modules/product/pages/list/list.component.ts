import {AuthService} from '../../../../services/auth.service';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RestService} from '../../../../services/rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {faPhoneAlt, faIndustry, faUser, faCaretDown} from '@fortawesome/free-solid-svg-icons';
import {ShareService} from '../../../../services/share.service';
import {animate, query, stagger, style, transition, trigger} from '@angular/animations';
import {PageChangedEvent} from 'ngx-bootstrap/pagination';
import {PaginationServiceService} from '../../../../services/pagination-service.service';
import {faAngleLeft, faAngleRight, faAngleDoubleLeft, faAngleDoubleRight} from '@fortawesome/free-solid-svg-icons';
import {PDFReportService} from '../../../../../../projects/pdfreport/src/lib/pdfreport.service';
import {SearchService} from '../../../search/search.service';
import {FilterServiceService} from '../../../../components/list-items/filter-service.service';
import {finalize, map, tap} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-list-products',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({opacity: 0}),
          stagger(15, [
            animate(25, style({opacity: 1}))
          ])
        ], {optional: true})
      ])
    ])
  ]
})
export class ListComponent implements OnInit {
  @Input() mode: string = 'page';
  @Input() title: any = false;
  @Input() limit: any = 15;
  @Input() viewMore: boolean = true;
  @Output() cbClick = new EventEmitter<any>();
  public cbMode: any = null;
  public currency: any = null;
  public currencySymbol: any = null;
  @Input() dataTake: any
  @Input() data: Observable<any>;
  loading: any;

  constructor(private share: ShareService,
              private route: ActivatedRoute,
              public pagination: PaginationServiceService,
              public filterService: FilterServiceService,
              public auth: AuthService,
              private pdf: PDFReportService,
              public search: SearchService,
              private router: Router) {
  }

  faAngleDoubleLeft = faAngleDoubleLeft;
  faAngleDoubleRight = faAngleDoubleRight;
  faAngleLeft = faAngleLeft;
  faAngleRight = faAngleRight;
  faPhoneAlt = faPhoneAlt;
  faIndustry = faIndustry;
  faUser = faUser;
  public page: number = 1;
  public source = 'products';

  public history: any = [
    {
      name: 'Productos'
    }
  ];

  ngOnInit(): void {
    this.pagination.init();
    this.search.fields = [];
    this.search.filters = [];
    this.filterService.filterSelect = [];
    const {currency, currencySymbol} = this.share.getSettings();
    this.currencySymbol = currencySymbol;
    this.currency = currency;
    this.route.queryParams.subscribe(
      params => {
        this.cbInitFilter(params);
        const {q = ''} = params;
        if (!this.data) {
          this.onSrc(q);
        }
      });
  }

  cbInitFilter = (params: any) => {
    const {filter} = params;
    if (filter) {
      const fill = {
        condition: null,
        pre: {label: 'FILTER.DEPOSIT', source: 'deposits', field: 'deposits', level: 'second', condition: false},
        value: {
          name: filter
        }
      };
      this.filterService.filterSelect.push(fill);
      this.cbFilter({filters: [fill]});
    }
  }

  emitCbClick = (inside: any = {}) => {
    if (this.mode === 'modal') {
      this.cbClick.emit(inside);
    } else {
      this.router.navigate(['/', 'products', inside?._id]);
    }

  }

  /**** GLOBAL FUNCTIONS ****/

  cbPdf() {
    this.pdf.generateList({source: this.source, page: this.page, data: this.dataTake});
  }

  load = (src: string = '', merge = true, secondQuery = [], strict = false) => {
    this.loading = true
    let fields = [
      `?page=${this.pagination.page}`,
      `&limit=${this.limit}`
    ];

    if (secondQuery.length) {
      fields = secondQuery;
    }
    if (strict) {
      fields.push(`&strict=true`);
    }
    const q = this.share.parseLoad(src, this.source, fields);

    this.data = this.pagination.paginationData$(q, this.dataTake, merge, this.source)
      .pipe(
        tap((b: any) => {
          this.dataTake = b.docs;
          this.pagination.morePage = b.hasNextPage;
          this.pagination.paginationConfig = b;
        }),
        finalize(() => this.loading = false),
        map((a: any) => a.docs)
    )
  
  }

  goTo = () => this.share.goTo(this.source);

  onSrc = (e) => {
    this.pagination.src = (e && e.length) ? e : '';

    this.load(e, false, this.search.snipQuery(
      {...this.pagination, ...{limit: this.limit}}, [],
      true));
  }

  cbFilter($event: any = []) {
    const {filters, concat} = $event;
    this.search.fields = this.search.fields.filter(f => f !== 'tag');
    this.load('', false, this.search.snipQuery(this.pagination, filters,
      concat, (!filters.length)), true);
  }

  paginate = () => {
    this.pagination.page = this.pagination.page + 1;
    this.load();
  }

  pageChanged($event: PageChangedEvent) {
    const {page} = $event;
    this.pagination.page = page;
    this.load(this.pagination.src, false, this.search.snipQuery(this.pagination, [],
      true));
  }

  /**** END GLOBAL FUNCTIONS ****/

}
