import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AddComponent} from "./pages/add/add.component";


const routes: Routes = [
  {path: ':event/:id', component: AddComponent},
  {path: ':event', component: AddComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddEventsRoutingModule {
}
