import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./pages/login/login.component";
import { ForgotComponent } from "./pages/forgot/forgot.component";
import { ResetComponent } from "./pages/reset/reset.component";
import { CallbackComponent } from "./pages/callback/callback.component";
import { RegisterComponent } from './pages/register/register.component';
import { RegisterGuardGuard } from 'src/app/guards/register-guard.guard';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'register', component: RegisterComponent,
    canActivate: [RegisterGuardGuard]
  },
  { path: 'forgot', component: ForgotComponent },
  { path: 'reset/:code', component: ResetComponent },
  { path: 'callback', component: CallbackComponent },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OauthRoutingModule {
}
