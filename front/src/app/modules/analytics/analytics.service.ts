import {Injectable, OnInit} from '@angular/core';
import * as moment from "moment";
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService implements OnInit {
  public rangesTime = [
    {
      name: 'Año actual',
      value: [
        moment().startOf('year').format('YYYY-MM-DD'),
        moment().endOf('year').format('YYYY-MM-DD')
      ]
    },
    {
      name: 'Mes actual',
      value: [
        moment().startOf('month').format('YYYY-MM-DD'),
        moment().endOf('month').format('YYYY-MM-DD')
      ]
    },
    {
      name: 'Mes anterior',
      value: [
        moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD'),
        moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD')
      ]
    },
    {
      name: 'Últimos 15 días',
      value: [
        moment().subtract(2, 'week').format('YYYY-MM-DD'),
        moment().format('YYYY-MM-DD')
      ]
    },
    {
      name: 'Últimos 7 días',
      value: [
        moment().subtract(1, 'week').format('YYYY-MM-DD'),
        moment().format('YYYY-MM-DD')
      ]
    },
    {
      name: 'Rango personalizado',
      value: 'custom'
    }
  ];

  public rangeSelect: RangeSelectModel = {
    startDate: null,
    endDate: null,
    selectValue: null,
  };

  constructor() {
    const firstValue = _.head(this.rangesTime)
    this.rangeSelect = {startDate: firstValue.value[0], endDate: firstValue.value[1], selectValue: firstValue}
  }

  ngOnInit(): void {

  }
}

export class RangeSelectModel {
  startDate: any;
  endDate: any;
  selectValue: any
}
