import { AfterViewChecked, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { RestService } from "../../../../services/rest.service";
import { ActivatedRoute, Router } from "@angular/router";
import { PaginationServiceService } from "../../../../services/pagination-service.service";
import { SearchService } from "../../../search/search.service";
import { ShareService } from "../../../../services/share.service";
import { faBox, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import {
  faAngleLeft,
  faAngleRight,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faUserTie,
  faExternalLinkAlt
} from '@fortawesome/free-solid-svg-icons';
import { debounceTime, finalize, map, tap } from "rxjs/operators";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import * as moment from 'moment';
import * as _ from 'lodash';
import { AnalyticsService } from "../../analytics.service";

@Component({
  selector: 'app-list-seller',
  templateUrl: './list-seller.component.html',
  styleUrls: ['./list-seller.component.css']
})
export class ListSellerComponent implements OnInit, AfterViewChecked {
  @ViewChild('dateRangePicker') rangePickerView;
  public page: number = 1;
  dataTake: any;
  loading: any;
  @Input() limit: any = 15;
  faPhoneAlt = faPhoneAlt;
  faAngleDoubleLeft = faAngleDoubleLeft
  faAngleDoubleRight = faAngleDoubleRight
  faBox = faBox
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight
  faUserTie = faUserTie
  faExternalLinkAlt = faExternalLinkAlt
  public data: any
  public source = 'analytics/author';
  public currency: any;
  public currencySymbol: any;
  selectRangeSelectValue: any;
  displayedColumns: string[] = ['createdAt', 'customer', 'total', 'convert'];
  rangesTime = [];
  rangeDate: any
  rangeDateDisable = true

  constructor(private rest: RestService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    public pagination: PaginationServiceService,
    private route: ActivatedRoute,
    public search: SearchService,
    public analyticsService: AnalyticsService,
    public share: ShareService) {
    this.rangesTime = analyticsService.rangesTime
  }

  ngOnInit(): void {
    console.log(this.analyticsService.rangeSelect)
    const { startDate, endDate, selectValue } = this.analyticsService.rangeSelect
    const { currency, currencySymbol } = this.share.getSettings();
    let fields = [
      `?strict=true&fields=status`,
      `&start=${startDate}&end=${endDate}`
    ];
    this.currency = currency
    this.currencySymbol = currencySymbol
    this.selectRangeSelectValue = selectValue;
    this.search.setConfig({ fields, key: this.source, limit: this.limit })
    if (startDate && endDate && selectValue) {
      this.onSrc(`*paid`)
    }
  }

  showChart(event): void {
    _.forEach(this.dataTake, (v, k) => {
      v.show = (k === event)
      return v
    })
  }

  goToFilter = ($event, deposit: string) => {
    $event.stopPropagation();
    this.router.navigate(['/', 'products'], { queryParams: { fields: 'deposits', filter: deposit } })
  }


  /**** GLOBAL FUNCTIONS ****/

  load = (src: any, resetPagination = true) => {
    this.loading = true;
    this.data = this.search.find({
      source: this.source,
      query: src
    }, resetPagination).pipe(
      debounceTime(500),
      tap(a => this.dataTake = a.data),
      finalize(() => this.loading = false),
      map((a) => a.data)
    )
  }


  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => this.load(e);

  pageChanged($event: PageChangedEvent) {
    const { page } = $event;
    this.pagination.page = page;
    this.load(undefined, false);
  }


  /**** END GLOBAL FUNCTIONS ****/

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  selectRangeSelect($event: any, source: string) {
    if ($event && $event.value === 'custom') {
      this.rangeDateDisable = false
      this.rangePickerView.toggle()
    } else {
      const value = $event.value || $event

      if (value && value[0] && value[1]) {
        const starDate = moment(value[0]).format('YYYY-MM-DD')
        const endDate = moment(value[1]).format('YYYY-MM-DD')

        if (source === 'select') {
          this.analyticsService.rangeSelect = { startDate: starDate, endDate: endDate, selectValue: $event }
          this.rangeDate = [moment(value[0]).toDate(), moment(value[1]).toDate()]
        }
        this.search.reset()

        let fields = [
          `?strict=true&fields=status`,
          `&start=${this.analyticsService.rangeSelect.startDate}&end=${this.analyticsService.rangeSelect.endDate}`
        ];

        this.search.setConfig({ fields, key: this.source, limit: this.limit })
        this.load(`*paid`)
      }
    }

  }



}
