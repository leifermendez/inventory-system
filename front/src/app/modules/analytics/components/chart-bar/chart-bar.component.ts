import {Component, Input, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from "chart.js";
import {Label} from "ng2-charts";
import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
  selector: 'app-chart-bar',
  templateUrl: './chart-bar.component.html',
  styleUrls: ['./chart-bar.component.css']
})
export class ChartBarComponent implements OnInit {
  @Input() title: string
  @Input() data: any
  public barChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [];

  constructor() {
  }

  ngOnInit(): void {
    _.forEach(this.data, (v, k) => {
      const index = moment(v.createdAt).format(`DD, MMM YYYY`)
      v.chartIndex = index
      this.barChartLabels.push(index)
      this.barChartData.push({data: [v.total], label: index})
      return v
    })
    this.barChartLabels = _.uniq(this.barChartLabels)
    console.log('-------->', this.barChartData)
  }
}
