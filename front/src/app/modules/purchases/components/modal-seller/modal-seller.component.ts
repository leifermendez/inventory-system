import {Component, OnInit} from '@angular/core';
import {CurrencyMaskInputMode} from "ngx-currency";
import {ShareService} from "../../../../services/share.service";
import {BsModalRef} from "ngx-bootstrap/modal";
import {PurchaseService} from "../../purchase.service";
import {concat, Observable, of, Subject} from "rxjs";
import {catchError, distinctUntilChanged, map, switchMap, tap} from "rxjs/operators";
import {RestService} from "../../../../services/rest.service";

@Component({
  selector: 'app-modal-seller',
  templateUrl: './modal-seller.component.html',
  styleUrls: ['./modal-seller.component.css']
})
export class ModalSellerComponent implements OnInit {
  results$: Observable<any>;
  public section: any
  userLoading = false;
  userInput$ = new Subject<string>()
  author: any;

  constructor(private shared: ShareService, public bsModalRef: BsModalRef, private purchaseService: PurchaseService,
              private rest: RestService) {
  }

  ngOnInit(): void {
    this.loadUsers()
    this.author = this.section
  }

  selectUser = (e) => {
    if (e && e._id) {
      this.section = e;
    }
  }

  trackByFn(item: any) {
    return item._id;
  }

  private loadUsers() {
    this.results$ = concat(
      of([]), // default items
      this.userInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.userLoading = true),
        switchMap(term => this.singleSearch$(term).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.userLoading = false)
        ))
      )
    );
  }

  singleSearch$ = (term) => {
    const q = [
      `users?`,
      `filter=${term}`,
      `&fields=name,email,nameBusiness`,
      `&page=1&limit=5`,
      `&sort=name&order=-1`,
    ];
    return this.rest.get(q.join(''), true,
      {ignoreLoadingBar: ''}).pipe(
      map((a) => a.docs)
    )
  }

  close = () => {
    this.purchaseService.changeSeller.emit(this.section)
    this.bsModalRef.hide()
  }

  onFocus = (e) => {
    // console.log(this.section.prices)

  }

}
