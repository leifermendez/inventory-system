import {Component, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ShareService} from "../../../../services/share.service";
import {PDFReportService} from "../../../../../../projects/pdfreport/src/lib/pdfreport.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    event.returnValue = false;
  }
  public id: any = false;

  constructor(private route: ActivatedRoute,
              private share: ShareService,
              private pdf: PDFReportService,
              public router: Router) {
  }

  public history: any = [
    {
      name: 'Pedidos',
      router: ['/', 'purchase']
    },
    {
      name: 'Nuevo',
      router: null
    }
  ]

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
    });
  }

  save = () => {
    this.share.confirm().then(() => {
      this.share.savePurchase.emit(true)
    })
  }

  cbList() {
    this.router.navigate(['/', 'purchase'])
  }

  cbAdd(e) {
    this.share.addPurchase.emit(e)
  }

  cbPdf(id: any) {
    this.pdf.generatePurchase(id)
  }

  cbPay($event: any) {
    this.router.navigate(['/', 'orders', 'add'], {queryParams: {purchase: $event}})
  }

  cbConvert($event: any) {
    this.share.confirm().then(() => {
      this.share.savePurchase.emit($event)
    })
  }

  cbTrash($event: any) {
    this.share.deletePurchase.emit($event)
  }
}
