import {Component, Input, OnInit} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {faPhoneAlt, faIndustry, faUser, faUserTie, faListUl} from '@fortawesome/free-solid-svg-icons';
import {faCalendarAlt} from '@fortawesome/free-regular-svg-icons';
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {ShareService} from "../../../../services/share.service";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {PaginationServiceService} from "../../../../services/pagination-service.service";
import {faAngleLeft, faAngleRight, faAngleDoubleLeft, faAngleDoubleRight} from '@fortawesome/free-solid-svg-icons';
import {PDFReportService} from "../../../../../../projects/pdfreport/src/lib/pdfreport.service";
import {SearchService} from "../../../search/search.service";
import {debounceTime, distinctUntilChanged, finalize, map, tap} from "rxjs/operators";
import {Observable, of} from "rxjs";
import {PurchaseService} from "../../purchase.service";

@Component({
  selector: 'app-list-purchases',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({opacity: 0}),
          stagger(20, [
            animate(25, style({opacity: 1}))
          ])
        ], {optional: true})
      ])
    ])
  ]
})
export class ListComponent implements OnInit {
  @Input() mode: string = 'page'
  @Input() title: any = false;
  @Input() limit: any = 15;
  @Input() viewMore: boolean = true;
  public cbMode: any = false;
  @Input() dataTake: any
  @Input() data: Observable<any> = of([])
  loading: any;
  indexTab: number = 0

  constructor(private rest: RestService,
              private router: Router,
              public pagination: PaginationServiceService,
              private route: ActivatedRoute,
              private pdf: PDFReportService,
              private purchaseService: PurchaseService,
              private search: SearchService,
              public share: ShareService) {
  }

  faAngleDoubleLeft = faAngleDoubleLeft
  faAngleDoubleRight = faAngleDoubleRight
  faCalendarAlt = faCalendarAlt
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight
  faIndustry = faIndustry
  faPhoneAlt = faPhoneAlt
  faUser = faUser
  faUserTie = faUserTie
  public source = 'purchase';
  public currency = null;
  public currencySymbol = null;
  public history: any = [
    {
      name: 'Pedidos'
    }
  ]

  ngOnInit(): void {
    this.initLoad();
  }

  initLoad = () => {
    let fields = [
      `?fields=controlNumber,customer.name,customer.email,customer.lastName,tag,items.serials,invoiceNumber`
    ];
    this.search.setConfig({fields, key: this.source, limit: this.limit})
    const {currency, currencySymbol} = this.share.getSettings();
    this.currency = currency;
    this.currencySymbol = currencySymbol
    this.route.queryParams.subscribe(
      params => {
        const {q = ''} = params
        if (!this.data) {
          this.onSrc(q)
        }
      });

    this.changeTab({index: this.indexTab})
  }

  cbPdf() {
    this.pdf.generateList({source: this.source, data: this.dataTake})
  }


  /**** GLOBAL FUNCTIONS ****/

  load = (src: any, resetPagination = true) => {
    this.loading = true;
    this.data = this.search.find({
      source: this.source,
      query: src
    }, resetPagination).pipe(
      debounceTime(500),
      tap(a => this.dataTake = a.docs),
      finalize(() => this.loading = false),
      map((a) => a.docs)
    )
  }

  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => this.load(e);

  pageChanged($event: PageChangedEvent) {
    const {page} = $event;
    this.pagination.page = page;
    this.load(undefined, false);
  }

  changeTab = (event) => {
    if (event) {
      this.search.reset();
      const {index} = event;
      switch (index) {
        case 0:
          this.purchaseService.filterBy = '';
          break;
        case 1:
          this.purchaseService.filterBy = 'order_purchase';
          break;
        case 2:
          this.purchaseService.filterBy = 'order_pay';
          break;
        case 3:
          this.purchaseService.filterBy = 'order_invoice';
          break;
      }

      const {filterBy} = this.purchaseService
      let fields = [
        `?fields=controlNumber,customer.name,customer.email,customer.lastName,tag,items.serials,invoiceNumber`,
        (filterBy?.length) ? `&filterAnd=convert:${filterBy}` : ''
      ];
      this.search.setConfig({fields, key: this.source, limit: this.limit})
      this.load('')
    }
  }

  /**** END GLOBAL FUNCTIONS ****/

}
