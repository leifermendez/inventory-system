import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AddComponent} from "./pages/add/add.component";
import {HtmlEditComponent} from "./pages/html-edit/html-edit.component";

const routes: Routes = [
  {path: '', component: AddComponent},
  {path: ':tab', component: AddComponent},
  {path: ':tab/design/:name', component: HtmlEditComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {
}
