import {EventEmitter, Injectable, Output} from '@angular/core';
import {ModalCardPayComponent} from "../../components/modal-card-pay/modal-card-pay.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  @Output() tax = new EventEmitter<any>();
  @Output() taxOffset = new EventEmitter<any>();
  @Output() generalSave = new EventEmitter<any>();
  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService) {
  }

  public openModalCard = (data: any = {}) => {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalCardPayComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }
}
