import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlEditComponent } from './html-edit.component';

describe('HtmlEditComponent', () => {
  let component: HtmlEditComponent;
  let fixture: ComponentFixture<HtmlEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
