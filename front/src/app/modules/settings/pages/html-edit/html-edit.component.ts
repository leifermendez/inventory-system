import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import grapesjs from 'grapesjs';
import {HtmlEditService} from "./html-edit.service";
import {finalize} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {ShareService} from "../../../../services/share.service";
import {faTimes}
  from '@fortawesome/free-solid-svg-icons';
import {VideoHelpService} from "../../../../services/video-help.service";

@Component({
  selector: 'app-html-edit',
  templateUrl: './html-edit.component.html',
  styleUrls: ['./html-edit.component.css'],
})
export class HtmlEditComponent implements OnInit {
  @ViewChild('gjsElement') gjsElement: ElementRef
  options = {};
  loading: boolean
  typeInvoice: String
  private editor: any;
  private template: TemplateModel;
  history = []
  templatesHTML: Array<any> = []
  faTimes = faTimes

  constructor(private rest: RestService, public htmlEditService: HtmlEditService, private route: ActivatedRoute,
              public sharedService: ShareService, private router: Router, public videoHelpService: VideoHelpService) {
    this.route.params.subscribe(params => {
      this.typeInvoice = params.name
      if (this.typeInvoice != 'invoice' && this.typeInvoice != 'purchase') this.router.navigate(['settings'])
      this.history.push({
        name: params.name
      })
    });
  }

  ngOnInit(): void {
    this.loading = true
    this.loadInit()

  }

  grapesjInit = (responseSetting: any) => {
    try {
      const configInit = this.htmlEditService.callConfig(responseSetting)
      this.editor = grapesjs.init({
        ...configInit, ...{
          assetManager: {
            assets: [this.template.logo],
          },
          modal: this.htmlEditService.gasperVariableInvoice, // esto cambiarlo por un modal propio para mejro UX Ui
          plugins: ['grapesjs-custom-code', 'gjs-plugin-ckeditor'],
          pluginsOpts: {
            'grapesjs-custom-code': {},
            'gjs-plugin-ckeditor': {}
          }
        }
      });
      this.editor.setComponents(this.template?.components);
      this.editor.setStyle(this.template?.style);
      const panelManager = this.editor.Panels;
      const blockManager = this.editor.BlockManager;

      this.addAndRemove(panelManager)
      this.editor.on('load', () => {
        this.htmlEditService.makeZone()
        this.loadTemplateHTML(blockManager)
        const bm = this.editor.BlockManager;
        const blocks = bm.getAll();
        const templatesZone = []

        blocks.filter(a => {
          const id = a.get('id')
          if (id.includes('full-design')) {
            templatesZone.push(id)
          }
        })


        panelManager.getButton('views', 'open-blocks')
        blockManager.render([
          ...templatesZone.map(o => {
            return bm.get(o).set('category', 'Diseños')
          })
        ])
        panelManager.getButton('views', 'open-blocks').set('active', true)

        const categories = blockManager.getCategories();
        categories.each(category => {
          category.set('open', false).on('change:open', opened => {
            opened.get('open') && categories.each(category => {
              category !== opened && category.set('open', false)
            })
          })
        })

      })


      this.loading = false

    } catch (e) {
      this.loading = false
      alert('Error loading building')
    }
  }

  loadTemplateHTML = (blockManager) => {
    this.templatesHTML.forEach((t, i) => {
      t.label = `Diseño ${i + 1}`
      t.attributes = {class: 'fa fa-file'}
      console.log(t)
      blockManager.add(`template-${i}`, t)
    })
  }

  addAndRemove = (panelManager: any) => {
    this.editor.setDevice('Tablet')

    panelManager.removePanel('devices-c');
    // panelManager.removeButton('views', 'open-sm');
    panelManager.removeButton('views', 'open-tm');
    panelManager.removeButton('views', 'open-layers');

    panelManager.addButton('options', {
      id: 'saveBtn',
      className: 'btn-kitagil btn-save-kitagil',
      command: this.getHtml,
      attributes: {title: 'Save'},
      active: false,
    });
    panelManager.addButton('options', {
      id: 'clearBtn',
      className: 'btn-kitagil btn-trash-kitagil',
      command: this.clearAll,
      attributes: {title: 'Clear'},
      active: false,
    });
    panelManager.addButton('options', {
      id: 'showVariable',
      className: 'btn-kitagil btn-list-kitagil',
      command: this.openShow,
      attributes: {title: 'Show variable'},
      active: false,
    });

    panelManager.getButton('options', 'sw-visibility').set('active', false)
  }

  editorLoaded($event) {
    // this.emailEditor.editor.loadDesign(this.data);


  }

  loadInit = () => {
    try {
      this.loading = true;
      this.rest.get(`settings/invoiceTemplates`)
        .pipe(finalize(() => this.loading = false))
        .subscribe(res => {
          this.templatesHTML = res
          this.loadCurrentSetting()
        })
    } catch (e) {
      return null
    }
  }

  loadCurrentSetting = () => {
    try {
      this.rest.get(`settings/invoice`).subscribe(res => {
        const responseSetting = res;
        const {invoiceDesign = {}, purchaseDesign = {}, logo, name} = responseSetting
        this.template = {
          components: (this.typeInvoice === 'invoice') ? invoiceDesign.html || '' : purchaseDesign.html || '',
          style: (this.typeInvoice === 'invoice') ? invoiceDesign.css || '' : purchaseDesign.css || '',
          logo,
          name
        }
        this.grapesjInit(responseSetting)
      })
    } catch (e) {
      return null
    }
  }

  getHtml = () => {
    this.sharedService.confirm().then(() => {
      const css = this.editor.getCss();
      const html = this.editor.getHtml();
      const body = {html, css}
      this.saveDesign(body)
    })

  }

  clearAll = () => {
    this.sharedService.confirm().then(() => {
      this.editor.DomComponents.clear(); // Clear components
      this.editor.CssComposer.clear();  // Clear styles
      this.editor.UndoManager.clear(); // Clear undo history
    })
  }

  openShow = () => {
    const modal = this.editor.Modal;
    modal.open()
  }


  saveDesign(desing) {
    this.loading = true;
    let data = this.typeInvoice === 'invoice' ? {invoiceDesign: desing} : {purchaseDesign: desing}
    this.rest.patch(`settings/template/${this.typeInvoice}`, data)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => console.log(res))
  }

}

export class TemplateModel {
  components: string;
  style: string;
  logo: string;
  name: string;
}
