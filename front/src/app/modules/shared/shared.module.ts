import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from "../../components/header/header.component";
import {FooterComponent} from "../../components/footer/footer.component";
import {SideBarComponent} from "../../components/side-bar/side-bar.component";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {TooltipModule} from "ngx-bootstrap/tooltip";
import {TabsModule} from 'ngx-bootstrap/tabs';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ListItemsComponent} from "../../components/list-items/list-items.component";
import {DetailInvoiceComponent} from "../../components/detail-invoice/detail-invoice.component";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {BoxEmptyComponent} from "../../components/box-empty/box-empty.component";
import {WorkingBoxComponent} from "../../components/working-box/working-box.component";
import {LottieModule} from "ngx-lottie";
import {LockedBoxComponent} from "../../components/locked-box/locked-box.component";
import {ProductFormComponent} from "../../components/product-form/product-form.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgxEditorModule} from "ngx-editor";
import {TranslateModule} from "@ngx-translate/core";
import {TagInputModule} from "ngx-chips";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgxCurrencyModule} from "ngx-currency";
import {ModalModule} from 'ngx-bootstrap/modal';
import {RouterModule} from "@angular/router";
import {ProviderFormComponent} from "../../components/provider-form/provider-form.component";
import {DepositFormComponent} from "../../components/deposit-form/deposit-form.component";
import {SectionBtnComponent} from "../../components/section-btn/section-btn.component";
import {UserFormComponent} from "../../components/user-form/user-form.component";
import {AvatarModule} from "ngx-avatar";
import {ModalUserComponent} from "../../components/modal-user/modal-user.component";
import {TimeagoModule} from "ngx-timeago";
import {QuillModule} from "ngx-quill";
import {InventoryFormComponent} from "../../components/inventory-form/inventory-form.component";
import {NgxDropzoneModule} from "ngx-dropzone";
import {PurchaseFormComponent} from "../../components/purchase-form/purchase-form.component";
import {FirstImagePipe} from "../../pipe/first-image.pipe";
import {FirstValuePipe} from "../../pipe/first-value.pipe";
import {SwipeAngularListModule} from "swipe-angular-list";
import {TotalPurchasePipe} from "../../pipe/total-purchase.pipe";
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {RandomAvatarPipe} from "../../pipe/random-avatar.pipe";
import {ListAddonsComponent} from "../../components/list-addons/list-addons.component";
import {CountSearchDataPipe} from "../../pipe/count-search-data.pipe";
import {NgxCopilotModule} from "../../../../projects/ngx-copilot/src/lib/ngx-copilot.module";
import {SafeHtmlPipe} from "../../pipe/safe-html.pipe";
import {OrderFormComponent} from "../../components/order-form/order-form.component";
import {ListComponent} from "../orders/pages/list/list.component";
import {DropGalleryComponent} from "../../components/drop-gallery/drop-gallery.component";
import {Base64ImagePipe} from "../../pipe/base64-image.pipe";
import {PDFReportModule} from "../../../../projects/pdfreport/src/lib/pdfreport.module";
import {FilterDepositPipe} from "../../pipe/filter-deposit.pipe";
import {ExcelImportModule} from "../../../../projects/excel-import/src/lib/excel-import.module";
import {InvoicePlaceHolderPipe} from "../../pipe/invoice-place-holder.pipe";
import {
  CalendarDateFormatter,
  CalendarModule,
  CalendarMomentDateFormatter,
  DateAdapter,
  MOMENT,
} from 'angular-calendar';
import {CalendaryComponent} from "../../components/calendary/calendary.component";
import {GetTaxPipe} from "../../pipe/get-tax.pipe";
import {CehckArrayPipe} from "../../pipe/cehck-array.pipe";
import {NotificationDropComponent} from "../../components/notification-drop/notification-drop.component";
import {CanLeaveModule} from "../../can-leave/can-leave.module";
import {ToDatePipe} from "../../pipe/to-date.pipe";
import {LoadingBtnDirective} from "../../directives/loading-btn.directive";
import {adapterFactory} from 'angular-calendar/date-adapters/moment';
import * as moment from 'moment';
import {ModalPayComponent} from "../../components/modal-pay/modal-pay.component";
import {CheckPluginDirective} from "../../directives/check-plugin.directive";
import {NgxFunnyDatepickerModule} from "ngx-funny-datepicker";
import {ShowRoleDirective} from "../../directives/show-role.directive";
import {PlanExpiredComponent} from "../../components/plan-expired/plan-expired.component";
import {StripeHtmlPipe} from "../../pipe/stripe-html.pipe";
import {ModalCardPayComponent} from "../../components/modal-card-pay/modal-card-pay.component";
import {NotImagesDirective} from "../../directives/not-images.directive";
import { NavbarComponent } from '../../components/navbar/navbar.component';

export function momentAdapterFactory() {
  return adapterFactory(moment);
}

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SideBarComponent,
    ListItemsComponent,
    DetailInvoiceComponent,
    BoxEmptyComponent,
    WorkingBoxComponent,
    LockedBoxComponent,
    ProductFormComponent,
    ProviderFormComponent,
    DepositFormComponent,
    SectionBtnComponent,
    UserFormComponent,
    ListItemsComponent,
    ModalUserComponent,
    InventoryFormComponent,
    PurchaseFormComponent,
    FirstImagePipe,
    FirstValuePipe,
    RandomAvatarPipe,
    TotalPurchasePipe,
    ListAddonsComponent,
    CountSearchDataPipe,
    SafeHtmlPipe,
    OrderFormComponent,
    ListComponent,
    DropGalleryComponent,
    Base64ImagePipe,
    FilterDepositPipe,
    InvoicePlaceHolderPipe,
    CalendaryComponent,
    GetTaxPipe,
    CehckArrayPipe,
    ToDatePipe,
    NotificationDropComponent,
    LoadingBtnDirective,
    ModalPayComponent,
    CheckPluginDirective,
    ShowRoleDirective,
    PlanExpiredComponent,
    StripeHtmlPipe,
    ModalCardPayComponent,
    NotImagesDirective,
    NavbarComponent
  ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        TimeagoModule.forRoot(),
        PaginationModule.forRoot(),
        CalendarModule.forRoot(
            {
                provide: DateAdapter,
                useFactory: momentAdapterFactory,
            },
            {
                dateFormatter: {
                    provide: CalendarDateFormatter,
                    useClass: CalendarMomentDateFormatter,
                },
            }
        ),
        NgxCurrencyModule,
        CanLeaveModule,
        BsDropdownModule,
        BsDatepickerModule,
        AvatarModule,
        LottieModule,
        NgSelectModule,
        FormsModule,
        NgxEditorModule,
        ReactiveFormsModule,
        TranslateModule,
        TagInputModule,
        RouterModule,
        QuillModule,
        NgxDropzoneModule,
        SwipeAngularListModule,
        NgxCopilotModule,
        PDFReportModule,
        ExcelImportModule,
        NgxFunnyDatepickerModule
    ],
  providers: [
    {
      provide: MOMENT,
      useValue: moment,
    }],
    exports: [HeaderComponent, SideBarComponent, WorkingBoxComponent,
        ListItemsComponent, DetailInvoiceComponent, BoxEmptyComponent,
        LockedBoxComponent, ProductFormComponent, FirstValuePipe, GetTaxPipe,
        ProviderFormComponent, DepositFormComponent, PurchaseFormComponent,
        SectionBtnComponent, InventoryFormComponent, FirstImagePipe, CalendaryComponent,
        TotalPurchasePipe, RandomAvatarPipe, ListAddonsComponent, FilterDepositPipe, CehckArrayPipe,
        CountSearchDataPipe, ListComponent, DropGalleryComponent, Base64ImagePipe, InvoicePlaceHolderPipe,
        UserFormComponent, ListItemsComponent, ModalUserComponent, SafeHtmlPipe, OrderFormComponent,
        NotificationDropComponent, ToDatePipe, LoadingBtnDirective, ModalPayComponent, CheckPluginDirective,
      ShowRoleDirective, StripeHtmlPipe, NotImagesDirective, NavbarComponent

      ]
})
export class SharedModule {
}
