import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {LocalStorageService} from "ngx-localstorage";

@Directive({
  selector: '[appCheckPlugin]'
})
export class CheckPluginDirective implements OnInit {
  @Input() namePlugin: string;

  constructor(private storageService: LocalStorageService, private elementRef: ElementRef) {
    if(elementRef.nativeElement &&  elementRef.nativeElement.style){
      elementRef.nativeElement.style.display = 'none';
    }

  }

  async ngOnInit(): Promise<any> {
    if(this.elementRef.nativeElement &&  this.elementRef.nativeElement.style){
      this.elementRef.nativeElement.style.display = (await this.checkPlugins(this.namePlugin)) ? 'block' : 'none';
    }
  }

  checkPlugins = (name = '', force = true) => {
    return new Promise((resolve, reject) => {
      this.storageService.asPromisable().get('plugins').then(res => {
        console.log(res)
        const pluginName = res.includes(name);
        resolve(pluginName);
      }).catch((e) => {
        reject(false)
      })
    })
  }

}
