import {AfterContentChecked, AfterViewInit, Directive, ElementRef, Input, OnInit} from '@angular/core';
import {LocalStorageService} from "ngx-localstorage";
import {CookieService} from "ngx-cookie-service";

@Directive({
  selector: '[appShowRole]'
})
export class ShowRoleDirective implements OnInit {
  @Input() role: Array<string>;

  constructor(private storageService: LocalStorageService, private elementRef: ElementRef, private cookie: CookieService) {
  }

  async ngOnInit(): Promise<any> {
    if (!await this.checkRole(this.role)) {
      this.elementRef.nativeElement.remove()
    }
  }

  checkRole = (rolesList: any) => {
    return new Promise((resolve, reject) => {
      const roles = JSON.parse(this.cookie.get('user')) || null
      if (roles) {
        resolve(rolesList.includes(roles.role))
      } else {
        reject(false)
      }
    })
  }


}
