import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appNotImages]'
})
export class NotImagesDirective {

  @Input() urlCustom: string
  constructor(private elementRef: ElementRef) { }


  @HostListener('error')
  loadDefault() {
    const element = this.elementRef.nativeElement
    element.src = this.urlCustom || `${window.location.origin}/assets/images/not-image.png`
  }

}
