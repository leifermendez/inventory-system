import { TestBed } from '@angular/core/testing';

import { VideoHelpService } from './video-help.service';

describe('VideoHelpService', () => {
  let service: VideoHelpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoHelpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
