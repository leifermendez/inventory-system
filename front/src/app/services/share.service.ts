import {EventEmitter, Injectable, Output} from '@angular/core';
import {Router} from "@angular/router";
import Swal from 'sweetalert2';
import {TranslateService} from "@ngx-translate/core";
import {CookieService} from "ngx-cookie-service";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalUpdateComponent} from "../components/modal-update/modal-update.component";
import {DomSanitizer, Title} from "@angular/platform-browser";
import {split} from "ts-node";
import {LocalStorageService} from "ngx-localstorage";
import {defer, Observable, of} from "rxjs";
import {Toaster} from "ngx-toast-notifications";
import {ModalCardPayComponent} from "../components/modal-card-pay/modal-card-pay.component";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ShareService {
  @Output() registerUser = new EventEmitter<string>();
  @Output() common = new EventEmitter<any>();
  @Output() purchaseData = new EventEmitter<any>();
  @Output() addPurchase = new EventEmitter<any>();
  @Output() savePurchase = new EventEmitter<any>();
  @Output() deletePurchase = new EventEmitter<any>();
  @Output() loading = new EventEmitter<any>();
  @Output() copilot = new EventEmitter<any>();
  @Output() limitAccount = new EventEmitter<any>();
  @Output() changeSetting = new EventEmitter<any>();
  @Output() openMenu = new EventEmitter<any>();
  payPlan = new EventEmitter()
  bsModalRef: BsModalRef;
  public unSave: any
  public disablePlugin: string = ''

  constructor(private router: Router,
              private sanitizer: DomSanitizer,
              private modalService: BsModalService,
              private cookie: CookieService,
              private toaster: Toaster,
              private title: Title,
              private storageService: LocalStorageService,
              private translate: TranslateService) {
  }

  updateTitle = (title: string) => {
    this.title.setTitle(title);
  }

  checkPlugins = (name = '', force = true) => {
    return new Promise((resolve, reject) => {
      this.storageService.asPromisable().get('plugins')
        .then(res => {
          // console.log('--->',res, name)
          const pluginName = res.includes(name);
          console.log('aaaaa--', pluginName)
          if (pluginName) {
            resolve(pluginName)
          } else {
            resolve(null)
          }
        }).catch(err => {
        resolve(null)
      })
    })

  }

  showToast = (source: string) => {
    try {
      this.translate.get(`${source}.TOAST`).subscribe(res => {
        if (typeof res === 'string') {
          this.translate.get(`ERRORS.UNDEFINED`).subscribe(r => {
            this.toaster.open({
              text: r.TEXT,
              caption: r.TITLE,
            });
          });
        } else {
          this.toaster.open({
            text: res.TEXT,
            caption: res.TITLE,
          });
        }
      });
    } catch (e) {
      return 422;
    }
  }

  public prepare<T>(callback: () => void): (source: Observable<T>) => Observable<T> {
    return (source: Observable<T>): Observable<T> => defer(() => {
      callback();
      return source;
    });
  }


  generate = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  public checkCopilot = () => new Promise((resolve, reject) => {
    try {
      const cookie = this.cookie.get('globalCopilot');
      resolve(cookie)
    } catch (e) {
      resolve(false)
    }
  })

  public parseData = (data: any, source: string = '') => {
    try {
      const tmp = [];
      data.docs.map(a => tmp.push({
        ...a, ...{
          router: ['/', source, a._id]
        }
      }));
      return tmp;
    } catch (e) {
      return null;
    }
  }

  public goTo = (source: string = '') => this.router.navigate(['/', source, 'add'])

  public findInvalidControls(form: any) {
    const invalid = [];
    const controls = form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  public nextPage = (data: any) => {
    return data.nextPage;
  }

  public parseLoad = (src: string = '', source: string = '', fields = []) => {
    let q: (string | any[])[] = [source];
    q = (fields.length) ? [...q, ...fields] : [...q,
      ...[`?sort=updateAt&order=-1`
      ]
    ]
    return q;
  };


  public toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  public openCopilot = (section = null) => new Promise((resolve, reject) => {
    try {
      if (section) {
        const copilot = (this.cookie.get(section)) ? this.cookie.get(section) : null;
        if (copilot) {
          resolve(copilot)
        } else {
          resolve(null)
        }
      }
    } catch (e) {
      reject(null);
    }
  })

  public saveCopilot = (section = null) => new Promise((resolve, reject) => {
    try {
      this.cookie.set(section, '1', 365, '/')
      resolve(true)
    } catch (e) {
      reject(null);
    }
  })

  public confirm = () => new Promise((resolve, reject) => {
    this.translate.get("GENERAL").subscribe((res: string) => {
      // @ts-ignore
      const {ARE_YOU_SURE, ARE_YOU_SURE_SENTENCE, OK, ANY_ISSUE} = res;
      // console.log(res)
      Swal.fire({
        title: ARE_YOU_SURE,
        text: ARE_YOU_SURE_SENTENCE,
        icon: null,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: OK,
        footer: '<a href>' + ANY_ISSUE + '</a>'
      }).then((result) => {
        if (result.value) {
          resolve(true)
        } else {
          reject(false)
        }
      }).then()
    });

  })

  public leaveDialog = () => new Observable((observer) => {
    this.translate.get("GENERAL").subscribe((res: string) => {
      // @ts-ignore
      const {LEAVE_ARE_YOU_SURE, LEAVE_ARE_YOU_SURE_SENTENCE, OK, ANY_ISSUE} = res;
      Swal.fire({
        title: LEAVE_ARE_YOU_SURE,
        text: LEAVE_ARE_YOU_SURE_SENTENCE,
        icon: null,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: OK,
        footer: '<a href>' + ANY_ISSUE + '</a>'
      }).then(res => {
        observer.next(res.isConfirmed)
        observer.complete()
      })
    })
  })

  public alert = (title = '', text = '') => {
    Swal.fire({
      title,
      text,
      icon: null
    })
  }

  public openUpdateModal = (data: any = {}) => {
    const initialState: any = {
      section: data
    };
    this.bsModalRef = this.modalService.show(
      ModalUpdateComponent,
      Object.assign({initialState}, {
        class: 'modal-light-upgrade',
        ignoreBackdropClick: true
      })
    );
  }


  public getUserInfo = () => {
    try {
      return JSON.parse(this.cookie.get('user'))
    } catch (e) {
      return null
    }
  }

  public checkRole = (roles = []) => {
    try {
      let data = this.getUserInfo();
      data = (data) ? data : [];
      return roles.includes(data.role);
    } catch (e) {
      return false;
    }
  }

  public getSettings = () => {
    try {
      const data = JSON.parse(this.cookie.get('settings'));
      if (data) {
        return data;
      } else {
        return {
          name: null,
          currency: null,
          logo: null
        }
      }

    } catch (e) {
      this.cookie.delete('session', '/');
      this.cookie.delete('settings', '/');
      this.cookie.delete('user', '/');
      return {
        name: null,
        currency: null,
        logo: null
      }
    }
  }

  public setSettings = (key, value) => {
    try {
      const data = JSON.parse(this.cookie.get('settings'));
      data[key] = value;
      this.cookie.set('settings', JSON.stringify(data), environment.daysTokenExpire, '/')
      return data;
    } catch (e) {
      return null
    }
  }

}
