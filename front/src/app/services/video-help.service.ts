import {Injectable} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {VideoHelpComponent} from "../components/video-help/video-help.component";

@Injectable({
  providedIn: 'root'
})
export class VideoHelpService {
  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService) {
  }

  open(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      VideoHelpComponent,
      {
        initialState,
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      }
    );
  }
}
