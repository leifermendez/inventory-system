import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {CookieService} from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class ReferredGuard implements CanActivate {
  constructor(private cookieService: CookieService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    try {
      const {ref = null} = next.queryParams;
      if (ref) {
        this.cookieService.set('userReferred', ref, 9999, '/');
      }
      return true;
    } catch (e) {
      return false;
    }

  }

}
