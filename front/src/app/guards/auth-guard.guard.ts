import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(private auth: AuthService) {

  }

  canActivate() {
    return this.auth.checkSession(true, true).then(a => {
      return true;
    }).catch(e => {
      return false;
    });
  }

}

export interface HasUnsavedData {
  hasUnsavedData(): boolean;
}
