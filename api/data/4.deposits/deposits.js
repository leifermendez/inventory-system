const faker = require('faker')
const ObjectID = require('mongodb').ObjectID
const manager = require('../1.users/user')

module.exports = [
    {
        _id: new ObjectID('5f638cfe8392a15730f40288'),
        name: 'Dickinson - Upton',
        address: faker.address.streetAddress(),
        manager:manager[0],
        phone: faker.phone.phoneNumber(),
        tenantId: 'subdomain'
    }
]