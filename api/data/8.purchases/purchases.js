const faker = require('faker')
const ObjectID = require('mongodb').ObjectID
const author = require('../1.users/user')
const provider = require('../6.providers/providers')
const deposit = require('../4.deposits/deposits')
const products = require('../5.products/products')

var items = []
items[0] = products[0]
items[0].qty = 1
items[0].depositList = [ deposit[0] ]
items[0].deposits = [ deposit[0].name ]
items[0].providers = [ provider[0].name ]
items[0].deposit = deposit[0]



module.exports = [
    {
        _id: new ObjectID('5f721a3c3429633fa89bf86a'),
        controlNumber: 'SEED-1601313425Mo',
        status : "hold",
        deliveryType : "to_send",
        total : 200,
        customer : author[2],
        items : items,
        deliveryAddress : "Madrid",
        invoiceAddress : faker.address.streetAddress(),
        description : "Order Seed",
        author : author[0]._id,
        tenantId : "subdomain",
    }
]