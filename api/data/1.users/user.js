const faker = require('faker')
const ObjectID = require('mongodb').ObjectID

module.exports = [
  {
    _id: new ObjectID('5aa1c2c35ef7a4e97b5e995a'),
    name: 'Super Administrator',
    email: 'admin@admin.com',
    password: '$2a$05$2KOSBnbb0r.0TmMrvefbluTOB735rF/KRZb4pmda4PdvU9iDvUB26',
    role: 'admin',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148f',
    city: 'Bucaramanga',
    country: 'Colombia',
    phone: '123123',
    tenantId: 'subdomain',
    urlTwitter: faker.internet.url(),
    urlGitHub: faker.internet.url(),
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    _id: new ObjectID('5aa1c2c35ef7a4e97b5e995b'),
    name: 'Simple user',
    email: 'user@user.com',
    password: '$2a$05$2KOSBnbb0r.0TmMrvefbluTOB735rF/KRZb4pmda4PdvU9iDvUB26',
    role: 'user',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148d',
    city: 'Bucaramanga',
    country: 'Colombia',
    phone: '123123',
    tenantId: 'subdomain',
    referredCode:faker.random.hexaDecimal(),
    urlTwitter: faker.internet.url(),
    urlGitHub: faker.internet.url(),
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  // Usuarios para las pruebas
  {
    _id: new ObjectID('5aa1c2c35ef7a4e97b5e995c'),
    name: 'Manager user',
    email: 'manager@manager.com',
    password: '$2a$05$2KOSBnbb0r.0TmMrvefbluTOB735rF/KRZb4pmda4PdvU9iDvUB26',
    role: 'manager',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148j',
    city: 'Bucaramanga',
    country: 'Colombia',
    phone: '123123',
    tenantId: 'subdomain',
    referredCode:faker.random.number({
      'min': 500,
      'max': 50000
    }),
    urlTwitter: faker.internet.url(),
    urlGitHub: faker.internet.url(),
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    _id: new ObjectID('5aa1c2c35ef7a4e97b5e995d'),
    name: 'Seller user',
    email: 'seller@seller.com',
    password: '$2a$05$2KOSBnbb0r.0TmMrvefbluTOB735rF/KRZb4pmda4PdvU9iDvUB26',
    role: 'seller',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148h',
    city: 'Bucaramanga',
    country: 'Colombia',
    phone: '123123',
    tenantId: 'subdomain',
    referredCode:faker.random.number({
      'min': 500,
      'max': 50000
    }),
    urlTwitter: faker.internet.url(),
    urlGitHub: faker.internet.url(),
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    _id: new ObjectID('5aa1c2c35ef7a4e97b5e995f'),
    name: 'Customer customer',
    email: 'customer@customer.com',
    password: '$2a$05$2KOSBnbb0r.0TmMrvefbluTOB735rF/KRZb4pmda4PdvU9iDvUB26',
    role: 'customer',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148h',
    city: 'Bucaramanga',
    country: 'Colombia',
    phone: '123123',
    tenantId: 'subdomain',
    referredCode:faker.random.number({
      'min': 500,
      'max': 50000
    }),
    urlTwitter: faker.internet.url(),
    urlGitHub: faker.internet.url(),
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  }
]
