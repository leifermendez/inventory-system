const faker = require('faker')
const ObjectID = require('mongodb').ObjectID
const author = require('../1.users/user')
const product = require('../5.products/products')
const provider = require('../6.providers/providers')
const deposit = require('../4.deposits/deposits')

module.exports = [
    {
        _id: new ObjectID('5f68d83c53ee1e2a64529f98'),
        product: product[0],
        address: faker.address.streetAddress(),
        author:author[0],
        provider: provider[0],
        qty: 20,
        priceBase: 10,
        deposit: deposit[0],
        trace:'',
        description: 'Movimiento Seed',
        tag: ['Seed'],
        tenantId: 'subdomain'
    }
]