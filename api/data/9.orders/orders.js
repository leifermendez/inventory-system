const faker = require('faker')
const ObjectID = require('mongodb').ObjectID
const author = require('../1.users/user')
const purchases = require('../8.purchases/purchases')

module.exports = [
    {
        _id: new ObjectID('5f721a3c3429633fa89bf86a'),
        status : "success",
        tag : [ 
            "PRUEBA"
        ],
        customer : author[3]._id,
        platform : "cash",
        amount : "200",
        purchase : purchases[0]._id,
        reference : "REF-PRUEBA",
        description : "PRUEBA",
        author : author[0]._id,
        currency : "Dong",
        tenantId : "subdomain",
    }
]