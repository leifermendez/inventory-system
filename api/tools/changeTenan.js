require('dotenv-safe').config()
const initMongo = require('../config/mongo')
const userModel = require('../app/models/user')

const change = async () => {
  const TENANT_PREV = 'macoba';
  const TENANT_NEXT = 'divenca';

  const getPrevUser = await userModel.byTenant(TENANT_PREV).find({})
  const newUsers = getPrevUser.map(a => {
    a = a.toObject()
    delete a._id
    a.password = 'token_divenca'
    return {...a,...{tenantId:TENANT_NEXT}}
  })


  await userModel.insertMany(newUsers)

  console.log(newUsers)
}

change()

initMongo()
