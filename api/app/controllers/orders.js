const pluginStripe = require('../plugins/stripe')
const pluginPaypal = require('../plugins/paypal')
const mongoose = require('mongoose')
const { nanoid } = require('nanoid')
const model = require('../models/orders')
const Settings = require('../models/settings')
const { matchedData } = require('express-validator')
const utils = require('../middleware/utils')
const db = require('../middleware/db')


/*********************
 * Private functions *
 *********************/
/**
 * Gets all items from database
 */
const getAllItemsFromDB = async (tenant = null) => {
  return new Promise((resolve, reject) => {
    model
      .byTenant(tenant)
      .find(
        {},
        '-updatedAt -createdAt',
        {
          sort: {
            name: 1
          }
        },
        (err, items) => {
          if (err) {
            reject(utils.buildErrObject(422, err.message))
          }
          resolve(items)
        }
      )
  })
}


const checkPlugin = (plugins = []) => {
  return plugins.filter(p => p.path === 'stripe').find(() => true)
}

/**
 * Get with inventory
 */


/********************
 * Public functions *
 ********************/

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getAllItems = async (req, res) => {
  try {
    res.status(200).json(await getAllItemsFromDB())
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItems = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const query = await db.checkQueryFilterMulti(req.query, model);
    // console.log(JSON.stringify(query))
    // const secondQuery = await db.checkSecondQueryString(req.query);
    const data = db.getLookListOrders(model, query, tenant)
    res.status(200).json(await db.getItemsAggregate(req, model, data, tenant))
    // res.status(200).json(await db.getItems(req, model, query))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Get item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    const id = await utils.isIDGood(req.id, true);
    const aggregate = [
      {
        $lookup: {
          from: 'users',
          let: { idUser: '$customer' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ['$$idUser', '$_id'] }]
                }
              }
            },
            {
              $project: {
                _id: 1,
                name: 1,
                email: 1,
                role: 1,
                lastName: 1
              }
            }
          ],
          as: 'customer'
        }
      },
      {
        $lookup: {
          from: 'users',
          let: { idAuthor: '$author' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ['$$idAuthor', '$_id'] }]
                }
              }
            },
            {
              $project: {
                _id: 1,
                name: 1,
                email: 1,
                role: 1,
                lastName: 1
              }
            }
          ],
          as: 'author'
        }
      },
      // {
      //   $lookup: {
      //     from: 'purchases',
      //     let: {idPurchase: '$purchase._id'},
      //     pipeline: [
      //       {
      //         $match: {
      //           $expr: {
      //             $and: [{$eq: ['$$idPurchase', '$_id']}]
      //           }
      //         }
      //       }
      //     ],
      //     as: 'purchase'
      //   }
      // },
      { $unwind: '$customer' },
      { $unwind: '$author' },
      // {$unwind: '$purchase'},
      {
        $project: {
          _id: 1,
          status: 1,
          platform: 1,
          customer: 1,
          purchase: 1,
          reference: 1,
          amount: 1,
          currency: 1,
          author: 1,
          description: 1,
          tag: 1,
          customData: 1,
          createdAt: 1,
          updatedAt: 1
        }
      },
      {
        $match: { _id: id }
      }
    ]

    res.status(200).json(await db.getItemAggregate(aggregate, model, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Update item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.updateItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const { currency = null } = await utils.getSingleValue(Settings, {}, tenant);
    const author = req.user;
    req = matchedData(req)
    req = {
      ...req, ...{
        author: author._id,
        currency,
        purchase: (req.purchase._id) ? { ...req.purchase, ...{ _id: mongoose.Types.ObjectId(req.purchase._id) } } : null
      }
    }
    const id = await utils.isIDGood(req.id)
    res.status(200).json(await db.updateItem(id, model, req, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.createItem = async (req, res) => {
  try {
    const tracker = nanoid(25);
    let customData = {};
    const tenant = req.clientAccount;
    const { currency = null } = await utils.getSingleValue(Settings, {}, tenant);
    const author = req.user;
    req = matchedData(req)
    customData = {
      tracker,
      stripeTrack: await pluginStripe.generate_link(req, tenant),
      paypalLink: await pluginPaypal.generate_link(req, tenant, tracker)
    }
    req = {
      ...req, ...{
        author: author._id,
        currency,
        customData,
        purchase: (req.purchase._id) ? { ...req.purchase, ...{ _id: mongoose.Types.ObjectId(req.purchase._id) } } : null
      }
    }

    res.status(201).json(await db.createItem(req, model, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Delete item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.deleteItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    const id = await utils.isIDGood(req.id)
    res.status(200).json(await db.deleteItem(id, model, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}
