const model = require('../models/purchase')
const Settings = require('../models/settings')
const modelInventory = require('../models/inventory')
const invoiceService = require('../service/invoice')
const notificationService = require('../service/notification')
const calendarService = require('../service/calendar.service')
const {matchedData} = require('express-validator')
const utils = require('../middleware/utils')
const db = require('../middleware/db')
const AuthController = require('../controllers/auth')
const mongoose = require('mongoose')
const moment = require('moment')
const _ = require('lodash')
/*********************
 * Private functions *
 *********************/

const getSettingInvoice = (tenant = null) => new Promise((resolve, reject) => {
  Settings.byTenant(tenant).findOne({}, (err, item) => {
    if (!err) {
      resolve(item)
    } else {
      reject(err)
    }
  })
})

const convertPurchase = async (id, req, settingData, tenant) => {
  const rawDownload = await invoiceService.generatePurchase(id, tenant)
  req = {download: rawDownload.download}
  return req
}

const convertInvoice = async (id, req, settingData, tenant) => {

  const invoiceNumber = parseInt(settingData.initialInvoice + 1);
  await updateInvoiceNumber(tenant, settingData._id, {initialInvoice: invoiceNumber})

  req = {
    ...req, ...{
      invoiceNumber: formatInvoice(invoiceNumber, settingData.invoiceFormat)
    }
  }
  await insideCreate(req, tenant)
  const rawDownload = await invoiceService.generateInvoice(id, invoiceNumber, tenant)
  req = {...req, ...{download: rawDownload.download}}
  return req

}

const convertGenerateInvoice = async (id, req, settingData, tenant) => {
  const checkCurrentData = await db.getItem(id, model, tenant);
  const invoiceNumber = checkCurrentData.invoiceNumber  ||  parseInt(settingData.initialInvoice);
  const rawDownload = await invoiceService.generateInvoice(id, invoiceNumber, tenant)
  req = {download: rawDownload.download}
  return req

}

/**
 * Send to calendar
 * @param data
 * @param tenant
 * @returns {Promise<unknown>}
 */
const sendToCalendar = async (data, tenant) => {
  try {
    const status = ['order_pay']
    if (data && data.deliveryDate && status.includes(data.convert)) {
      await calendarService.addDelivery(data, tenant)
    }
  } catch (e) {
    return e
  }
}

const updateInvoiceNumber = (tenant = null, id, req) => new Promise((resolve, reject) => {
  try {

    Settings.byTenant(tenant).findByIdAndUpdate(
      id,
      req,
      {
        new: true,
        runValidators: true
      },
      (err, item) => {
        if (!err) {
          resolve(item)
        } else {
          reject(err)
        }
      }
    )
  } catch (e) {
    console.log(e)
    reject(e)
  }
})

const formatInvoice = (numberInvoice = '', formatInvoice = '') => {
  numberInvoice = numberInvoice.toString();
  formatInvoice = formatInvoice.replace(/%/gi, '0');
  const partFormat = formatInvoice.slice(0, -numberInvoice.length);
  return `${partFormat}${numberInvoice}`
}
/**
 * Use in model
 */
const insideCreate = async (data, tenant = null) => {
  try {

    const authorId = await utils.isIDGood(data.author, true);
    const author = await AuthController.findUserById(authorId, tenant);
    let inventory = data.items.map((item) => {
        const deposit = {...item.deposit, ...{_id: mongoose.Types.ObjectId(item.deposit._id)}}
        return {
          product: {
            ...item, ...{
              _id: mongoose.Types.ObjectId(item._id),
              id: null
            }
          },
          provider: null,
          qty: (Math.abs(item.qty) * -1),
          deposit,
          author,
          purchase: mongoose.Types.ObjectId(data.id),
          createdAt: moment().toDate(),
          updatedAt: moment().toDate(),
          deleted: false,
          deletedAt: null
        };
      }
    )
    modelInventory.byTenant(tenant).remove({
      purchase: mongoose.Types.ObjectId(data.id),
    }, (err, item) => {
      if (!err) {
        modelInventory.byTenant(tenant).insertMany(inventory)
      }
    })

  } catch (e) {

    return null
  }
}
/**
 * Gets all items from database
 */
const getAllItemsFromDB = async () => {
  return new Promise((resolve, reject) => {
    model.find(
      {},
      '-updatedAt -createdAt',
      {
        sort: {
          name: 1
        }
      },
      (err, items) => {
        if (err) {
          reject(utils.buildErrObject(422, err.message))
        }
        resolve(items)
      }
    )
  })
}

/**
 * Parse multi filter status
 */

const parseStatus = (inData) => {
  try {
    inData = inData.split(':');
    const raw = {
      [_.head(inData)]: _.last(inData)
    };
    return {
      '$and': [raw]
    }
  } catch (e) {
    return []
  }
}

/********************
 * Public functions *
 ********************/

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getAllItems = async (req, res) => {
  try {
    res.status(200).json(await getAllItemsFromDB())
  } catch (error) {
    utils.handleError(res, error)
  }
}


/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItems = async (req, res) => {
  try {
    const {filterAnd} = req.query
    const tenant = req.clientAccount;
    let query = await db.checkQueryString(req.query);
    query = {...query, ...parseStatus(filterAnd)};
    const data = db.getLookListPurchases(model, query, tenant, req.user)
    res.status(200).json(await db.getItemsAggregate(req, model, data, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Get item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    const id = await utils.isIDGood(req.id, true)
    const data = await db.getLookListPurchases(model, {_id: id}, tenant).exec();
    res.status(200).json(data.find(a => true))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Update item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.updateItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    // const author = await utils.getUserCurrent(req)
    req = matchedData(req)
    const {customer, convert = null} = req
    req = {
      ...req, ...{
        customer: {
          _id: mongoose.Types.ObjectId(customer._id),
          name: customer.name,
          email: customer.email
        }
      }
    }
    const id = await utils.isIDGood(req.id)
    const data = await db.updateItem(id, model, req, tenant)
    await sendToCalendar(data, tenant)
    res.status(200).json(data)
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Update convert item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.updateItemConvert = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const author = req.user;
    req = matchedData(req)
    const id = await utils.isIDGood(req.id)
    const {convert = ''} = req
    const settingData = await getSettingInvoice(tenant)
    req = {
      ...req, ...{
        author
      }
    }

    /**
     * Convirtiendo ORDEN DE COMPRA (Descontar inventario)
     */
    if (convert === 'order_pay') {
      await insideCreate(req, tenant)
      const data = await db.updateItem(id, model, req, tenant)
      res.status(200).json(data)
    }
    /**
     * Convirtiendo EN FACTURA
     */
    if (convert === 'order_invoice') {
      req = await convertInvoice(id, req, settingData, tenant)
      const data = await db.updateItem(id, model, req, tenant)
      await sendToCalendar(data, tenant)
      res.status(200).json(data)
    }

    if (convert === 'order_generate_purchase') {
      const newReq = await convertPurchase(id, req, settingData, tenant)
      const data = {...req, ...newReq};
      res.status(200).json(data)
    }

    /**
     * Ver factura
     */
    if (convert === 'order_generate_invoice') {
      req = await convertGenerateInvoice(id, req, settingData, tenant)
      const data = await db.updateItem(id, model, req, tenant)
      res.status(200).json(data)
    }

  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Duplicate Purchase
 */

exports.duplicateItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    const id = await utils.isIDGood(req.id, false)
    let originalPurchase = await db.getItem(id, model, tenant)
    originalPurchase = originalPurchase.toObject()
    delete originalPurchase._id;
    delete originalPurchase.convert;
    delete originalPurchase.createdAt;
    delete originalPurchase.updatedAt;
    const data = await db.createItem(originalPurchase, model, tenant)
    res.status(201).json(data)
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.createItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const author = req.user;
    const locale = req.getLocale()
    req = matchedData(req)
    const {customer} = req
    req = {
      ...req, ...{
        author: author._id,
        customer: {
          _id: mongoose.Types.ObjectId(customer._id),
          name: customer.name,
          email: customer.email
        }
      }
    }
    const data = await db.createItem(req, model, tenant)
    await sendToCalendar(data, tenant)
    await notificationService.notiPurchase(data, author, locale)
    res.status(201).json(data)
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Delete item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.deleteItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    const id = await utils.isIDGood(req.id)
    res.status(200).json(await db.deleteItem(id, model, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}
