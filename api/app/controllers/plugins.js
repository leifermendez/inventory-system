const model = require('../models/plugins')
const settings = require('../models/settings')
const {matchedData} = require('express-validator')
const utils = require('../middleware/utils')
const db = require('../middleware/db')
const _ = require('lodash');
const mongoose = require('mongoose')
const PluginsSettings = require('../models/pluginsSettings')

const PluginsFree = ['excelImport']

/*********************
 * Private functions *
 *********************/

/**
 * Gets all items from database
 */
const getAllItemsFromDB = async () => {
  return new Promise((resolve, reject) => {
    model.find(
      {},
      '-updatedAt -createdAt',
      {
        sort: {
          name: 1
        }
      },
      (err, items) => {
        if (err) {
          reject(utils.buildErrObject(422, err.message))
        }
        resolve(items)
      }
    )
  })
}

/**
 *
 */
const findPluginSetting = (id, mode = 'id', tenant = null) => new Promise((resolve, reject) => {
  let query = {};
  if (mode === 'id') {
    query = {'plugin._id': mongoose.Types.ObjectId(id)};
  } else {
    query = {'plugin.path': id};
  }
  PluginsSettings.byTenant(tenant)
    .findOne(query, 'plugin', (err, item) => {
      if (!err) {
        resolve(item)
      } else {
        reject(err)
      }
    })
})

/**
 *
 */
const pluginSetting = async (plugin = null, tenant = null) => new Promise((resolve, reject) => {
  try {
    const options = {upsert: true, new: true, setDefaultsOnInsert: true};
    PluginsSettings.byTenant(tenant).findOneAndUpdate({
      'plugin._id': mongoose.Types.ObjectId(plugin._id)
    }, {
      plugin
    }, options, (err, result) => {
      if (!err) {
        resolve(result)
      }
    });
  } catch (e) {
    return null;
  }
})

/**
 * active plugin
 */

const activePlugins = async (plugins, tenant) => {
  const list  = await db.findPluginsByName([...plugins], model);
  _.forEach(list, async (dataPlugin) => {
    await pluginSetting(dataPlugin, tenant)
  })
  console.log('--->',list)
}

/**
 *
 */
const findPluginWithoutTenant = (id) => new Promise((resolve, reject) => {
  let query = {'path': id};
  model
    .findOne(query, (err, item) => {
      if (!err) {
        resolve(item)
      } else {
        reject(err)
      }
    })
})

/**
 *
 */
const checkIfPluginFree = async (id, tenant) => new Promise(async (resolve, reject) => {
  try {
    // Primero buscamos el plugin si esta en la db por el nombre
    console.log('PLugin a buscar es: ', id)
    let plugin = await findPluginWithoutTenant(id, '')
    if (!plugin) reject(utils.buildErrObject(400, 'Plugin not found'))

    let parentModule = await findPluginSetting(id, 'path', tenant);
    parentModule = parentModule.plugin
    if (!parentModule) {
      reject(
        utils.handleError(res, {
          code: 404,
          message: 'NOT_SETTINGS_PLUGIN'
        })
      )
    }
    console.log('Valor del plugin en settings')
    resolve(parentModule)
  } catch (error) {
    console.log(error)
    return null;
  }
})

/********************
 * Public functions *
 ********************/

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getAllItems = async (req, res) => {
  try {
    res.status(200).json(await getAllItemsFromDB())
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItems = async (req, res) => {
  try {
    const query = await db.checkQueryString(req.query)
    res.status(200).json(await db.getWithOutTenant(req, model, query))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Get item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    const id = await utils.isIDGood(req.id);
    const item = await db.getItemWithOut(id, model);
    const data = {
      item,
      pluginSetting: await findPluginSetting(id, 'id', tenant)
    }
    res.status(200).json(data)
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Update item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.updateItem = async (req, res) => {
  try {
    req = matchedData(req)
    const id = await utils.isIDGood(req.id)
    res.status(200).json(await db.updateItem(id, model, req))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.createItem = async (req, res) => {
  try {
    req = matchedData(req)
    res.status(201).json(await db.createItem(req, model))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Active item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.activeItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const id = req.params.id;
    const checkPlugin = await db.getItemWithOut(id, model);
    res.status(201).json(await pluginSetting(checkPlugin, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}


/**
 * Disabled plugin
 */

exports.disabledItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const id = req.params.id;
    const pluginSetting = await findPluginSetting(id, 'id', tenant)
    res.status(200).json(await db.deleteItem(pluginSetting._id, PluginsSettings, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Delete item function called by route
 * @param {Object} req - request object
 *
 * @param {Object} res - response object
 */
exports.deleteItem = async (req, res) => {
  try {
    req = matchedData(req)
    const id = await utils.isIDGood(req.id)
    res.status(200).json(await db.deleteItem(id, model))
  } catch (error) {
    utils.handleError(res, error)
  }
}

exports.activePlugins = (plugin, tenant) => activePlugins(plugin, tenant)

/**
 * Get and  Post events for each action plugin
 *
 */


exports.actionForPlugin = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const dataUser = req.user;
    req = matchedData(req);
    const {id, action, value} = req;
    let parentModule = await checkIfPluginFree(id, tenant)
    const singleModule = require(`../plugins/${parentModule.path}`);
    /** Action is a Method name for use **/
    res.status(201).json(await singleModule[action]({}, parentModule, value, tenant, dataUser))
  } catch (error) {
    console.log(error)
    error = {
      code: 422,
      message: 'ERROR_INTERNAL',
      error
    }
    utils.handleError(res, error)
  }
}


/**
 * Exports Free Controllers
 **/

exports.actionForPublic = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const dataUser = req.user;
    req = matchedData(req);
    const {id, action, value} = req;
    let parentModule = await findPluginSetting(id, 'path', tenant);
    parentModule = parentModule.plugin
    if (!parentModule) {
      utils.handleError(res, {
        code: 404,
        message: 'NOT_SETTINGS_PLUGIN'
      })
    } else {
      const singleModule = require(`../plugins/${parentModule.path}`);

      /** Action is a Method name for use **/
      res.status(201).json(await singleModule[action]({}, parentModule, value, tenant, dataUser))
    }
  } catch (error) {
    console.log(error)
    error = {
      code: 422,
      message: 'ERROR_INTERNAL',
      error
    }
    utils.handleError(res, error)
  }
}




