const pdf = require('html-pdf');
const mongoose = require('mongoose');
const _ = require('lodash');
const {nanoid} = require('nanoid')
const moment = require('moment');
const settings = require('../models/settings');
const purchase = require('../models/purchase');

const findPurchase = (id = null) => new Promise((resolve, reject) => {
  purchase.aggregate([{
    $match: {"_id": mongoose.Types.ObjectId(id)},
  },
    {
      $lookup: {
        from: 'users',
        let: {idUser: "$customer._id"},
        pipeline: [
          {
            $match:
              {
                $expr:
                  {
                    $and:
                      [
                        {$eq: ["$$idUser", "$_id"]}
                      ]
                  }
              }
          }
        ],
        as: 'customer'
      }
    },
    {
      $lookup: {
        from: 'users',
        let: {idUser: "$author"},
        pipeline: [
          {
            $match:
              {
                $expr:
                  {
                    $and:
                      [
                        {$eq: ["$$idUser", "$_id"]}
                      ]
                  }
              }
          }
        ],
        as: 'author'
      }
    },
    {$unwind: '$customer'},
    {$unwind: '$author'},
    {
      "$project": {
        "_id": 1,
        "customer": 1,
        "items": 1,
        "author": 1,
        "status": 1,
        "deliveryAddress": 1,
        "deliveryType": 1,
        "total": 1,
        "subTotal": 1,
        "sumTaxesTotal": 1,
        "taxes": 1,
        "tag": 1,
        "controlNumber": 1,
        "invoiceNumber": 1,
        "description": 1,
        "createdAt": 1,
        "updatedAt": 1,
        "tenantId:": 1
      }
    },
  ], (err, item) => {
    if (!err) {
      resolve(item.find(() => true))
    } else {
      reject(err)
    }
  })
});

const renderTable = (items = [], currency = '', inData = {}, template = '') => {

  items = (typeof items !== 'object') ? [] : items;
  inData.total = parseFloat(inData.total).toFixed(2);
  inData.totalWithOffset = parseFloat(inData.totalWithOffset).toFixed(2);
  inData.sumTaxesTotal = parseFloat(inData.sumTaxesTotal).toFixed(2);
  inData.subTotal = parseFloat(inData.subTotal).toFixed(2);

  let findTemplate = null;
  let findTax = null;
  let findSubTotal = null;
  let findTotal = null;
  let findTaxes = null;
  let findTaxOffset = null;

  if (template.includes('<!-- ** TaxesStart **-->') && template.includes('<!-- ** TaxesEnd **-->')) {
    findTaxes = template.substring(
      template.lastIndexOf("<!-- ** TaxesStart **-->") + 24,
      template.lastIndexOf("<!-- ** TaxesEnd **-->"));
  }

  if (template.includes('<!-- ** ItemsStart **-->') && template.includes('<!-- ** ItemsEnd **-->')) {
    findTemplate = template.substring(
      template.lastIndexOf("<!-- ** ItemsStart **-->") + 24,
      template.lastIndexOf("<!-- ** ItemsEnd **-->"));
  }

  if (template.includes('<!-- ** InitTax **-->') && template.includes('<!-- ** EndTax **-->')) {
    findTax = template.substring(
      template.lastIndexOf("<!-- ** InitTax **-->") + 21,
      template.lastIndexOf("<!-- ** EndTax **-->"));
  }

  if (template.includes('<!-- ** InitTaxOffset **-->') && template.includes('<!-- ** EndTaxOffset **-->')) {
    findTaxOffset = template.substring(
      template.lastIndexOf("<!-- ** InitTaxOffset **-->") + 27,
      template.lastIndexOf("<!-- ** EndTaxOffset **-->"));
  }

  if (template.includes('<!-- ** InitSubTotal **-->') && template.includes('<!-- ** EndSubTotal **-->')) {
    findSubTotal = template.substring(
      template.lastIndexOf("<!-- ** InitSubTotal **-->") + 26,
      template.lastIndexOf("<!-- ** EndSubTotal **-->"));
  }

  if (template.includes('<!-- ** InitTotal **-->') && template.includes('<!-- ** EndTotal **-->')) {
    findTotal = template.substring(
      template.lastIndexOf("<!-- ** InitTotal **-->") + 23,
      template.lastIndexOf("<!-- ** EndTotal **-->"));
  }

  let itemDynamic = items.map(i => {
    const price = i.prices.find(() => true).amount;

    return `<tr style="padding: 10px 0px;" class="items-table _${price}">
            <td style="padding: 10px 0px;" class="item-single _${price}">(${i.sku.toUpperCase()}) ${i.name}</td>
            <td style="padding: 10px 0px;" class="item-single _${price}">${price.toLocaleString('en')} ${currency}</td>
            <td style="padding: 10px 0px;" class="item-single _${price}">${i.qty}</td>
            <td style="padding: 10px 0px;" class="item-single _${price}">${parseFloat(i.qty * price).toLocaleString('en')} ${currency}</td>
          </tr>`
  });
  itemDynamic = itemDynamic.join('')
  let taxesList = inData.taxes.map(i => {
    return ` <div class="wrapper-taxes-div">
                <span>${i.tax_name}</span>
                <span>${i.tax_total} ${currency}</span>
                <span>${i.tax_percentage} ${currency}</span>
            </div>`
  });
  taxesList = taxesList.join('')


  const regExpItemTemplate = new RegExp(findTemplate);
  const regExpFindSubTotal = new RegExp(findSubTotal);
  const regExpTax = new RegExp(findTax);
  const regExpTaxOffset = new RegExp(findTaxOffset);
  const regExpFindTotal = new RegExp(findTotal);
  const regExpTaxesList = new RegExp(findTaxes);

  /**
   * ZONA MATH
   * @type {string}
   */

  let result = template.replace(regExpItemTemplate, itemDynamic);
  result = result.replace(regExpTaxesList, taxesList);
  // console.log(embedAmount(regExpFindSubTotal)); ///<span class="secure-amount">OOO EUR<\/span>/
  result = result.replace(regExpFindSubTotal, `${(inData.subTotal) ? inData.subTotal.toLocaleString('en') : 0} ${currency}`);
  result = result.replace(regExpTax, `${(inData.sumTaxesTotal) ? inData.sumTaxesTotal.toLocaleString('en') : 0} ${currency}`);
  result = result.replace(regExpTaxOffset, `${(inData.totalWithOffset) ? inData.totalWithOffset.toLocaleString('en') : 0} ${currency}`);
  result = result.replace(regExpFindTotal, `${(inData.total) ? inData.total.toLocaleString('en') : 0} ${currency}`);
  return result
}
const replaceData = (rawText = '', inData = {}, settings = {}) => new Promise((resolve, reject) => {
  const {customer, controlNumber, author, items, description, invoiceNumber, createdAt} = inData;
  const {currency, logo} = settings;
  /**
   * Customer variables
   */
  rawText = rawText.replace(/__CUSTOMER_NAME__/gi, customer.name ? customer.name : '');
  rawText = rawText.replace(/__CUSTOMER_LAST_NAME__/gi, customer.lastName ? customer.lastName : '');
  rawText = rawText.replace(/__CUSTOMER_ID__/gi, customer.nie ? customer.nie : '');
  rawText = rawText.replace(/__CUSTOMER_EMAIL__/gi, customer.email ? customer.email : '');
  rawText = rawText.replace(/__CUSTOMER_PHONE__/gi, customer.phone ? customer.phone : '');
  rawText = rawText.replace(/__CUSTOMER_ADDRESS__/gi, customer.address ? customer.address : '');
  rawText = rawText.replace(/__CUSTOMER_BUSINESS_NAME__/gi, customer.nameBusiness ? customer.nameBusiness : '');

  /**
   * Invoice
   *
   */
  rawText = rawText.replace(/__INVOICE_NUMBER__/gi, invoiceNumber ? invoiceNumber : '');
  /**
   * Purchase variable
   */
  rawText = rawText.replace(/__PURCHASE_ORDER__/gi, controlNumber ? controlNumber : '');
  rawText = rawText.replace(/__PURCHASE_OBSERVATION__/gi, description ? description : '');
  rawText = rawText.replace(/__PURCHASE_DATE__/gi,
    createdAt ? moment(createdAt.toString()).format('DD-MM-YYYY') : moment().format('DD-MM-YYYY'));

  /**
   * Settings variable
   */
  rawText = rawText.replace(/__LOGO__/gi, logo ? logo : '');
  /**
   * Seller variable
   */
  rawText = rawText.replace(/__SELLER_NAME__/gi, author.name ? author.name : '');
  rawText = rawText.replace(/__SELLER_ID__/gi, author.nie ? author.nie : '');
  rawText = rawText.replace(/__SELLER_EMAIL__/gi, author.email ? author.email : '');
  rawText = rawText.replace(/__SELLER_PHONE__/gi, author.phone ? author.phone : '');
  rawText = rawText.replace(/__SELLER_ADDRESS__/gi, author.address ? author.address : '');

  /**
   * Items variable
   */
  rawText = renderTable(items, currency, inData, rawText, settings)
  resolve(rawText)
});

const getSetting = (tenantId) => new Promise((resolve, reject) => {
  settings.findOne({tenantId}, (err, item) => {
    if (!err) {
      resolve(item)
    } else {
      reject(err)
    }
  })
})

/**
 * Service generate INVOICE PDF
 */

exports.generateInvoice = (idPurchase = null, invoiceNumber = null, tenant) => new Promise(async (resolve, reject) => {
  let templatePurchase = '';
  const purchaseData = (idPurchase) ? await findPurchase(idPurchase) : null;
  const name = `invoice-${invoiceNumber}-${tenant}.pdf`;
  if (process.env.NODE_ENV === 'production') {
    templatePurchase = `<style>html{zoom: 0.753 !important;}</style>`
  }
  const settingTenant = await getSetting(tenant);

  /**
   * If has IRPF
   */
  purchaseData.totalWithOffset = 0;
  const taxOff = _.head(settingTenant.taxOffset)
  if (taxOff) {
    purchaseData.totalWithOffset = (purchaseData.subTotal * (taxOff.amount / 100))
    purchaseData.total -= purchaseData.totalWithOffset
  }
  purchaseData.invoiceNumber = invoiceNumber;

  const {invoiceDesign = {}} = settingTenant;
  templatePurchase += `<style>${invoiceDesign.css}</style>`;
  templatePurchase += await replaceData(invoiceDesign.html, purchaseData, settingTenant);
  pdf.create(templatePurchase).toFile(`./public/${name}`,
    (err, res) => {
      if (err) {
        reject(err)
      } else {
        const output = {
          download: `${process.env.APP_URL}/${name}`
        }
        resolve(output)
      }
    });
})

/**
 * Generate PURCHASE
 */

exports.generatePurchase = (idPurchase = null, tenant) => new Promise(async (resolve, reject) => {
  let templatePurchase = '';
  const purchaseData = (idPurchase) ? await findPurchase(idPurchase) : null;
  const name = `purchase-${nanoid(5)}-${tenant}.pdf`;
  if (process.env.NODE_ENV === 'production') {
    templatePurchase = `<style>html{zoom: 0.753 !important;}</style>`
  }
  const settingTenant = await getSetting(tenant);

  purchaseData.totalWithOffset = 0;
  const taxOff = _.head(settingTenant.taxOffset)
  if (taxOff) {
    purchaseData.totalWithOffset = (purchaseData.subTotal * (taxOff.amount / 100))
    purchaseData.total -= purchaseData.totalWithOffset
  }
  ;

  const {purchaseDesign = {}} = settingTenant;
  templatePurchase += `<style>${purchaseDesign.css}</style>`;
  templatePurchase += await replaceData(purchaseDesign.html, purchaseData, settingTenant);
  pdf.create(templatePurchase).toFile(`./public/${name}`,
    (err, res) => {
      if (err) {
        reject(err)
      } else {
        const output = {
          download: `${process.env.APP_URL}/${name}`
        }
        resolve(output)
      }
    });
})

