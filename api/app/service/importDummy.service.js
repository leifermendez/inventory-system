const Product = require('../models/products')
const Providers = require('../models/providers')
const Deposits = require('../models/deposits')
const Inventory = require('../models/inventory')
const Purchase = require('../models/purchase')
const Notification = require('../models/notifications')
const User = require('../models/user')
const mongoose = require('mongoose');
// const moment = require('moment');
const faker = require('faker');
const _ = require('lodash');

/**
 * Provider
 **/

exports.generateProvider = async (owner = {}, user = {}) => {
  const providers = {
    "name": "MEGA-FACTORY",
    "address": "77408 Okuneva Springs Ebertbury, SC 54223",
    "manager": user,
    "phone": "88888888 ",
    "email": `info-${user.email}@factory.test`,
    "description": "Nulla quis lorem ut libero malesuada feugiat.",
    "tenantId": owner.tenantId,
    "dummy": true,
    "tag": ["dummy"]
  }
  return Providers.findOneAndUpdate({dummy: true}, providers,
    {upsert: true, new: true, setDefaultsOnInsert: true});
}

/**
 * Product
 **/

exports.generateProduct = async (owner = {}, user = {}) => {

  const imageFake =  faker.image.imageUrl();
  const product = {
    "gallery": [
      {
        "original": imageFake,
        "small":  imageFake,
        "medium": imageFake,
        "large": imageFake,
        "author": owner,
        "createdAt": "2020-07-07T21:58:56.560Z",
        "updatedAt": "2020-07-07T21:58:56.560Z",
        "deleted": false,
        "deletedAt": null,
        "tenantId": owner.tenantId
      }
    ],
    "name": faker.commerce.productName(),
    "prices": [
      {
        "amount": faker.commerce.price()
      },
      {
        "amount": faker.commerce.price()
      }
    ],
    "categories": ["DUMMY"],
    "tag": ["CUP", "dummy"],
    "sku": faker.random.alphaNumeric(),
    "description": faker.commerce.productDescription(),
    "author": owner,
    "tenantId": owner.tenantId,
    "dummy": true
  }
  return Product.findOneAndUpdate({dummy: true, sku:product.sku}, product,
    {upsert: true, new: true, setDefaultsOnInsert: true});
}

/**
 * Deposits
 **/

exports.generateDeposits = async (owner = {}, user = {}) => {
  const deposit = {
    "name": "MAIN-DEPOSIT",
    "manager": owner,
    "phone": "8888888",
    "address": "77408 Okuneva Springs Ebertbury, SC 54223",
    "tenantId": owner.tenantId,
    "dummy": true,
    "tag": ["dummy"],
  }
  return Deposits.findOneAndUpdate({dummy: true}, deposit,
    {upsert: true, new: true, setDefaultsOnInsert: true});
}


/**
 * Users
 **/
exports.generateUser = async (owner = {}) => {
  const user = {
    "role": "customer",
    "tag": ["dummy"],
    "name": faker.name.firstName(),
    "lastName": faker.name.lastName(),
    "nie": "123456789",
    "email": faker.internet.email(),
    "password": "$2b$05$CjmqOr82psh0Jmd0svdR8ebS.NpBWITa.o7TJ3j2YUqtE8cM7YwOq",
    "phone":faker.phone.phoneNumber(),
    "nameBusiness": "",
    "address": "77408 Okuneva Springs Ebertbury, SC 54223",
    "verification": "0454f49cd-6307-4750-bb33-52283119ce63",
    "tenantId": owner.tenantId,
    "dummy": true
  }

  return User.findOneAndUpdate({dummy: true, email:user.email}, user,
    {upsert: true, new: true, setDefaultsOnInsert: true});
}


/**
 * Iventory
 **/
exports.generateInventory = async (owner = {}, product = {}, deposits = {}) => {
  const inventory = {
    "product": product,
    "provider": null,
    "qty": 10,
    "deposit": deposits,
    "author": owner,
    "tenantId": owner.tenantId,
    "dummy": true,
    "tag": ["dummy"],
  }

  return Inventory.findOneAndUpdate({dummy: true,'product.name':product.name}, inventory,
    {upsert: true, new: true, setDefaultsOnInsert: true});
}

/**
 * Purchase
 **/
exports.generatePurchase = async (owner = {}, customer = {}) => {
  const purchase = {
    "status": "hold",
    "deliveryType": "in_house",
    "convert": "order_purchase",
    "subTotal": 0,
    "sumTaxesTotal": 0,
    "taxes": [],
    "total": 0,
    "attachment": [],
    "deleted": false,
    "customer": customer,
    "items": [],
    "deliveryAddress": "77408 Okuneva Springs Ebertbury, SC 54223",
    "invoiceAddress": "77408 Okuneva Springs Ebertbury, SC 54223",
    "description": "",
    "author": mongoose.Types.ObjectId(owner.id),
    "tenantId": owner.tenantId,
    "dummy": true,
    "tag": ["dummy"],
  }

  return Purchase.findOneAndUpdate({dummy: true, 'customer.email':customer.email}, purchase,
    {upsert: true, new: true, setDefaultsOnInsert: true});
}


/**
 * Notifications
 **/
exports.generateNotification = async (owner = {}) => {
  const notification = {
    "receptor": mongoose.Types.ObjectId(owner._id),
    "sender": mongoose.Types.ObjectId(owner._id),
    "seen": false,
    "behavior": "modal",
    "tenantId": owner.tenantId,
    "dummy": true,
    "title": "Datos de pruebas importados",
    "body": "El sistema importó la data de pruebas correctamente, comienza a probar la interfaz",
    "event": "<div class='w-100 text-center'><img src='https://media1.giphy.com/media/3oEjHBUwuSGbFS5iAE/giphy.gif?cid=ecf05e47ku3d2ydm01wyac0g5082rsbkbu9l094ba9rgvj6b&rid=giphy.gif'/></div>"
  }
  return Notification.findOneAndUpdate({dummy: true}, notification,
    {upsert: true, new: true, setDefaultsOnInsert: true});

}
