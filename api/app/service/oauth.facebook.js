const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy;
const {matchedData} = require('express-validator')
const user = require('../models/user');

/**
 * Configuracion FACEBOOK ==> https://i.imgur.com/rSCWFoC.png
 * @param req
 * @returns {Authenticator}
 */
module.exports = (req = {}) => {
  const {cookies} = req
  return passport.use(new FacebookStrategy({
      clientID: "290664481957869",
      clientSecret: "cea8911bea5169134b2e8eff50c11781",
      callbackURL: (process.env.NODE_ENV !== 'production') ?
        `https://app.kitagil.test/callback/facebook` : `https://app.kitagil.com/callback/facebook`,
      profileFields: ['id', 'email', 'name', 'picture.type(large)'],
    }, (accessToken, refreshToken, profile, done) => {
      profile._json.picture = (profile && profile._json.picture) ? profile._json.picture.data.url : ''
      profile.tenant = cookies.tenant
      done(null, profile);
    }
  ));
}
