const config = {
  pk: process.env.STRIPE_PK,
  sk: process.env.STRIPE_SK,
  id: process.env.STRIPE_ID
}
const stripe = require('stripe')(config.sk);

exports.createCustomer = async (data) => {
  return new Promise((resolve, reject) => {
    stripe.customers.create({
      source: data.token,
      email: data.email,
    }).then(
      (response) => {
        resolve(response);
      },
      (err) => {
        reject(err)
      }
    );
  })
}


exports.getHookStripe = (inData) => new Promise((resolve, reject) => {
  try {
    const {data, type} = inData;
    const customerId = data.object.customer || null
    const invoicePdf = data.object.invoice_pdf || null
    const billingReason = data.object.billing_reason || null
    // console.log(billingReason)
    /**
     * Check Type
     */
    switch (type) {
      case 'invoice.payment_succeeded':
        if (billingReason.includes('subscription')) {
          resolve(
            {
              customer: customerId,
              invoicePdf,
              actions: 'subscription_update'
            }
          )
        } else {
          resolve(null)
        }
        break;
      default:
        resolve(null)
        break
    }

  } catch (e) {
    resolve(null)
  }
})


/**
 * Per order
 */
exports.getHookStripePer = (inData) => new Promise((resolve, reject) => {
  try {
    const {data, type} = inData;
    const customerId = data.object.customer || null
    const metadata = data.object.metadata || {}
    const invoicePdf = data.object.invoice_pdf || null
    // console.log(billingReason)
    /**
     * Check Type
     */
    switch (type) {
      case 'payment_intent.succeeded':
        resolve(
          {
            customer: customerId,
            idOrder: metadata.internal_id || null,
            status: 'success'
          }
        )
        break;
      default:
        resolve(null)
        break
    }

  } catch (e) {
    resolve(null)
  }
})

exports.checkSignatureHook = async (body, sig, endpointSecret) => {
  return stripe.webhooks.constructEvent(body, sig, endpointSecret);
}
