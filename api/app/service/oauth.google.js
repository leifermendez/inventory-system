const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

module.exports = (req = {}) => {
  const {cookies} = req
  return passport.use(new GoogleStrategy({
      clientID: "931889758382-g2c2468ckm7snf235mq6lrtmb2tr7c15.apps.googleusercontent.com",
      clientSecret: "QZ_NFCe3rlLV2XZEPa-noLvx",
      callbackURL: "https://app.kitagil.com/callback/google"
    }, (accessToken, refreshToken, profile, done) => {
      profile.tenant = cookies.tenant
      done(null, profile);
    }
  ));
}
