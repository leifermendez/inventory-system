const pdf = require('html-pdf');
const mongoose = require('mongoose');
const moment = require('moment');
const settings = require('../../models/settings');
const pluginsSettings = require('../../models/pluginsSettings');
const purchase = require('../../models/purchase');
const generateRandomString = function () {
  return Math.random().toString(20).substr(2, 12)
}


/**
 * Private functions
 */

const renderTableList = (items = [], currency = '', source = null) => {
  switch (source) {
    case 'products':
      return items.map(i => {
        return `<tr>
            <th style="text-align: left">
            <div style="margin-bottom: 5px">
               <div>
              <b>Product: </b>${i.name}
              </div>
              <div><b>Qty:</b> ${i.qty}</div>
              <div><b>Prices:</b> ${i.prices.map(a => `(${a.amount} ${currency})`).join('  ')}</div>
            </div>
            </th>
          </tr>`;
      }).join('');
    case 'purchase':
      return items.map(i => {
        return `<tr>
            <th style="text-align: left">
            <div style="margin-bottom: 15px">
               <div>
              <b>Customer: </b>${i.customer.name} (${i.customer.email})
              </div>
                 <div>
              <b>Seller: </b>${i.author.name} (${i.author.email})
              </div>
                <div>
              <b>Total: </b>${i.total} ${currency}
              </div>
            </div>
            </th>
          </tr>`;
      }).join('');

    case 'providers':
      return items.map(i => {
        return `<tr>
            <th style="text-align: left">
            <div style="margin-bottom: 15px">
               <div>
              <b>Name: </b>${i.name}
              </div>
                 <div>
              <b>Contact: </b>${i.manager.name} (${i.manager.email}) ${i.manager.phone}
              </div>
            </div>
            </th>
          </tr>`;
      }).join('');

    case 'inventory':
      return items.map(i => {
        return `<tr>
            <th style="text-align: left">
            <div style="margin-bottom: 15px">
               <div>
              <b>Name: </b>${i.product.name}
              </div>
                 <div>
              <b>Qty: </b>${i.qty}
              </div>
                   <div>
              <b>Contact: </b>${i.author.name} (${i.author.email})
              </div>
            </div>
            </th>
          </tr>`;
      }).join('');
  }
};

const templateListRaw = (value = {}, settings = {}) => {
  return `<style>
  .template-html-report .clearfix:after {
    content: "";
    display: table;
    clear: both;
  }

  .template-html-report a {
    color: #5D6975;
    text-decoration: underline;
  }

  .template-html-report body {
    position: relative;
    color: #001028;
    background: #FFFFFF;
    font-family: Arial, sans-serif;
    font-size: 10px;
    font-family: Arial;
  }

  .template-html-report header {
    padding: 10px 0;
    margin-bottom: 30px;
  }

  .template-html-report .logo {
    text-align: center;
    margin-bottom: 10px;
  }

  .template-html-report .logo img {
    width: 90px;
  }

  .template-html-report h1 {
    border-top: 0px solid #5D6975 !important;
    border-bottom: 1px solid #eaeaea;
    color: #5D6975;
    font-size: 2.4em;
    line-height: 1.4em;
    font-weight: normal;
    text-align: center;
    margin: 0 0 20px 0;
  }

  .template-html-report .project {
    float: left;
  }

  .template-html-report .project span,
  .template-html-report .company span {
    color: #5D6975;
    text-align: right;
    width: 100px !important;
    margin-right: 10px;
    display: inline-block;
    font-size: 0.8em;
  }

  .template-html-report .company {
    float: right;
    text-align: left !important;
  }

  .template-html-report .project div,
  .template-html-report .company div {
    white-space: nowrap;
  }

  .template-html-report table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px;
  }

  .template-html-report table tr:nth-child(2n-1) td {
    background: #F5F5F5;
  }

  .template-html-report table th,
  .template-html-report table td {
    text-align: center;
    font-size: 12px;
  }

  .template-html-report table th {
    padding: 5px 20px;
    color: #5D6975;
    border-bottom: 1px solid #C1CED9;
    white-space: nowrap;
    font-weight: normal;
  }

  .template-html-report table .service,
  .template-html-report table .desc {
    text-align: left;
  }

  .template-html-report table td {
    padding: 20px;
    text-align: right;
  }

  .template-html-report table td.service,
  .template-html-report table td.desc {
    vertical-align: top;
  }

  .template-html-report table td.unit,
  .template-html-report table td.qty,
  .template-html-report table td.total {

  }

  .template-html-report table td.grand {
    border-top: 1px solid #eaeaea;
    ;
  }

  .template-html-report .notices .notice {
    color: #5D6975;
    font-size: 1.2em;
    margin-top: 15px !important;
    margin-bottom: 15px !important;
  }

  .template-html-report footer {
    color: #5D6975;
    width: 100%;
    height: 30px;
    bottom: 0;
    border-top: 0px solid #eaeaea;
    padding: 15px 0;
    text-align: center;
  }
</style>

<head>
  <meta charset="utf-8">
</head>

<html class="template-html-report">
  <body>
    <header class="clearfix">
      <div class="logo">
        <img src="${settings.logo ? settings.logo : ''}">
      </div>
    </header>
    <main>
     <table style="width:100% !important;">
     <thead>
     <tbody>
       ${renderTableList(value.data, settings.currency, value.source)}
     </tbody></table>
    </main>
    <footer>
    </footer>
  </body>
</html>`
};

const getSetting = (tenantId) => new Promise((resolve, reject) => {
  settings.findOne({ tenantId }, (err, item) => {
    if (!err) {
      resolve(item)
    } else {
      reject(err)
    }
  })
})

const findPurchase = (id = null) => new Promise((resolve, reject) => {
  purchase.aggregate([{
    $match: {"_id": mongoose.Types.ObjectId(id)},
  },
    {
      $lookup: {
        from: 'users',
        let: {idUser: "$customer._id"},
        pipeline: [
          {
            $match:
              {
                $expr:
                  {
                    $and:
                      [
                        {$eq: ["$$idUser", "$_id"]}
                      ]
                  }
              }
          }
        ],
        as: 'customer'
      }
    },
    {
      $lookup: {
        from: 'users',
        let: {idUser: "$author"},
        pipeline: [
          {
            $match:
              {
                $expr:
                  {
                    $and:
                      [
                        {$eq: ["$$idUser", "$_id"]}
                      ]
                  }
              }
          }
        ],
        as: 'author'
      }
    },
    {$unwind: '$customer'},
    {$unwind: '$author'},
    {
      "$project": {
        "_id": 1,
        "customer": 1,
        "items": 1,
        "author": 1,
        "status": 1,
        "deliveryAddress": 1,
        "deliveryType": 1,
        "total": 1,
        "tag": 1,
        "controlNumber": 1,
        "description": 1,
        "createdAt": 1,
        "updatedAt": 1,
        "tenantId:": 1
      }
    },
  ], (err, item) => {
    if (!err) {
      resolve(item.find(() => true))
    } else {
      reject(err)
    }
  })
});

const renderTable = (items = [], currency = '', total = 0) => {
  items = (typeof items !== 'object') ? [] : items;
  let table = items.map(i => {
    const price = i.prices.find(() => true).amount;
    return `<tr>
            <td class="service">(${i.sku.toUpperCase()}) ${i.name}</td>
            <td class="unit">${price.toLocaleString('en')} ${currency}</td>
            <td class="qty">${i.qty}</td>
            <td class="total">${parseFloat(i.qty * price).toLocaleString('en')} ${currency}</td>
          </tr>`
  });
  table = table.join('')
  const tHead = `<thead>
          <tr>
            <th class="service">SERVICE</th>
            <th width="70">PRICE</th>
            <th>QTY</th>
            <th width="70">TOTAL</th>
          </tr>
        </thead>`;

  const tFoot = `<tr>
            <td colspan="3" class="grand total">TOTAL</td>
            <td class="grand total">${(total) ? total.toLocaleString('en') : 0} ${currency}</td>
          </tr>`;

  return `<table>${tHead}<tbody>${table}${tFoot}</tbody></table>`
}

const replaceData = (rawText = '', inData = {}, settings = {}) => new Promise((resolve, reject) => {
  const {customer, controlNumber, author, items, total, description} = inData;
  const {currency, logo} = settings;
  /**
   * Customer variables
   */
  rawText = rawText.replace(/__CUSTOMER_NAME__/gi, customer.name);
  rawText = rawText.replace(/__CUSTOMER_LAST_NAME__/gi, customer.lastName);
  rawText = rawText.replace(/__CUSTOMER_ID__/gi, customer.nie);
  rawText = rawText.replace(/__CUSTOMER_EMAIL__/gi, customer.email);
  rawText = rawText.replace(/__CUSTOMER_PHONE__/gi, customer.phone);
  rawText = rawText.replace(/__CUSTOMER_ADDRESS__/gi, customer.address);

  /**
   * Purchase variable
   */
  rawText = rawText.replace(/__PURCHASE_ORDER__/gi, controlNumber);
  rawText = rawText.replace(/__PURCHASE_OBSERVATION__/gi, description);
  rawText = rawText.replace(/__PURCHASE_DATE__/gi, moment().format('DD-MM-YYYY'));

  /**
   * Settings variable
   */
  rawText = rawText.replace(/__LOGO__/gi, logo);
  /**
   * Seller variable
   */
  rawText = rawText.replace(/__SELLER_NAME__/gi, author.name);
  rawText = rawText.replace(/__SELLER_ID__/gi, author.nie);
  rawText = rawText.replace(/__SELLER_EMAIL__/gi, author.email);
  rawText = rawText.replace(/__SELLER_PHONE__/gi, author.phone);
  rawText = rawText.replace(/__SELLER_ADDRESS__/gi, author.address);

  /**
   * Items variable
   */
  // console.log(renderTable(items))
  rawText = rawText.replace(/__LIST_ITEMS__/gi, renderTable(items, currency, total));
  resolve(rawText)
});


const generatePurchase = (idPurchase = null, tenant) => new Promise(async (resolve, reject) => {
  let templatePurchase = ''
  const purchaseData = (idPurchase) ? await findPurchase(idPurchase) : null;
  const name = `purchases_${generateRandomString()}.pdf`;
  const templates = await settings.byTenant(tenant).findOne({}, 'invoiceDesign purchaseDesign logo name')
  const {purchaseDesign} = templates;
  // const { pdfPurchase } = template.features;
  const settingTenant = await getSetting(tenant)
  if (process.env.NODE_ENV === 'production') {
    templatePurchase = `<style>html{zoom: 0.753 !important;}</style>`
  }
  templatePurchase += `<style>${purchaseDesign.css}</style>`;
  templatePurchase += await replaceData(purchaseDesign.html, purchaseData, settingTenant);
  pdf.create(templatePurchase).toFile(`./public/${name}`,
    (err, res) => {
      if (err) {
        reject(err)
      } else {
        const output = {
          download: `${process.env.APP_URL}/${name}`
        }
        resolve(output)
      }
    });
})


const generateList = (value = {}, tenant = null) => new Promise(async (resolve, reject) => {
  const settings = await getSetting(tenant);
  const name = `list_${value.source}_${generateRandomString()}.pdf`;
  pdf.create(templateListRaw(value, settings)).toFile(`./public/${name}`,
    (err, res) => {
      if (err) {
        reject(err)
      } else {
        const output = {
          download: `${process.env.APP_URL}/${name}`
        }
        resolve(output)
      }
    });
})

const updatePurchaseTemplate = (data = {}, tenant) => new Promise((resolve, reject) => {
  pluginsSettings.findOneAndUpdate({"plugin.path": "pdfReport", "tenantId": tenant},
    {
      "$set": {"plugin.features.pdfPurchase": data}
    },
    false,
    (err, item) => {
      if (!err) {
        resolve(item)
      } else {
        reject(err)
      }
    })
});

/**
 * Public functions
 */

exports.purchase = ({}, parentModule, value, tenant, dataUser = null) => generatePurchase(value, tenant);

exports.list = ({}, parentModule, value, tenant, dataUser = null) => generateList(value, tenant);

exports.update = ({}, parentModule, value, tenant, dataUser = null) => updatePurchaseTemplate(value, tenant);
