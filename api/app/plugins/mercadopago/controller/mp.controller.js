class PaymentController {
  constructor(paymentService) {
    this.paymentService = paymentService;
  }

  async getMercadoPagoLink(purchaseData ,tenant, urls) {
    try {
      const checkout = await this.paymentService.createPaymentMercadoPago(purchaseData ,tenant, urls);

      return checkout.init_point

    } catch (err) {

      return {
        error: true,
        msg: "Hubo un error con Mercado Pago, asegurate de tener la divisa correcta"
      }
    }
  }

  webhook(req, res) {
    if (req.method === "POST") {
      let body = "";
      req.on("data", chunk => {
        body += chunk.toString();
      });
      req.on("end", () => {
        console.log(body, "webhook response");
        res.end("ok");
      });
    }
    return res.status(200);
  }
}

module.exports = PaymentController;
