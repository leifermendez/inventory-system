const axios = require("axios");

class PaymentService {
  constructor() {
    this.tokensMercadoPago = {
      prod: {},
      test: {
        access_token:
          "APP_USR-6317427424180639-042414-47e969706991d3a442922b0702a0da44-469485398"
      }
    };

    this.mercadoPagoUrl = "https://api.mercadopago.com/checkout";
  }

  async createPaymentMercadoPago(item = {}, tenantId, urls) {
    const url = `${this.mercadoPagoUrl}/preferences?access_token=${this.tokensMercadoPago.test.access_token}`;
    const items = [
      {
        id: `Kitagil-${tenantId}`,
        title: `item-kitagil-${tenantId}`,
        description: item.description,
        quantity: 1,
        currency_id: item.currency,
        unit_price: parseFloat(item.price)
      }
    ];

    const preferences = {
      items,
      external_reference: `Ref-kitagil-${tenantId}`,
      payer: {
        name: "Lalo",
        surname: "Landa",
        email: "test_user_63274575@testuser.com",
        // si estan en sandbox, aca tienen que poner el email de SU usuario de prueba
        phone: {
          area_code: "11",
          number: "22223333"
        },
        address: {
          zip_code: "1111",
          street_name: "False",
          street_number: "123"
        }
      },
      payment_methods: {
        excluded_payment_methods: [
          {
            id: "amex"
          }
        ],
        excluded_payment_types: [{id: "atm"}],
        installments: 6,
        default_installments: 6
      },
      back_urls: {
        success: urls.success,
        pending: urls.pending,
        failure: urls.failure
      },
      notification_url: "https://mercadopago-checkout.herokuapp.com/webhook",
      auto_return: "approved"
    };

    try {
      const request = await axios.post(url, preferences, {
        headers: {
          "Content-Type": "application/json"
        }
      });

      return request.data;
    } catch (e) {
      console.log(e);
    }
  }
}

//NOTA: TODAS las URLS que usemos tienen que ser reales,
//si prueban con localhost, va a fallar

module.exports = PaymentService;
