const utils = require('../../middleware/utils')
const db = require('../../middleware/db');
const settings = require('../../models/settings');
const pluginsSettings = require('../../models/pluginsSettings');
const Orders = require('../../models/orders')
const axios = require('axios').default;
const urlApi = {
  'live': 'https://api.paypal.com',
  'sandbox': 'https://api.sandbox.paypal.com'
}

const brandName = ` (🚀 Kitagil.com) `;
const pathName = 'paypal';
/*********************
 * Private functions *
 *********************/
/**
 * Item already exists
 * @param {string} url - string endpoint
 * @param {string} method - GET | POST | PATCH | DELETE
 * @param {Object} body - Object
 * @param {Object} headers - Object headers
 */
const httpRequest = async (url, method, body, headers) => {
  return new Promise(async (resolve, reject) => {
    await axios({
      method: method,
      url: url,
      data: body,
      headers: headers,
    })
      .then((response) => {
        resolve(response.data)
      })
      .catch(err => {
        err.message = err.response.data
        reject(err.message)
      })
  })
}

const getSetting = (tenantId) => new Promise((resolve, reject) => {
  settings.findOne({tenantId}, (err, item) => {
    if (!err) {
      resolve(item)
    } else {
      reject(err)
    }
  })
})

const findSettingTenant = (tenant = null) => new Promise((resolve, reject) => {
  settings.byTenant(tenant).findOne({}, (err, item) => {
    if (!err) {
      resolve(item)
    } else {
      reject(err)
    }
  })
})

const checkKeys = (tenant, secret = 'plugin.features.keys.clientID') => new Promise(async (resolve, reject) => {
  const dataTenant = await findSettingTenant(tenant);
  pluginsSettings.findOne({
      "plugin.path": pathName,
      "plugin.features.keys.clientID": {$ne: null},
      "plugin.features.keys.secretID": {$ne: null},
      "tenantId": tenant
    }, `${secret} currency currencySymbol plugin.features.keys.mode`,
    (err, response) => {
      if (!err && response) {
        const keys = response.get('plugin');
        const validCurrency = (['USD', 'MXN', 'EUR'].includes(dataTenant.currency))
        const currencySymbol = dataTenant.currencySymbol
        response = keys
        const {features} = response
        resolve({
          keys: features.keys,
          currency: dataTenant.currency,
          validCurrency,
          currencySymbol
        })
      } else {
        resolve(null)
      }
    })
})

const createOrder = async (value, tenant, user) => new Promise(async (resolve, reject) => {
  try {
    const dataKeys = await checkKeys(tenant, 'plugin.features.keys.clientID plugin.features.keys.secretID');
    const {clientID = null, secretID = null, mode = null} = dataKeys.keys;
    const tenantData = await getSetting(tenant);
    const {amount, customer, description = '', reference = '', tag = []} = value
    let purchase_units = [{
      amount: {
        currency_code: tenantData.currency,
        value: amount
      }
    }]
    let order = new Orders({
      customer: customer._id,
      author: user,
      platform: pathName,
      amount: amount,
      currency: tenantData.currency,
      description: description,
      reference: reference,
      tag: (tag) ? tag : [],
      tenantId: tenant,
      customData: {}
    });
    order.save();
    let url_callback = "https://www.google.com/"
    let body = {
      intent: 'AUTHORIZE', purchase_units: purchase_units, application_context: {
        brand_name: `${tenantData.name}${brandName}`,
        landing_page: 'NO_PREFERENCE', // Default, para mas informacion https://developer.paypal.com/docs/api/orders/v2/#definition-order_application_context
        user_action: 'PAY_NOW', // Accion para que en paypal muestre el monto del pago
        return_url: `${url_callback}&id_order=${order._id}` // Url despues de realizar el pago
      }
    }
    const token = Buffer.from(`${clientID}:${secretID}`, 'utf8').toString('base64')
    let headers = {
      'Accept': 'application/json',
      'Authorization': `Basic ${token}`,
    };
    let data = {};
    try {
      data = await httpRequest(`${urlApi[mode]}/v2/checkout/orders`, 'POST', body, headers)
    } catch (error) {
      order.delete();
      reject(utils.buildErrObject(error.code, error.message));
    }

    const link = data.links.find(r => r.rel === 'approve')
    order.customData = {tracker: data.id, link_payment: link}
    order = await db.createItem(order, Orders)
    resolve(order)
  } catch (error) {
    reject(utils.buildErrObject(error.code, error.message));
  }
})

const updateOrder = async (value, tenant, user) => new Promise(async (resolve, reject) => {
  try {
    const dataKeys = await checkKeys(tenant, 'plugin.features.keys.clientID plugin.features.keys.secretID plugin.features.keys.mode');
    const {clientID = null, secretID = null, mode = null} = dataKeys.keys;
    let order_id = value.token
    console.log(value)
    const url = `${urlApi[mode]}/v2/checkout/orders/${order_id}`
    const token = Buffer.from(`${clientID}:${secretID}`, 'utf8').toString('base64')
    let headers = {
      'Accept': 'application/json',
      'Authorization': `Basic ${token}`,
    };
    let body = [{
      op: "replace",
      path: "/purchase_units/@reference_id=='default'",
      value:
        {
          amount: {
            currency_code: value.currency,
            value: value.amount
          }
        }

    }]
    let data = await httpRequest(url, 'PATCH', body, headers)
    resolve(data)
  } catch (error) {
    reject(utils.buildErrObject(error.code, error.message));
  }
});

const checkOrder = async (value, tenant, user) => new Promise(async (resolve, reject) => {
  try {
    const dataKeys = await checkKeys(tenant, 'plugin.features.keys.clientID plugin.features.keys.secretID');
    const {clientID = null, secretID = null, mode} = dataKeys.keys;
    let orderToken = value.token
    let orderId = value.id
    const url = `${urlApi[mode]}/v2/checkout/orders/${orderToken}`
    const token = await getToken(clientID, secretID)
    let headers = {
      'Accept': 'application/json',
      'Authorization': `Basic ${token}`,
    };
    let data = await httpRequest(url, 'GET', {}, headers)
    switch (data.status) {
      case 'APPROVED':
        let approved = await authorizePayment(data.id, clientID, secretID, mode)
        if (approved.status === 'COMPLETED') {
          let order = await Orders.findOne({'customData.tracker': orderId})
          order.customData = {...order.customData, ...{response: data}}
          order.status = 'success'
          await order.save();
        }
        return resolve(approved)
      case 'COMPLETED':
        let order = await Orders.findOne({'customData.tracker': orderId})
        order.customData = {...order.customData, ...{response: data}}
        order.status = 'success'
        await order.save();
        break;
      default:
        break;
    }
    resolve(data)
  } catch (error) {
    reject(utils.buildErrObject(error.code, error.message));
  }

});

const updateSetting = (purchase, template, value, tenant, dataUser) => new Promise((resolve, reject) => {
  const {clientID = null, secretID = null, mode = 'test'} = value;
  if (dataUser.role === 'admin') {
    pluginsSettings.findOneAndUpdate({"plugin.path": pathName, "tenantId": tenant},
      {
        "$set": {
          "plugin.features.keys": {
            clientID, secretID, mode
          }
        }
      },
      false,
      (err, item) => {
        if (!err) {
          resolve(item)
        } else {
          reject(err)
        }
      })
  }
});

const authorizePayment = async (order_id, clientID, secretID, mode) => new Promise(async (resolve, reject) => {
  try {
    const token = await getToken(clientID, secretID)
    let headers = {
      'Accept': 'application/json',
      'Authorization': `Basic ${token}`,
    };
    const url = `${urlApi[mode]}/v2/checkout/orders/${order_id}/authorize`
    let data = await httpRequest(url, 'POST', {}, headers)
    resolve(data)
  } catch (error) {
    console.log(error)
    reject(utils.buildErrObject(error.code, error.message));
  }
});

const getToken = async (clientID, secretID) => new Promise(async (resolve, reject) => {
  resolve(Buffer.from(`${clientID}:${secretID}`, 'utf8').toString('base64'))
})

const generateLink = (value, tenant, track) => new Promise(async (resolve, reject) => {
  try {
    const dataKeys = await checkKeys(tenant, 'plugin.features.keys.clientID plugin.features.keys.secretID');
    if (dataKeys) {
      const {clientID = null, secretID = null, mode = null} = dataKeys.keys;
      const tenantData = await getSetting(tenant);
      const {amount} = value
      let purchase_units = [{
        amount: {
          currency_code: tenantData.currency,
          value: amount
        }
      }]
      const urlCallBack = `${process.env.FRONTEND_URL_TENANT
        .replace(/__TENANT__/gi, tenant)}/add-events/paypal-cb/${track}`
      const body = {
        intent: 'AUTHORIZE', purchase_units: purchase_units, application_context: {
          brand_name: `${tenantData.name}${brandName}`,
          landing_page: 'NO_PREFERENCE',
          user_action: 'PAY_NOW',
          return_url: urlCallBack
        }
      }
      const token = Buffer.from(`${clientID}:${secretID}`, 'utf8').toString('base64')
      const headers = {
        'Accept': 'application/json',
        'Authorization': `Basic ${token}`,
      };
      const data = await httpRequest(`${urlApi[mode]}/v2/checkout/orders`, 'POST', body, headers);
      const link = data.links.find(r => r.rel === 'approve')
      resolve(link)
    } else {
      console.log('NOT_KEYS_PAYPAL');
      resolve(null)
    }
  } catch (e) {
    console.log({message: e.error, code: 422})
    resolve(null)
  }
})

const getOrder = (value) => new Promise((resolve, reject) => {
  const {tracker} = value;
  Orders.aggregate(
    [
      {
        $lookup: {
          from: 'users',
          let: {idUser: '$customer'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idUser', '$_id']}]
                }
              }
            },
            {
              $project: {
                _id: 1,
                name: 1,
                lastName: 1
              }
            }
          ],
          as: 'customer'
        }
      },
      {
        $lookup: {
          from: 'settings',
          let: {tenant: '$tenantId'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$tenant', '$tenantId']}],
                }
              }
            },
            {
              $project: {
                name: 1,
                logo: 1
              }
            },
          ],
          as: 'settings'
        },
      },
      {
        $lookup: {
          from: 'pluginssettings',
          let: {tenant: '$tenantId'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$tenant', '$tenantId']}],
                }
              }
            },
            {
              $project: {
                plugin: 1
              }
            },
          ],
          as: 'pluginssettings'
        }
      },
      {$unwind: '$customer'},
      {$unwind: '$settings'},
      {$unwind: '$pluginssettings'},
      {
        $project: {
          customData: 1,
          "pk": ["$pluginssettings.plugin.features.keys.pk"],
          "settings.logo": 1,
          "settings.name": 1,
          "customer.name": 1,
          "customer.lastName": 1,
          "customer.email": 1,
          "customer._id": 1,
          status: 1,
          _id: 1,
          description: 1,
          pluginssettings: 1,
          currency: 1,
          amount: 1
        }
      },
      {
        $match: {
          "customData.tracker": {$exists: true, $eq: tracker}
        }
      }
    ],
    (err, res) => {
      resolve(res.find(() => true))
    })
})

/********************
 * Public functions *
 ********************/
exports.generate_link = (value, tenant, track) => generateLink(value, tenant, track)
// exports.create_order = async ({}, parentModule, value, tenant, dataUser) => await createOrder(value, tenant, dataUser)
exports.check_order = async ({}, parentModule, value, tenant, dataUser) => await checkOrder(value, tenant, dataUser)
exports.update_order = async ({}, parentModule, value, tenant, dataUser) => await updateOrder(value, tenant, dataUser)
exports.check_keys = (purchase, template, value = {}, tenant = null, dataUser = null) => checkKeys(tenant,
  'plugin.features.keys.clientID plugin.features.keys.secretID plugin.features.keys.mode'
)
exports.get_order = ({}, parentModule, value, tenant) => getOrder(value)
exports.update_setting = (purchase, template, value = {}, tenant = null, dataUser = null) => {
  return updateSetting(purchase, template, value, tenant, dataUser);
}
