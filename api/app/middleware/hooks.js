const stripe = require('../service/stripe')
const model = require('../models/clientsMachine')
const pluginSettings = require('../models/pluginsSettings')
/**
 *
 */

exports.checkDomain = async (req, res, next) => {
  try {
    const key = req.get('keyMachine')
    // const hash = req.params.hash
    model.findOne({key}, (err, response) => {
      if (err || !response) {
        res.status(500).json({errors: {message: 'unauthorized'}})
      } else {
        req.clientMachine = response
        req.clientAccount = response.tenantId;
        next()
      }
    })
  } catch (e) {
    res.status(500).json({errors: {message: e.message}})
    req.clientAccount = null
  }
}

exports.checkSignatureStripe = async (req, res, next) => {
  try {
    const signature = req.get('stripe-signature')
    // console.log('-------------->', event)
    model.findOne({key: 'stripe'}, async (err, response) => {
      if (err || !response) {
        new Error('unauthorized');
      } else {
        req.clientMachine = response
        // req.clientAccount = response.tenantId;
        stripe.checkSignatureHook(req.rawBody, signature, response.hash)
          .then(() => {
            next()
          })
          .catch(() => {
            res.status(500).json({errors: {message: 'unauthorized'}})
          })
      }
    })
    // next()
  } catch (e) {
    res.status(500).json({errors: {message: e.message}})
    req.clientAccount = null
  }
}

exports.checkSignatureStripePer = async (req, res, next) => {
  try {
    const tenant = req.params.tenant;
    const signature = req.get('stripe-signature')
    const pluginData = await pluginSettings.byTenant(tenant).findOne({'plugin.path': 'stripe'})
    const {features = {}} = pluginData.plugin;
    stripe.checkSignatureHook(req.rawBody, signature, features.keys.wh_sk)
      .then(() => {
        next()
      })
      .catch(() => {
        res.status(500).json({errors: {message: 'unauthorized'}})
      })

  } catch (e) {
    res.status(500).json({errors: {message: e.message}})
    req.clientAccount = null
  }
}

exports.checkTenant = async (req, res, next) => {
  next()
}
