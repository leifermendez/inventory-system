const nodemailer = require('nodemailer')
const mg = require('nodemailer-mailgun-transport')
const sgTransport = require('nodemailer-sendgrid-transport');
const smtpTransport = require('nodemailer-smtp-transport')
const sgMail = require('@sendgrid/mail')
const i18n = require('i18n')
const User = require('../models/user')
const {itemAlreadyExists} = require('../middleware/utils')

/**
 * Sends email
 * @param {Object} data - data
 * @param {boolean} callback - callback
 */
const sendEmail = async (data, callback) => {
  let auth = {}
  let options = {}
  if (process.env.NODE_ENV === 'production') {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY)
  } else {
    options = {
      host: process.env.EMAIL_SMTP,
      port: process.env.EMAIL_SMTP_PORT,
      auth: {
        user: process.env.EMAIL_SMTP_USER,
        pass: process.env.EMAIL_SMTP_PASS
      }
    }
    auth = smtpTransport(options)
  }
  const transporter = nodemailer.createTransport(auth)
  const mailOptions = {
    from: `${process.env.EMAIL_FROM_NAME} <${process.env.EMAIL_FROM_ADDRESS}>`,
    to: `${data.user.name} <${data.user.email}>`,
    subject: data.subject,
    html: data.htmlMessage,
    bcc: process.env.EMAIL_BBC
  }

  if (process.env.NODE_ENV === 'production') {
    sgMail.send(mailOptions).then(() => {
      console.log('Send email')
      return callback(true)
    }).catch((e) => {
      console.log('Error send  email', e)
      return callback(false)
    })
  } else {
    transporter.sendMail(mailOptions, (err) => {
      if (err) {
        return callback(false)
      }
      return callback(true)
    })
  }
}


/**
 * Prepares to send email
 * @param {string} user - user object
 * @param {string} subject - subject
 * @param {string} htmlMessage - html message
 */
const prepareToSendEmail = (user, subject, htmlMessage) => {
  user = {
    name: user.name,
    email: user.email,
    verification: user.verification
  }
  const data = {
    user,
    subject,
    htmlMessage
  }
  if (process.env.NODE_ENV === 'production') {
    sendEmail(data, (messageSent) =>
      messageSent
        ? console.log(`Email SENT to: ${user.email}`)
        : console.log(`Email FAILED to: ${user.email}`)
    )
  } else if (process.env.NODE_ENV === 'development') {
    console.log(data)
  }
}

module.exports = {
  /**
   * Checks User model if user with an specific email exists
   * @param {string} email - user email
   */
  async emailExists(email, tenant = null) {
    return new Promise((resolve, reject) => {
      User.byTenant(tenant).findOne(
        {
          email
        },
        (err, item) => {
          itemAlreadyExists(err, item, reject, 'EMAIL_ALREADY_EXISTS')
          resolve(false)
        }
      )
    })
  },

  /**
   * Checks User model if user with an specific email exists but excluding user id
   * @param {string} id - user id
   * @param {string} email - user email
   */
  async emailExistsExcludingMyself(id, email, tenant = null) {
    return new Promise((resolve, reject) => {
      User
        .byTenant(tenant)
        .findOne(
          {
            email,
            _id: {
              $ne: id
            }
          },
          (err, item) => {
            itemAlreadyExists(err, item, reject, 'EMAIL_ALREADY_EXISTS')
            resolve(false)
          }
        )
    })
  },

  /**
   * Sends registration email
   * @param {string} locale - locale
   * @param {Object} user - user object
   * @param tenant
   */
  async sendRegistrationEmailMessage(locale, user, tenant = '') {
    i18n.setLocale(locale)
    const subject = i18n.__('registration.SUBJECT')
    const urlTenant = process.env.FRONTEND_URL_TENANT.replace(/__TENANT__/gi, tenant);
    const htmlMessage = i18n.__(
      'registration.MESSAGE',
      user.email,
      urlTenant,
      user.verification
    )
    prepareToSendEmail(user, subject, htmlMessage)
  },

  /**
   * Sends reset password email
   * @param {string} locale - locale
   * @param {Object} user - user object
   * @param tenant
   */
  async sendResetPasswordEmailMessage(locale, user, tenant = '') {
    i18n.setLocale(locale)
    const subject = i18n.__('forgotPassword.SUBJECT')
    const urlTenant = process.env.FRONTEND_URL_TENANT.replace(/__TENANT__/gi, tenant);
    const htmlMessage = i18n.__(
      'forgotPassword.MESSAGE',
      user.email,
      urlTenant,
      user.verification
    )
    prepareToSendEmail(user, subject, htmlMessage)
  },
  async sendNotifications(locale, user, url = '', tenant = '') {
    i18n.setLocale(locale)
    const subject = i18n.__('sendNotification.SUBJECT')
    let urlTenant = process.env.FRONTEND_URL_TENANT.replace(/__TENANT__/gi, tenant);
    urlTenant += url;
    const htmlMessage = i18n.__(
      'sendNotification.MESSAGE',
      user.email,
      urlTenant,
      user.verification
    )
    prepareToSendEmail(user, subject, htmlMessage)
  }
}
