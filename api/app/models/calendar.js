const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');

const CalendarSchema = new mongoose.Schema(
  {
    start: {
      type: Date,
      default: null,
      required: true
    },
    end: {
      type: Date,
      default: null
    },
    title: {
      type: String,
      default: '',
      required: true
    },
    observation: {
      type: String,
      default: '',
      required: false
    },
    color: {
      type: String,
      default: 'yellow',
      required: false
    },
    allDay: {
      type: Boolean,
      default: true
    },
    user: {
      type: Object,
      required: true
    },
    author: {
      type: mongoose.Types.ObjectId,
      required: true
    },
    customData: {
      type: Object
    },
    tag: {
      type: Array,
      default: []
    },
    dummy: {
      type: Boolean,
      default: false
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
CalendarSchema.plugin(mongoose_delete, {overrideMethods: 'all'});
CalendarSchema.plugin(mongoose_delete)
CalendarSchema.plugin(mongoosePaginate)
CalendarSchema.plugin(mongoTenant)
module.exports = mongoose.model('Calendar', CalendarSchema)
