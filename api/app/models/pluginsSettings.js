const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const mongoTenant = require('mongo-tenant')
const mongoose_delete = require('mongoose-delete');

const PluginSettingSchema = new mongoose.Schema(
  {
    plugin: {
      type: Object,
      required: true
    },
    status: {
      type: String,
      enum: ['enabled', 'disabled'],
      default: 'enabled'
    },
    customData: {
      type: Object
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
PluginSettingSchema.plugin(mongoosePaginate)
PluginSettingSchema.plugin(mongoTenant)
PluginSettingSchema.plugin(mongoose_delete, { overrideMethods: 'all' });
PluginSettingSchema.plugin(mongoose_delete)
module.exports = mongoose.model('PluginsSettings', PluginSettingSchema)
