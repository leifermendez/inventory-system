const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const aggregatePaginate = require('mongoose-aggregate-paginate-v2')

const ReferredSchema = new mongoose.Schema(
  {
    userTo: {
      type: mongoose.Types.ObjectId,
      required: true
    },
    userFrom: {
      type: mongoose.Types.ObjectId,
      required: true
    },
    status: {
      type: String,
      enum: ['available', 'unavailable'],
      default: 'available'
    },
    amountFrom: {
      type: Number,
      required: true,
      default: 0
    },
    amountTo: {
      type: Number,
      required: true,
      default: 0
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)

ReferredSchema.plugin(mongoosePaginate)
ReferredSchema.plugin(aggregatePaginate)
module.exports = mongoose.model('referredUser', ReferredSchema)
