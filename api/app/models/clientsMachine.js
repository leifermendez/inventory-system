const mongoose = require('mongoose')
// const mongoosePaginate = require('mongoose-paginate-v2')
const aggregatePaginate = require('mongoose-aggregate-paginate-v2')
// const softDelete = require('mongoose-softdelete');
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');
const ClientsMachine = new mongoose.Schema({
  hostName: String,
  key: String,
  owner: { type: String, required: true },
  hash: { type: String, required: true },
  customData: Object
}, {
  versionKey: false,
  timestamps: true
})

ClientsMachine.plugin(mongoose_delete, { overrideMethods: 'all' });
ClientsMachine.plugin(mongoose_delete)

ClientsMachine.plugin(mongoTenant)
ClientsMachine.plugin(aggregatePaginate)

module.exports = mongoose.model('ClientsMachine', ClientsMachine)
