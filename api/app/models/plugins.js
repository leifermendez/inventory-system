const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const mongoTenant = require('mongo-tenant');
const mongoose_delete = require('mongoose-delete');
const PluginsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    path: {
      type: String,
      required: true,
      unique: true
    },
    icon: {
      type: String,
      required: false
    },
    video: {
      type: String,
      required: false
    },
    features: {
      type: Object,
      required: true
    },
    periodTry: {
      type: Number,
      default: 0
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
PluginsSchema.plugin(mongoosePaginate)
PluginsSchema.plugin(mongoTenant)
PluginsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });
PluginsSchema.plugin(mongoose_delete)
module.exports = mongoose.model('Plugins', PluginsSchema)
