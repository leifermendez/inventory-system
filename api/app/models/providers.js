const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');
const ProvidersSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    manager: {
      type: Object,
      required: true
    },
    address: {
      type: Object,
      required: true
    },
    trace: {
      type: Object
    },
    phone: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    tag: {
      type: Array,
      default: []
    },
    description: {
      type: String
    },
    dummy: {
      type: Boolean,
      default: false
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)


ProvidersSchema.plugin(mongoosePaginate)
ProvidersSchema.plugin(mongoTenant)
ProvidersSchema.plugin(mongoose_delete, { overrideMethods: 'all' });
ProvidersSchema.plugin(mongoose_delete)
module.exports = mongoose.model('Providers', ProvidersSchema)
