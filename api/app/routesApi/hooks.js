const controller = require('../controllers/hooks')
const validate = require('../controllers/hooks.validate')
const express = require('express')
const bodyParser = require('body-parser');
const router = express.Router()
const hooks = require('../middleware/hooks')
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

/**
 * Get productos
 */
router.get(
  '/products',
  hooks.checkDomain,
  validate.getHooks,
  controller.getProducts
)


router.get(
  '/token',
  hooks.checkDomain,
  validate.getHooks,
  controller.validToken
)

/**
 * Hook stripe MAIN system
 */

router.post(
  '/stripe',
  hooks.checkSignatureStripe,
  controller.getHookStripe
)


/**
 * Hook for single order pay
 */
/*
Se debe crear y registrar webhook en la cuenta stripe del clinete con un quer param del tenant
luego agarrar el evento y dependiendo del id tenant y status cambiar status interno
 */
router.post(
  '/:tenant/stripe',
  hooks.checkSignatureStripePer,
  controller.getHookStripePer
)

module.exports = router
