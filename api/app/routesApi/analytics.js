const controller = require('../controllers/analytics')
const validate = require('../controllers/analytics.validate')
const AuthController = require('../controllers/auth')
const origin = require('../middleware/origin')
const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})

/*
 * Analytics default routes
 */
router.get(
  '/default',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager', 'seller']),
  validate.getAnalytic,
  controller.getItems
)
/*
 * Analytics by author
 */
router.get(
  '/author',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager', 'seller']),
  validate.getAnalytic,
  controller.getAuthor
)
/*
 * Analytics by items
 */
router.get(
  '/items',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager', 'seller']),
  validate.getAnalytic,
  controller.getAnalyticsItems
)
/*
 * Analytics purchases
 */
router.get(
  '/purchases',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager', 'seller']),
  validate.getAnalytic,
  controller.getPurchases
)

module.exports = router
