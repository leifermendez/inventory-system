const controller = require('../controllers/plugins')
const validate = require('../controllers/plugins.validate')
const AuthController = require('../controllers/auth')
const origin = require('../middleware/origin')
const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

/*
 * Get items route
 */
router.get(
  '/',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager']),
  trimRequest.all,
  controller.getItems
)

/*
 * Create new item route
 */
router.post(
  '/',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  validate.createItem,
  controller.createItem
)

/*
 * Create new item route
 */
router.post(
  '/:id/active',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  validate.activeItem,
  controller.activeItem
)

/*
 * Get item route
 */
router.get(
  '/:id',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager']),
  trimRequest.all,
  validate.getItem,
  controller.getItem
)

/*
 * Update item route
 */
router.patch(
  '/:id',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager']),
  trimRequest.all,
  validate.updateItem,
  controller.updateItem
)

/*
 * Disabled item route
 */
router.delete(
  '/:id/disabled',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager']),
  trimRequest.all,
  validate.deleteItem,
  controller.disabledItem
)

/*
 * Delete item route
 */
router.delete(
  '/:id',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin', 'manager']),
  trimRequest.all,
  validate.deleteItem,
  controller.deleteItem
)

/**
 * Actions for events each plugin
 */

/*
 * Get item events
 */
router.get(
  '/:id/events/:action',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  trimRequest.all,
  validate.actionsPlugin,
  controller.actionForPlugin
)

/*
 * Post item events
 */
router.post(
  '/:id/events/:action',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  trimRequest.all,
  validate.actionsPlugin,
  controller.actionForPlugin
)

/* Free router */

router.post(
  '/public/:id/events/:action',
  origin.checkDomain,
  origin.checkTenant,
  trimRequest.all,
  validate.actionsPlugin,
  controller.actionForPublic
)

module.exports = router
