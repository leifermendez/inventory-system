/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Order = require('../app/models/orders')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const loginDetails = {
    admin: {
        id: '5aa1c2c35ef7a4e97b5e995a',
        email: 'admin@admin.com',
        password: '12345',
    },
    user: {
        id: '5aa1c2c35ef7a4e97b5e995b',
        email: 'user@user.com',
        password: '12345'
    },
    manager: {
        id: '5aa1c2c35ef7a4e97b5e995c',
        email: 'manager@manager.com',
        password: '12345'
    },
    seller: {
        id: '5aa1c2c35ef7a4e97b5e995d',
        email: 'seller@seller.com',
        password: '12345'
    },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
    admin: '',
    user: '',
    manager: '',
    seller: '',
}
const author = require('./../data/1.users/user')
const deposit = require('../data/4.deposits/deposits')
const { fake } = require('faker')
const products = require('../data/5.products/products')
const provider = require('../data/6.providers/providers')
const purchases = require('../data/8.purchases/purchases')
const orders = require('../data/9.orders/orders')

chai.use(chaiHttp)

describe('*********** ORDERS ***********', () => {
    describe('/POST login', () => {
        it('it should GET token as admin', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.admin)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.admin = res.body.session
                    done()
                })
        })
        it('it should GET token as user', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.user)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.user = res.body.session
                    done()
                })
        })
        it('it should GET token as manager', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.manager)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.manager = res.body.session
                    done()
                })
        })
        it('it should GET token as seller', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.seller)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.seller = res.body.session
                    done()
                })
        })
    })
    // Get all orders
    describe('/GET orders', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            chai
                .request(server)
                .get('/api/1.0/orders')
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should GET all orders sending a the token like admin, manager and seller', (done) => {
            chai
                .request(server)
                .get('/api/1.0/orders')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('docs', 'hasNextPage','hasPrevPage','limit','nextPage','page',
                    'pagingCounter','prevPage','totalDocs','totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')                    
                    res.body.docs[0].amount.should.be.a('string')
                    res.body.docs[0].author.should.be.a('object')
                    res.body.docs[0].currency.should.be.a('string')
                    res.body.docs[0].customer.should.be.a('object')
                    res.body.docs[0].description.should.be.a('string')
                    res.body.docs[0].platform.should.be.a('string')
                    res.body.docs[0].purchase.should.be.a('string')
                    res.body.docs[0].reference.should.be.a('string')
                    res.body.docs[0].status.should.be.a('string')
                    res.body.docs[0].tag.should.be.a('array')
                    res.body.docs[0]._id.should.be.a('string')
                    done()
                })
        })
        it('it should NOT be able to consume the route if the token is like user', (done) => {
            chai
                .request(server)
                .get('/api/1.0/orders')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.an('object')
                    // res.body.should.include.keys('name', 'email')
                    done()
                })
        })
        it('it should GET the orders with filters', (done) => {
            chai
                .request(server)
                .get('/api/1.0/orders?filter=REF-P&fields=reference')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('docs', 'hasNextPage','hasPrevPage','limit','nextPage','page',
                    'pagingCounter','prevPage','totalDocs','totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs.should.be.a('array')
                    res.body.docs.should.have.lengthOf(1)                    
                    res.body.docs[0].amount.should.be.a('string')
                    res.body.docs[0].author.should.be.a('object')
                    res.body.docs[0].currency.should.be.a('string')
                    res.body.docs[0].customer.should.be.a('object')
                    res.body.docs[0].description.should.be.a('string')
                    res.body.docs[0].platform.should.be.a('string')
                    res.body.docs[0].purchase.should.be.a('string')
                    res.body.docs[0].reference.should.be.a('string')
                    res.body.docs[0].status.should.be.a('string')
                    res.body.docs[0].tag.should.be.a('array')
                    res.body.docs[0]._id.should.be.a('string')
                    res.body.docs[0].should.have.property("reference").eql('REF-PRUEBA')
                    done()
                })
        })
    })
    describe('/POST orders', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            chai
                .request(server)
                .post('/api/1.0/orders')
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should NOT POST a orders without customer,author, platform, reference, amount and currency', (done) => {
            const order = {}
            chai
                .request(server)
                .post('/api/1.0/orders')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(order)
                .end((err, res) => {
                    res.should.have.status(422)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
        it('it should POST a order only like admin', (done) => {
            const order = {
                status : "success",
                tag : [ 
                    "TEST"
                ],
                customer : author[3]._id,
                platform : "cash",
                amount : "300",
                purchase : purchases[0]._id,
                reference : "REF-TEST",
                description : "TEST",
                author : author[0]._id,
                currency : "USD",
                deliveryDate : 'to_send',
                tenantId : "subdomain",
            }
            chai
                .request(server)
                .post('/api/1.0/orders')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(order)
                .end((err, res) => {
                    console.log(res.body)
                    res.should.have.status(201)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('_id', 'customer','author','status','platform','purchase','reference','amount','currency','description','tag',
                    'customData')
                    createdID.push(res.body._id)
                    done()
                })
        })
        it('it should NOT POST a order with token like manager, seller, customer and user', (done) => {
            const order = {
                status : "success",
                tag : [ 
                    "TEST"
                ],
                customer : author[3]._id,
                platform : "cash",
                amount : "300",
                purchase : purchases[0]._id,
                reference : "REF-TEST",
                description : "TEST",
                author : author[0]._id,
                currency : "USD",
                tenantId : "subdomain",
            }
            chai
                .request(server)
                .post('/api/1.0/orders')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .send(order)
                .end((err, res) => {
                    // console.log(res.body)
                    // console.log(res.body.errors)
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    describe('/GET/:id order', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .get(`/api/1.0/orders/${id}`)
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should GET a order by the given id like admin, manager and seller', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .get(`/api/1.0/orders/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((error, res) => {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('amount','author','customer','description','platform',
                    'purchase','reference','status','tag','_id')
                    res.body.should.have.property('_id').eql(id)
                    done()
                })
        })
    })
    describe('/PATCH/:id order', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            const order = {
                status : "success",
                tag : [ 
                    "TEST"
                ],
                customer : author[3]._id,
                platform : "cash",
                amount : "300",
                purchase : purchases[0]._id,
                reference : "REF-TEST",
                description : "TEST",
                author : author[0]._id,
                currency : "USD",
                tenantId : "subdomain",
            }
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/orders/${id}`)
                .set('Origin', origin)
                .send(order)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should UPDATE a order given the id with token only like admin ', (done) => {
            const order = {
                status : "success",
                tag : [ 
                    "TEST"
                ],
                customer : author[3]._id,
                platform : "cash",
                amount : "300",
                purchase : purchases[0]._id,
                reference : "REF-TEST-UPDATED",
                description : "TEST_UPDATED",
                author : author[0]._id,
                currency : "USD",
                convert: {},
                tenantId : "subdomain",
            }
            const id = createdID.slice(-1).pop()
            console.log(id)
            chai
                .request(server)
                .patch(`/api/1.0/orders/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(order)
                .end((error, res) => {
                    console.log(res.body.errors)
                    console.log(res.body)
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('_id','purchase','tag','deleted','customer','platform','amount','reference','description','author',
                    'currency','customData', 'tenantId','createdAt','updatedAt')
                    res.body.should.have.property('_id').eql(id)
                    res.body.should.have.property('description').eql('TEST_UPDATED')
                    done()
                })
        })
        it('it should NOT UPDATE a order given the id with token like customer, seller and user', (done) => {
            const order = {
                status : "success",
                tag : [ 
                    "TEST"
                ],
                customer : author[3]._id,
                platform : "cash",
                amount : "300",
                purchase : purchases[0]._id,
                reference : "REF-TEST-UPDATED",
                description : "TEST_UPDATED",
                author : author[0]._id,
                currency : "USD",
                tenantId : "subdomain",
            }
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/orders/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .send(order)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    describe('/DELETE/:id order', () => {
        it('it should DELETE a order given the id with token only like admin ', (done) => {
            const order = {
                status : "success",
                tag : [ 
                    "TEST"
                ],
                customer : author[3]._id,
                platform : "cash",
                amount : "300",
                purchase : purchases[0]._id,
                reference : "REF-TEST-UPDATED",
                description : "TEST_UPDATED",
                author : author[0]._id,
                currency : "USD",
                tenantId : "subdomain",
            }
            chai
                .request(server)
                .post('/api/1.0/orders')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(order)
                .end((err, res) => {
                    console.log(res.body.errors)
                    res.should.have.status(201)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('_id', 'description')
                    chai
                        .request(server)
                        .delete(`/api/1.0/orders/${res.body._id}`)
                        .set('Origin', origin)
                        .set('Authorization', `Bearer ${tokens.admin}`)
                        .end((error, result) => {
                            result.should.have.status(200)
                            result.body.should.be.a('object')
                            result.body.should.have.property('msg').eql('DELETED')
                            done()
                        })
                })
        })
        it('it should NOT DELETE a order given the id with token like customer, seller and user', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .delete(`/api/1.0/orders/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    after(() => {
        createdID.forEach((id) => {
            Order.findByIdAndRemove(id, (err) => {
                if (err) {
                    console.log(err)
                }
            })
        })
    })

});