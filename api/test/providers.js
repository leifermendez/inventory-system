/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Provider = require('../app/models/providers')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const loginDetails = {
  admin: {
    id: '5aa1c2c35ef7a4e97b5e995a',
    email: 'admin@admin.com',
    password: '12345',
  },
  user: {
    id: '5aa1c2c35ef7a4e97b5e995b',
    email: 'user@user.com',
    password: '12345'
  },
  manager: {
    id: '5aa1c2c35ef7a4e97b5e995c',
    email: 'manager@manager.com',
    password: '12345'
  },
  seller: {
    id: '5aa1c2c35ef7a4e97b5e995d',
    email: 'seller@seller.com',
    password: '12345'
  },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
  admin: '',
  user: '',
  manager: '',
  seller: '',
}
const manager = require('./../data/1.users/user')
const { fake } = require('faker')

chai.use(chaiHttp)

describe('*********** PROVIDERS ***********', () => {
  describe('/POST login', () => {
    it('it should GET token as admin', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(loginDetails.admin)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.admin = res.body.session
          done()
        })
    })
    it('it should GET token as user', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(loginDetails.user)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.user = res.body.session
          done()
        })
    })
    it('it should GET token as manager', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(loginDetails.manager)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.manager = res.body.session
          done()
        })
    })
    it('it should GET token as seller', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(loginDetails.seller)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.seller = res.body.session
          done()
        })
    })
  })
  // Get all providers
  describe('/GET providers', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      chai
        .request(server)
        .get('/api/1.0/providers')
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should GET all providers sending a the token like admin', (done) => {
      chai
        .request(server)
        .get('/api/1.0/providers')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
            'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs[0].should.include.keys('address','description','email','manager','name','phone','tenantId','trace','_id')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].description.should.be.a('string')
          res.body.docs[0].email.should.be.a('string')
          res.body.docs[0].manager.should.be.a('object')
          res.body.docs[0].name.should.be.a('string')
          res.body.docs[0].phone.should.be.a('string')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0].trace.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          done()
        })
    })
    it('it should NOT be able to consume the route if the token is like user and seller', (done) => {
      chai
        .request(server)
        .get('/api/1.0/providers')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.user}`)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.an('object')
          // res.body.should.include.keys('name', 'email')
          done()
        })
    })
    it('it should GET all providers sending a the token like manager and admin', (done) => {
      chai
        .request(server)
        .get('/api/1.0/providers')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
            'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs[0].should.include.keys('address','description','email','manager','name','phone','tenantId','trace','_id')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].description.should.be.a('string')
          res.body.docs[0].email.should.be.a('string')
          res.body.docs[0].manager.should.be.a('object')
          res.body.docs[0].name.should.be.a('string')
          res.body.docs[0].phone.should.be.a('string')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0].trace.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          done()
        })
    })
    it('it should GET the providers with filters', (done) => {
      chai
        .request(server)
        .get('/api/1.0/providers?filter=Proveedor Seed&fields=name')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.docs.should.be.a('array')
          res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
            'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs[0].should.include.keys('address','description','email','manager','name','phone','tenantId','trace','_id')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].description.should.be.a('string')
          res.body.docs[0].email.should.be.a('string')
          res.body.docs[0].manager.should.be.a('object')
          res.body.docs[0].name.should.be.a('string')
          res.body.docs[0].phone.should.be.a('string')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0].trace.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          res.body.docs.should.have.lengthOf(1)
          res.body.docs[0].should.have.property('name').eql('Proveedor Seed')
          done()
        })
    })
  })
  describe('/POST providers', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      chai
        .request(server)
        .post('/api/1.0/providers')
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should NOT POST a providers without name, manager, address, phone and email ', (done) => {
      const provider = {}
      chai
        .request(server)
        .post('/api/1.0/providers')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(provider)
        .end((err, res) => {
          res.should.have.status(422)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
    it('it should POST a provider only like admin', (done) => {
      const provider = {
        name: faker.company.companyName(),
        manager: manager[0],
        address: faker.address.streetAddress(),
        trace: '',
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        tenantId: 'subdomain',
        tag: [],
        description: ''
      }
      chai
        .request(server)
        .post('/api/1.0/providers')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(provider)
        .end((err, res) => {
          // console.log(res.body.errors)
          // console.log(res.body)
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.include.keys('_id','tag','deleted','name','address','manager','trace','phone','email','description','createdAt','updatedAt',
          'tenantId')
          createdID.push(res.body._id)
          done()
        })
    })
    it('it should NOT POST a provider with token like seller, manager and user', (done) => {
      const provider = {
        name: faker.company.companyName(),
        manager: manager[0],
        address: faker.address.streetAddress(),
        trace: '',
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        tenantId: 'subdomain',
        tag: [],
        description: ''
      }
      chai
        .request(server)
        .post('/api/1.0/providers')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.seller}`)
        .send(provider)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  describe('/GET/:id provider', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .get(`/api/1.0/providers/${id}`)
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should GET a provider by the given id like admin, and manager', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .get(`/api/1.0/providers/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((error, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.include.keys('address','deleted','description','email','manager','name','tag','phone','tenantId','trace','_id')
          res.body.address.should.be.a('string')
          res.body.deleted.should.be.a('boolean')
          res.body.description.should.be.a('string')
          res.body.email.should.be.a('string')
          res.body.manager.should.be.a('object')
          res.body.name.should.be.a('string')
          res.body.phone.should.be.a('string')
          res.body.tag.should.be.a('array')
          res.body.tenantId.should.be.a('string')
          res.body.trace.should.be.a('string')
          res.body._id.should.be.a('string')
          res.body.should.have.property('_id').eql(id)
          done()
        })
    })
  })
  describe('/PATCH/:id provider', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      const provider = {
        name: faker.company.companyName(),
        manager: manager[0],
        address: faker.address.streetAddress(),
        trace: '',
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        tenantId: 'subdomain',
        tag: [],
        description: ''
      }
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .patch(`/api/1.0/providers/${id}`)
        .set('Origin', origin)
        .send(provider)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should UPDATE a provider given the id with token only like admin', (done) => {
      const newName = faker.company.companyName()
      const provider = {
        name: newName,
        manager: manager[0],
        address: faker.address.streetAddress(),
        trace: '',
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        tenantId: 'subdomain',
        tag: [],
        description: ''
      }
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .patch(`/api/1.0/providers/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(provider)
        .end((error, res) => {
          // console.log(res.body.errors)
          console.log(res.body)
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.include.keys('_id', 'address','deleted','description','email','manager','name','phone','tag','tenantId','trace')
          res.body.should.have.property('_id').eql(id)
          res.body.should.have.property('name').eql(newName)
          done()
        })
    })
    it('it should NOT UPDATE a provider given the id with token like manager, seller and user', (done) => {
      const provider = {
        name: faker.company.companyName(),
        manager: manager[0],
        address: faker.address.streetAddress(),
        trace: '',
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        tenantId: 'subdomain',
        tag: [],
        description: ''
      }
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .patch(`/api/1.0/providers/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.seller}`)
        .send(provider)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  describe('/DELETE/:id provider', () => {
    it('it should DELETE a provider given the id with token only like admin', (done) => {
      const provider = {
        name: faker.company.companyName(),
        manager: manager[0],
        address: faker.address.streetAddress(),
        trace: '',
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        tenantId: 'subdomain',
        tag: [],
        description: ''
      }
      chai
        .request(server)
        .post('/api/1.0/providers')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(provider)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.include.keys('_id', 'name')
          chai
            .request(server)
            .delete(`/api/1.0/providers/${res.body._id}`)
            .set('Origin', origin)
            .set('Authorization', `Bearer ${tokens.admin}`)
            .end((error, result) => {
              result.should.have.status(200)
              result.body.should.be.a('object')
              result.body.should.have.property('msg').eql('DELETED')
              done()
            })
        })
    })
    it('it should NOT DELETE a provider given the id with token like manager, seller and user', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .delete(`/api/1.0/providers/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.manager}`)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  after(() => {
    createdID.forEach((id) => {
      Provider.findByIdAndRemove(id, (err) => {
        if (err) {
          console.log(err)
        }
      })
    })
  })

});