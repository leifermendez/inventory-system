/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const loginDetails = {
    admin: {
        id: '5aa1c2c35ef7a4e97b5e995a',
        email: 'admin@admin.com',
        password: '12345',
    },
    customer: {
        id: '5aa1c2c35ef7a4e97b5e995b',
        email: 'customer@customer.com',
        password: '12345'
    },
    manager: {
        id: '5aa1c2c35ef7a4e97b5e995c',
        email: 'manager@manager.com',
        password: '12345'
    },
    seller: {
        id: '5aa1c2c35ef7a4e97b5e995d',
        email: 'seller@seller.com',
        password: '12345'
    },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
    admin: '',
    user: '',
    manager: '',
    seller: '',
}

describe('*********** ANALYTICS ***********', () => {
    describe('/POST login', () => {
        it('it should GET token as admin', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.admin)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.admin = res.body.session
                    done()
                })
        })
        it('it should GET token as customer', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.customer)
                .end((err, res) => {
                    console.log(res)
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.user = res.body.session
                    done()
                })
        })
        it('it should GET token as manager', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.manager)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.manager = res.body.session
                    done()
                })
        })
        it('it should GET token as seller', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.seller)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.seller = res.body.session
                    done()
                })
        })
    })
    // Get analityc
    describe('/GET default analytics', () => {
        it('it should GET default analytics sending a the token like anyone', (done) => {
            chai
                .request(server)
                .get('/api/1.0/analytics/default')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    console.log(res.body)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('wait','success')
                    res.body.wait.should.be.a('array')
                    res.body.success.should.be.a('array')
                    if( res.body.wait.length !== 0 ){
                        res.body.wait[0].should.be.a('object')
                        res.body.wait[0].should.include.keys('total','general')
                        res.body.wait[0].total.should.be.a('number')
                        res.body.wait[0].general.should.be.a('array')
                    }
                    if( res.body.success.length !== 0 ){
                        res.body.success[0].should.be.a('object')
                        res.body.success[0].should.include.keys('total','general')
                        res.body.success[0].total.should.be.a('number')
                        res.body.success[0].general.should.be.a('array')
                    }
                    done()
                })
        })
        it('it should GET default analytics sending a the token like anyone and parameters by date', (done) => {
            chai
                .request(server)
                .get('/api/1.0/analytics/default?start=2020/10/01&end=2020/11/01')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    // console.log(res.body)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('wait','success')
                    res.body.wait.should.be.a('array')
                    res.body.success.should.be.a('array')
                    if( res.body.wait.length != 0 ){
                        res.body.wait[0].should.be.a('object')
                        res.body.wait[0].should.include.keys('total','general')
                        res.body.wait[0].total.should.be.a('number')
                        res.body.wait[0].general.should.be.a('array')
                    }
                    if( res.body.success.length != 0 ){
                        res.body.success[0].should.be.a('object')
                        res.body.success[0].should.include.keys('total','general')
                        res.body.success[0].total.should.be.a('number')
                        res.body.success[0].general.should.be.a('array')
                    }
                    done()
                })
        })
    })
    describe('/GET author analytic by purchase', () => {
        it('it should GET author analytic by purchase sending a the token like anyone', (done) => {
            chai
                .request(server)
                .get('/api/1.0/analytics/author')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    console.log(res.body)
                    res.should.have.status(200)
                    // console.log(res.body)
                    res.body.should.be.an('object')
                    if( res.body.data.length !== 0 ){
                        res.body[0].should.be.a('object')
                        res.body[0].should.include.keys('purchasesGroup','author')
                        res.body[0].purchasesGroup.should.be.a('object')
                        res.body[0].purchasesGroup.should.include.keys('status','total','purchases')
                        res.body[0].purchasesGroup.status.should.be.a('string')
                        res.body[0].purchasesGroup.total.should.be.a('number')
                        res.body[0].purchasesGroup.purchases.should.be.a('array')
                        res.body[0].author.should.be.a('object')
                        res.body[0].author.should.include.keys('_id','name','email','role')
                        res.body[0].author._id.should.be.a('string')
                        res.body[0].author.name.should.be.a('string')
                        res.body[0].author.email.should.be.a('string')
                        res.body[0].author.role.should.be.a('string')
                    }
                    done()
                })
        })
        it('it should GET author analytic by purchase sending a the token like anyone and parameters by date', (done) => {
            chai
                .request(server)
                .get('/api/1.0/analytics/author?start=2020/10/01&end=2020/11/01')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    console.log(res.body)
                    res.body.should.be.an('object')
                    if( res.body.data.length != 0 ){
                        res.body.data[0].should.be.a('object')
                        res.body.data[0].should.include.keys('statusGroup','author')
                        res.body.data[0].statusGroup.should.be.a('array')
                        res.body.data[0].statusGroup[0].should.include.keys('status','total','purchases')
                        res.body.data[0].statusGroup[0].status.should.be.a('string')
                        res.body.data[0].statusGroup[0].total.should.be.a('number')
                        res.body.data[0].statusGroup[0].purchases.should.be.a('array')
                        res.body.data[0].author.should.be.a('object')
                        res.body.data[0].author.should.include.keys('_id','name','email','role')
                        res.body.data[0].author._id.should.be.a('string')
                        res.body.data[0].author.name.should.be.a('string')
                        res.body.data[0].author.email.should.be.a('string')
                        res.body.data[0].author.role.should.be.a('string')
                    }
                    done()
                })
        })
    })
})

exports.credentials = loginDetails