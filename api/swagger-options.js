const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Travel app (Documentation)',
      description: 'Para la implementacion de la api con el FRONT o APP ' +
        'necesitaras realizar las siguiente conexiones',
      contact: {
        name: 'Developer'
      },
      servers: [
        'http://localhost:3000'
      ]
    },
    schemes: ["http"],
    "securityDefinitions": {
      "APIKeyHeader": {
        "type": "apiKey",
        "in": "header",
        "name": "Authorization"
      }
    },
    security: [
      { APIKeyHeader: [] }
    ],
    definitions: {
      categories: {
        "type": "object",
        properties: {
          name: {
            type: "string"
          },
          icon: {
            type: "string"
          },
          description: {
            type: "string"
          },
          color: {
            type: "string"
          }
        }
      },
      cities: {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          }
        }
      },
      comments: {
        "type": "object",
        "required": [
          "content",
          "status",
          "sharedProfile",
          "idReservation",
          "idTour",
          "vote"
        ],
        "properties": {
          "content": {
            "type": "string"
          },
          "status": {
            "type": "string"
          },
          "sharedProfile": {
            "type": "boolean"
          },
          "idReservation": {
            "type": "string"
          },
          "idTour": {
            "type": "string"
          },
          "vote": {
            "type": "object",
            "properties": {
              "generalRating": {
                "type": "integer"
              }
            }
          }
        }
      },
      coupons: {
        "type": "object",
        "required": [
          "startDateCoupon",
          "endDateCoupon",
          "amount",
          "couponCode",
          "couponType",
          "idTour"
        ],
        "properties": {
          "startDateCoupon": {
            "type": "string"
          },
          "endDateCoupon": {
            "type": "string"
          },
          "amount": {
            "type": "integer"
          },
          "couponCode": {
            "type": "string"
          },
          "couponType": {
            "type": "string"
          },
          "idTour": {
            "type": "string"
          }
        }
      },
      departures: {
        "type": "object",
        "required": [
          "startDateDeparture",
          "endDateDeparture",
          "stock",
          "minStock",
          "normalPrice",
          "specialPrice",
          "status",
          "closeDateDeparture",
          "flight",
          "idTour",
          "startLocationDeparture",
          "endLocationDeparture"
        ],
        "properties": {
          "startDateDeparture": {
            "type": "string"
          },
          "endDateDeparture": {
            "type": "string"
          },
          "stock": {
            "type": "number"
          },
          "minStock": {
            "type": "number"
          },
          "normalPrice": {
            "type": "number"
          },
          "specialPrice": {
            "type": "number"
          },
          "status": {
            "type": "string"
          },
          "closeDateDeparture": {
            "type": "string"
          },
          "flight": {
            "type": "boolean"
          },
          "idTour": {
            "type": "string"
          },
          "startLocationDeparture": {
            "type": "object",
            "properties": {
              "coordinates": {
                "type": "array",
                "items": {
                  "type": "number"
                }
              },
              "type": {
                "type": "string"
              }
            }
          },
          "endLocationDeparture": {
            "type": "object",
            "properties": {
              "coordinates": {
                "type": "array",
                "items": {
                  "type": "number"
                }
              },
              "type": {
                "type": "string"
              }
            }
          }
        }
      },
      favorites: {
        "type": "object",
        "required": [
          "idTour"
        ],
        "properties": {
          "idTour": {
            "type": "string"
          }
        }
      },
      itineraries: {
        "type": "object",
        "required": [
          "itineraryName",
          "itineraryDescription",
          "location",
          "idTour",
          "stringLocation"
        ],
        "properties": {
          "itineraryName": {
            "type": "string"
          },
          "itineraryDescription": {
            "type": "string"
          },
          "idTour": {
            "type": "string"
          },
          "location": {
            "type": "object",
            "properties": {
              "coordinates": {
                "type": "array",
                "items": {
                  "type": "number"
                }
              },
              "type": {
                "type": "string"
              }
            }
          },
          "stringLocation": {
            "type": "object",
            "properties": {
              "city": {
                "type": "string"
              },
              "country": {
                "type": "string"
              },
              "neighborhood": {
                "type": "string"
              }
            }
          }
        }
      },
      messages: {
        "type": "object",
        "required": [
          "message",
          "topic"
        ],
        "properties": {
          "message": {
            "type": "string"
          },
          "topic": {
            "type": "string"
          }
        }
      },
      notifications: {
        "type": "object",
        "required": [
          "title",
          "body",
          "toUser",
          "topic"
        ],
        "properties": {
          "title": {
            "type": "string"
          },
          "body": {
            "type": "string"
          },
          "toUser": {
            "type": "string"
          },
          "topic": {
            "type": "object",
            "properties": {
              "id": {
                "type": "string"
              }
            }
          }
        }
      },
      payOrders: {
        "type": "object",
        "required": [
          "status",
          "idOperation",
          "amount",
          "platform",
          "idReservation",
          "description"
        ],
        "properties": {
          "status": {
            "type": "string"
          },
          "idOperation": {
            "type": "string"
          },
          "amount": {
            "type": "number"
          },
          "platform": {
            "type": "string"
          },
          "idReservation": {
            "type": "string"
          },
          "description": {
            "type": "string"
          }
        }
      },
      profile: {
        "type": "object",
        "required": [
          "name",
          "urlTwitter",
          "urlGitHub",
          "phone",
          "city",
          "country"
        ],
        "properties": {
          "name": {
            "type": "string"
          },
          "urlTwitter": {
            "type": "string"
          },
          "urlGitHub": {
            "type": "string"
          },
          "phone": {
            "type": "string"
          },
          "city": {
            "type": "string"
          },
          "country": {
            "type": "string"
          }
        }
      },
      reservations: {
        "type": "object",
        "required": [
          "travelerFirstName",
          "travelerLastName",
          "travelerEmail",
          "travelerPhone",
          "travelerDocument",
          "travelerAddres",
          "travelerBirthDay",
          "travelerGender",
          "idDeparture",
          "idTour",
          "status",
          "observations",
          "amount",
          "country",
          "buyerFirstName",
          "buyerLastName",
          "buyeremail",
          "buyerAddres",
          "buyerPhone",
          "buyerBirthDay",
          "emergencyName",
          "emergencyPhone",
          "invoice"
        ],
        "properties": {
          "travelerFirstName": {
            "type": "string"
          },
          "travelerLastName": {
            "type": "string"
          },
          "travelerEmail": {
            "type": "string"
          },
          "travelerPhone": {
            "type": "string"
          },
          "travelerDocument": {
            "type": "string"
          },
          "travelerAddres": {
            "type": "string"
          },
          "travelerBirthDay": {
            "type": "string"
          },
          "travelerGender": {
            "type": "string"
          },
          "idDeparture": {
            "type": "string"
          },
          "idTour": {
            "type": "string"
          },
          "status": {
            "type": "string"
          },
          "observations": {
            "type": "string"
          },
          "amount": {
            "type": "number"
          },
          "country": {
            "type": "string"
          },
          "buyerFirstName": {
            "type": "string"
          },
          "buyerLastName": {
            "type": "string"
          },
          "buyeremail": {
            "type": "string"
          },
          "buyerAddres": {
            "type": "string"
          },
          "buyerPhone": {
            "type": "string"
          },
          "buyerBirthDay": {
            "type": "string"
          },
          "emergencyName": {
            "type": "string"
          },
          "emergencyPhone": {
            "type": "string"
          },
          "invoice": {
            "type": "string"
          }
        }
      },
      schedules: {
        "type": "object",
        "required": [
          "startRangeTime",
          "endRangeTime",
          "stock",
          "minStock",
          "normalPrice",
          "specialPrice",
          "status",
          "idTour",
          "startLocationschedule",
          "endLocationschedule"
        ],
        "properties": {
          "startRangeTime": {
            "type": "string"
          },
          "endRangeTime": {
            "type": "string"
          },
          "stock": {
            "type": "number"
          },
          "minStock": {
            "type": "number"
          },
          "normalPrice": {
            "type": "number"
          },
          "specialPrice": {
            "type": "number"
          },
          "status": {
            "type": "string"
          },
          "idTour": {
            "type": "string"
          },
          "startLocationschedule": {
            "type": "object",
            "properties": {
              "coordinates": {
                "type": "array",
                "items": {
                  "type": "number"
                }
              },
              "type": {
                "type": "string"
              }
            }
          },
          "endLocationschedule": {
            "type": "object",
            "properties": {
              "coordinates": {
                "type": "array",
                "items": {
                  "type": "number"
                }
              },
              "type": {
                "type": "string"
              }
            }
          }
        }
      },
      supportTickets: {
        "type": "object",
        "required": [
          "message",
        ],
        "properties": {
          "message": {
            "type": "string"
          },
        }
      },
      tours: {
        "type": "object",
        "required": [
          "title",
          "subTitle",
          "description",
          "route"
        ],
        "properties": {
          "title": {
            "type": "string"
          },
          "subTitle": {
            "type": "string"
          },
          "description": {
            "type": "string"
          },
          "route": {
            "type": "string"
          }
        }
      },
      users: {
        "type": "object",
        "required": [
          "name",
          "email",
          "password",
          "role",
          "phone",
          "city",
          "country"
        ],
        "properties": {
          "name": {
            "type": "string"
          },
          "email": {
            "type": "string"
          },
          "password": {
            "type": "string"
          },
          "role": {
            "type": "string"
          },
          "phone": {
            "type": "string"
          },
          "city": {
            "type": "string"
          },
          "country": {
            "type": "string"
          }
        }
      }
    }
  },
  //routers
  apis: ['./app/routesApi/*.js']
}
const swaggerDocs = swaggerJsDoc(swaggerOptions)
console.log(swaggerDocs.paths['/users'].post.parameters)

module.exports = (app = {}) => {
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))
}
